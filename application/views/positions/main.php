<style type="text/css">
    .deposit_date select {
        display: inline-block;
        width: 32%;
        vertical-align: middle;
    }
    .file {
      visibility: hidden;
      position: fixed;
   }
   .card-bank .card-body {
      padding: 10px;
      display: grid;
      grid-template-columns: 25% auto;
   }
   .img-thumbnail {
      background-color : transparent;
      border : 0px;
   }
   td img{
      display: block;
      margin-left: auto;
      margin-right: auto;
   }
</style>
<div class="row">
   <div class="col-xs-12">
      <div class="row">
         <div class="col-md-10">
            <form class="form-inline" id="search_from">
               <!-- <input type="text" class="input-sm" name="qtitle" id="qtitle" placeholder="หัวเรื่อง"> &nbsp; -->
               <input type="hidden" class="input-sm" name="qpage" id="qpage" value="0">
               <select class="form-control" id="qtitle" name="qtitle">
                  <option value="">-- ทั้งหมด --</option>
                  <?php foreach ($company as $k => $v):?>
                    <option value="<?php echo $v['name'];?>"><?php echo $v['name'];?></option>
                  <?php endforeach;?>
               </select>
               <label>จำนวนแถว : </label>
               <select class="form-control input-sm" id="qper_page" name="qper_page">
                  <option value="100">100</option>
                  <option value="300">300</option>
                  <option value="500">500</option>
                  <option value="1000">1000</option>
                  <option value="2000">2000</option>
                  <option value="3000">3000</option>
               </select>
               <button type="button" class="btn btn-info btn-sm" onclick="search_data();">
                  <i class="ace-icon fa fa-search bigger-110"></i>Search
               </button>
            </form>
         </div>
         <div class="col-md-1 pull-right">
            <div class="clearfix">
               <div class="pull-right">
                  <a class="btn btn-sm btn-success pull-right" href="#" onclick="add_data();"><i class="fa fa-plus"></i> เพิ่มข้อมูล</a>
               </div>
            </div>
         </div>
      </div>
      <div class="hr dotted"></div>
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <div class="table-responsive">
                  <table class="table table-bordered text-left table-hover" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                           <th class="text-center">ลำดับ</th>
                           <th class="text-center">ชื่อบริษัท</th>
                           <th class="text-center">ชื่อแผนก</th>
                           <th class="text-center">ชื่อตำแหน่ง</th>
                           <th class="text-center">ชื่อย่อตำแหน่ง</th>
                           <th class="text-center">เวลาสร้าง</th>
                           <th class="text-center">จัดการ</th>
                        </tr>
                     </thead>
                     <tbody id="tb_contents">
                        
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/js/chosen.jquery.min.js"></script>
<script src="//cdn.ckeditor.com/4.17.1/standard/ckeditor.js"></script>
<script type="text/javascript">
   var route = '<?php echo base_url().''.$this->uri->segment(1);?>';
   search_data();
   
   function search_data(){
      $.ajax({
         type: "POST",
         url: route+'/search',  
         data: $("#search_from").serializeArray(),
         dataType: 'json',
         success: function(res,s,y){
            if(res.status){
               $('#dataTable tbody').html(res.data);
            }else{
               alert(res.msg);
            }
         },
      });
   }

   function add_data(){
      $.ajax({
         url: route+'/add',  
         data: {},
         type: 'GET',
         dataType: 'json',
         success: function (res) {
            bootbox.dialog({
               message:res.temp,
               size: "large",
               buttons:            
               {
                  "OK" :
                  {
                     "label" : "<i class='ace-icon fa fa-check '></i> บันทึก",
                     "className" : "btn-sm btn-success",
                     "callback": function(e) {
                        // var files = $('#fi')[0].files;
                        // console.log(files);
                        var fd = new FormData();
                        // var files = $('#fi')[0].files;
                        // fd.append('file',files[0]);
                        fd.append('title',$('#title').val());
                        fd.append('desc',$('#desc').val());
                        // fd.append('desc',CKEDITOR.instances.desc.getData());
                        fd.append('status',$('#status').val());
                        fd.append('depart',$('#depart').val());
                        $.ajax({
                           url: route+'/create',
                           data: fd,
                           type: 'POST',
                           dataType: 'json',
                           contentType: false,
                           processData: false,
                           success: function (res) {
                              if(res.status){
                                 $.gritter.add({
                                    title: 'Success',
                                    text: res.msg,
                                    class_name: 'gritter-success'
                                 });
                              }else{
                                 $.gritter.add({
                                    title: 'Error',
                                    text: res.msg,
                                    class_name: 'gritter-error'
                                 });
                              }
                              search_data();
                           },
                           error: function (err) {
                               console.log(err);
                           }
                       });
                     }
                  },
                  "NO" :
                  {
                     "label" : "<i class='ace-icon fa fa-close'></i> ปิด",
                     "className" : "btn-sm btn-danger"
                  }
               }
            });
         }
      });
   }

   function edit_data(id){
      $.ajax({
         url: route+'/edit/'+id,  
         data: {},
         type: 'GET',
         dataType: 'json', 
         success: function (res) {
            bootbox.dialog({
               message:res.temp,
               size: "large",
               buttons:            
               {
                  "OK" :
                  {
                     "label" : "<i class='ace-icon fa fa-check '></i> บันทึก",
                     "className" : "btn-sm btn-success",
                     "callback": function(e) {
                        var fd = new FormData();
                        // var files = $('#fi')[0].files;
                        // fd.append('file',files[0]);
                        fd.append('title',$('#title').val());
                        fd.append('desc',$('#desc').val());
                        // fd.append('desc',CKEDITOR.instances.desc.getData());
                        fd.append('status',$('#status').val());
                        fd.append('depart',$('#depart').val());

                        $.ajax({
                           url: route+'/update/'+id,
                           data: fd,
                           type: 'POST',
                           dataType: 'json',
                           contentType: false,
                           processData: false,
                           success: function (res) {
                              if(res.status){
                                 $.gritter.add({
                                    title: 'Success',
                                    text: res.msg,
                                    class_name: 'gritter-success'
                                 });
                              }else{
                                 $.gritter.add({
                                    title: 'Error',
                                    text: res.msg,
                                    class_name: 'gritter-error'
                                 });
                              }
                              search_data();
                           },
                           error: function (err) {
                               console.log(err);
                           }
                        });
                     }
                  },
                  "NO" :
                  {
                      "label" : "<i class='ace-icon fa fa-close'></i> ปิด",
                      "className" : "btn-sm btn-danger"
                  }
               }
            });  
         }
      });
   }

   function del_data(id){
       
   }
</script>