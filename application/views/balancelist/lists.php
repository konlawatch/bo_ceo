<thead>
   <tr>
      <th>ลำดับ</th>
      <th>งวดวันที่</th>
      <th>จำนวนฝาก</th>
      <th>ยอดฝาก</th>
      <th>จำนวนถอน</th>
      <th>ยอดถอน</th>
      <th>ยอดโบนัส</th>
      <th>+- ฝาก/ถอน</th>
      <th>+- ธนาคาร</th>
      <th>ยอดส่วนต่าง</th>
      <th>แพ้ชนะ</th>
      <th>สถานะ</th>
      <th>จัดการ</th>
   </tr>
</thead>
<tbody>
   <?php $i = ($this->uri->segment(3) != '') ? (($this->uri->segment(3) - 1) * 50) + 1 : 1;
     $sum_did_cnt = $sum_did_amt = $sum_wid_cnt = $sum_wid_amt = $sum_bonus_amt = $sum_didwid = $sum_bank_bl = $sum_diff = $sum_wl = 0;
   ?>
   <?php if (isset($data) && count($data) >= 1): ?>
      <?php foreach ($data as $item): ?>
       <?php 
         $sum_did_cnt += $item->did_cnt;
         $sum_did_amt += $item->did_amount;
         $sum_wid_cnt += $item->wid_cnt;
         $sum_wid_amt += $item->wid_amount;
         $sum_bonus_amt += $item->bonus_amount;
         $sum_didwid += $item->didwid_bl;
         $sum_bank_bl += $item->bank_bl;
         $sum_diff += $item->diff;
         
         $wl = $item->did_amount - ($item->wid_amount + $item->bonus_amount);
         $sum_wl += $wl;
       ?>
            <tr class="text-right">
               <td><?php echo $i; ?></td>
               <td><?php echo $item->bdate; ?></td>
               <td><?php echo number_format($item->did_cnt); ?></td>
               <td><?php echo number_format($item->did_amount,2); ?></td>
               <td><?php echo number_format($item->wid_cnt); ?></td>
               <td><?php echo number_format($item->wid_amount,2); ?></td>
               <td><?php echo number_format($item->bonus_amount,2); ?></td>
               <td><?php echo number_format($item->didwid_bl,2); ?></td>
               <td><?php echo number_format($item->bank_bl,2); ?></td>
               <td class="<?php echo ($item->diff < 0) ? 'text-red': 'text-green';?>"><?php echo number_format($item->diff,2); ?></td>
               <td class="<?php echo ($wl < 0) ? 'text-red': 'text-green';?>"><?php echo number_format($wl ,2); ?></td>
               <td><?php echo $item->flag; ?></td>
               <td>
                  <div class="btn-group">
                     <a class="fa fa-search btn btn-minier btn-white" href="<?php echo base_url('balance/period/' . $item->bdate.'?company='.$company); ?>"> ดูข้อมูล</a>
                     <a class="fa fa-search btn btn-minier btn-white" href="<?php echo base_url('history/balance/' . $item->bdate.'?company='.$company); ?>" target="_blank"> ดูรายการ</a>
                  </div>
              </td>
         </tr>
         <?php $i++;?>
      <?php endforeach;?>
      <tr class="text-right">
         <td colspan="2" class="text-right">ยอดรวม</td>
         <td><?php echo number_format($sum_did_cnt);?></td>
         <td><?php echo number_format($sum_did_amt,2);?></td>
         <td><?php echo number_format($sum_wid_cnt);?></td>
         <td><?php echo number_format($sum_wid_amt,2);?></td>
         <td><?php echo number_format($sum_bonus_amt,2);?></td>
         <td><?php echo number_format($sum_didwid,2);?></td>
         <td><?php echo number_format($sum_bank_bl,2);?></td>
         <td class="<?php echo ($sum_diff < 0) ? 'text-red': 'text-green';?>"><?php echo number_format($sum_diff,2);?></td>
         <td class="<?php echo ($sum_wl < 0) ? 'text-red': 'text-green';?>"><?php echo number_format($sum_wl,2);?></td>
         <td colspan="2"></td>
      </tr>
      <?php else: ?>
         <tr>
            <td  colspan="12" class="text-red text-center"> ไม่มีข้อมูล. </td>
         </tr>
   <?php endif;?>
</tbody>