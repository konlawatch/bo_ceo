<?php $monthlist = array('มค.','กพ.','มีค.','เมษ.','พค.','มิย.','กค.','สค.','กย.','ตค.','พย.','ธค.');?>
<div class="row">
   <div class="col-xs-12">
      <div class="row">
         <form class="form-inline">
            <div class="col-md-12">
              <!--  <label>บริษัท : </label>
               <select class="chosen-select-customer form-control" id="company">
                  <option value="">--- กรุณาเลือก ----</option>
                  <option value="enjoy">Enjoy</option>
                  <option value="asia">Asia</option>
                  <option value="play">Play</option>
                  <option value="official">Official</option>
               </select> -->
               <label>วันที่ : </label>
               <input class="form-control date-picker input-sm" name="bdate" id="bdate" type="text" data-date-format="dd-mm-yyyy" value="">
               <label>ถึง : </label>
               <input class="form-control date-picker input-sm" name="edate" id="edate" type="text" data-date-format="dd-mm-yyyy" value="">
               <label>จำนวนแถว : </label>
               <select class="chosen-select-customer form-control" id="per_page">
                  <option value="100">100</option>
                  <option value="500">500</option>
               </select>
               <label>รูปแบบ : </label>
               <select class="chosen-select-customer form-control" id="type">
                  <option value="month">รายเดือน</option>
                  <option value="day">รายวัน</option>
               </select>
               <button type="button" class="btn btn-info btn-sm" onclick="searchdata();">
                  <i class="ace-icon fa fa-search bigger-110"></i>Search
               </button>
               <button type="reset" class="btn btn-warning btn-sm" >
                  <i class="ace-icon fa fa-search bigger-110"></i>Clear
               </button>
               <!-- <button type="button" class="btn btn-success btn-sm" onclick="searchdata('7days');">
                  <i class="ace-icon fa fa-search bigger-110"></i>7 วันที่ผ่านมา
               </button>
               <button type="button" class="btn btn-success btn-sm" onclick="searchdata('30days');">
                  <i class="ace-icon fa fa-search bigger-110"></i>30 วันที่ผ่านมา
               </button> -->
            </div>
            <div class="col-md-12">
               <div class="hr dotted"></div>
               <div class="control-group">
                  <label>เลือกปี : </label>
                  <select class="chosen-select-customer form-control" id="year">
                     <option value="2022">2022</option>
                     <option value="2021">2021</option>
                     <option value="2020">2020</option>
                  </select>
                  <label style="margin-right: 10px;">
                      <input name="all" class="ace ace-checkbox-2 chkGame" type="checkbox" onclick="checkAll('all');" />
                      <span class="lbl"> ทั้งหมด</span>
                  </label>
                  <?php foreach($monthlist as $k => $v):?>
                     <div class="checkbox">
                        <label style="margin-right: 10px;">
                           <input name="month" class="ace ace-checkbox-2 chkGame" type="checkbox" onclick="checkAll();" value="<?php echo $k+1;?>" id="checkbox_<?php echo $k+1;?>">
                           <span class="lbl"> <?php echo $v;?></span>
                        </label>
                     </div>
                  <?php endforeach;?>
               </div>
            </div>
         </form>
         <!-- <div class="col-md-2 pull-right">
            <div class="clearfix">
               <div class="pull-right">
                  <a class="btn btn-sm btn-success pull-right" href="<?php echo base_url(); ?>customer/add"><i class="fa fa-plus"></i> เพิ่มข้อมูล</a>
               </div>
            </div>
         </div> -->
      </div>
      <div class="hr dotted"></div>
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                     
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   function checkAll(type){
        if(type == 'all'){
            $(".chkGame").prop('checked', $('input[name="all"]').prop("checked"));
        }else{
            var chk = true;
            $('.chkGame').each(function(){
                if(!$(this).prop('checked')){
                    chk = false;
                }
            });
            $('input[name="all"]').prop("checked",chk);
        }
   }
   var page  = 0;
   // searchdata('today');
   function searchdata(){
      var company = '<?php echo $company;?>';
      var month = [];
      if(company != ''){
         $('#loading').show();
         $("input:checkbox[name=month]:checked").each(function(){
            month.push($(this).val());
         });
         $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>reportwinloss/search',  
            data: {
               company  : company,
               // type     : (month.length > 0) ? 'month' : type,
               type     : $('#type').val(),
               year     : $('#year').val(),
               month    : month.join(","),
               bdate    : $('#bdate').val(),
               edate    : $('#edate').val(),
               page     : page,
               per_page : $('#per_page').val(),
            },
            dataType: 'json',
            success: function(res){
                // var tb ='';
                // if(res.status){
                //     res.data.forEach(function(k,v){
                //         tb += `<tr>
                //             <td>${(v + 1)}</td>
                //             <td>${k.bdate}</td>
                //             <td>${k.did_cnt}</td>
                //             <td>${k.did_amount}</td>
                //             <td>${k.wid_cnt}</td>
                //             <td>${k.wid_amount}</td>
                //             <td>${k.bonus_amount}</td>
                //             <td>${k.didwid_bl}</td>
                //             <td>${k.bank_bl}</td>
                //             <td>${k.diff}</td>
                //             <td>${k.flag}</td>
                //            <td>
                //               <div class="btn-group">
                //                  <a class="fa fa-search btn btn-minier btn-white" href="<?php echo base_url('balance/period/${k.bdate}?company=${company}'); ?>" target="_blank"> ดูข้อมูล </a>
                //                  <a class="fa fa-search btn btn-minier btn-white" href="<?php echo base_url('history/balance/${k.bdate}?company=${company}'); ?>" target="_blank"> ดูรายการ</a>
                //               </div>
                //            </td>
                //         </tr>`;
                //     });

                //     $('#dataTable tbody').html((tb == '')? '<tr><td  colspan="10" class="text-danger text-center"> ไม่มีข้อมูล. </td></tr>' : tb);
                //     // $('.num').digits();
                // }else{
                //     $('#dataTable tbody').html('<tr><td  colspan="12" class="text-danger text-center"> ไม่มีข้อมูล. </td></tr>');
                    
                // }
               if(res.status){
                  if(res.sm != '' && res.em != ''){
                     for (var i = res.sm; i <= res.em; i++) {
                        $('#checkbox_'+i).prop('checked', true);
                     }
                  }
                  $('#bdate').val(res.q.bdate);
                  $('#edate').val(res.q.edate);
                  $('#dataTable').html(res.data);
               }else{
                  alert(res.msg);
               }
               $('#loading').hide();
            },
         });
      }else{
         alert('กรุณาเลือกบริษัท !!');
         $('#company').focus();
         return;
      }
   }
</script>
