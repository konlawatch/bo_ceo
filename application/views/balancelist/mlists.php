<?php 
   $dd = array();
   foreach ($data as $item){
      $month = date('m-Y',strtotime($item->bdate));
      if(!isset($dd[$month])){
         $dd[$month] = array(
            'month' => $month,
            'did_cnt'      => $item->did_cnt,
            'did_amount'   => $item->did_amount,
            'wid_cnt'      => $item->wid_cnt,
            'wid_amount'   => $item->wid_amount,
            'bonus_amount' => $item->bonus_amount,
            'didwid_bl'    => $item->didwid_bl,
            'bank_bl'      => $item->bank_bl,
            'diff'         => $item->diff,
            'wl'           => ($item->wid_amount + $item->bonus_amount) - $item->did_amount,
         );
      }else{
         $dd[$month]['did_cnt']     += $item->did_cnt;
         $dd[$month]['did_amount']  += $item->did_amount;
         $dd[$month]['wid_cnt']     += $item->wid_cnt;
         $dd[$month]['wid_amount']  += $item->wid_amount;
         $dd[$month]['bonus_amount'] += $item->bonus_amount;
         $dd[$month]['didwid_bl']   += $item->didwid_bl;
         $dd[$month]['didwid_bl']   += $item->didwid_bl;
         $dd[$month]['bank_bl']     += $item->bonus_amount;
         $dd[$month]['diff']        += $item->diff;
         $dd[$month]['wl']          += ($item->wid_amount + $item->bonus_amount) - $item->did_amount;
      }
   }

   // debug($dd,true);
?>
<thead>
   <tr>
      <th>ลำดับ</th>
      <th>งวดวันที่</th>
      <th>จำนวนฝาก</th>
      <th>ยอดฝาก</th>
      <th>จำนวนถอน</th>
      <th>ยอดถอน</th>
      <th>ยอดโบนัส</th>
      <th>+- ฝาก/ถอน</th>
      <th>+- ธนาคาร</th>
      <th>ยอดส่วนต่าง</th>
      <th>แพ้ชนะ</th>
   </tr>
</thead>
<tbody>
   <?php $i = ($this->uri->segment(3) != '') ? (($this->uri->segment(3) - 1) * 50) + 1 : 1;
     $sum_did_cnt = $sum_did_amt = $sum_wid_cnt = $sum_wid_amt = $sum_bonus_amt = $sum_didwid = $sum_bank_bl = $sum_diff = $sum_wl = 0;
   ?>
   <?php if (isset($dd) && count($dd) >= 1): ?>
      <?php foreach ($dd as $k => $item): ?>
       <?php 
         $sum_did_cnt += $item['did_cnt'];
         $sum_did_amt += $item['did_amount'];
         $sum_wid_cnt += $item['wid_cnt'];
         $sum_wid_amt += $item['wid_amount'];
         $sum_bonus_amt += $item['bonus_amount'];
         $sum_didwid    += $item['didwid_bl'];
         $sum_bank_bl   += $item['bank_bl'];
         $sum_diff      += $item['diff'];
         
         $wl = $item['did_amount'] - ($item['wid_amount'] + $item['bonus_amount']);
         $sum_wl += $wl;
       ?>
         <tr class="text-right">
            <td><?php echo $i; ?></td>
            <td><?php echo $k; ?></td>
            <td><?php echo number_format($item['did_cnt']); ?></td>
            <td><?php echo number_format($item['did_amount'],2); ?></td>
            <td><?php echo number_format($item['wid_cnt']); ?></td>
            <td><?php echo number_format($item['wid_amount'],2); ?></td>
            <td><?php echo number_format($item['bonus_amount'],2); ?></td>
            <td><?php echo number_format($item['didwid_bl'],2); ?></td>
            <td><?php echo number_format($item['bank_bl'],2); ?></td>
            <td class="<?php echo ($item['diff'] < 0) ? 'text-red': 'text-green';?>"><?php echo number_format($item['diff'],2); ?></td>
            <td class="<?php echo ($wl < 0) ? 'text-red': 'text-green';?>"><?php echo number_format($wl ,2); ?></td>
         </tr>
         <?php $i++;?>
      <?php endforeach;?>
      <tr class="text-right">
         <td colspan="2" class="text-right">ยอดรวม</td>
         <td><?php echo number_format($sum_did_cnt);?></td>
         <td><?php echo number_format($sum_did_amt,2);?></td>
         <td><?php echo number_format($sum_wid_cnt);?></td>
         <td><?php echo number_format($sum_wid_amt,2);?></td>
         <td><?php echo number_format($sum_bonus_amt,2);?></td>
         <td><?php echo number_format($sum_didwid,2);?></td>
         <td><?php echo number_format($sum_bank_bl,2);?></td>
         <td class="<?php echo ($sum_diff < 0) ? 'text-red': 'text-green';?>"><?php echo number_format($sum_diff,2);?></td>
         <td class="<?php echo ($sum_wl < 0) ? 'text-red': 'text-green';?>"><?php echo number_format($sum_wl,2);?></td>
      </tr>
      <?php else: ?>
         <tr>
            <td  colspan="12" class="text-red text-center"> ไม่มีข้อมูล. </td>
         </tr>
   <?php endif;?>
</tbody>