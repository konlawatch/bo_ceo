<style type="text/css">
    .infobox {
        width: 280px;
    }
</style>
<?php if(isset($bl[0])):?>
<div class="row">
   <div class="space-6"></div>
   <div class="col-sm-12 infobox-container">
      <div class="infobox infobox-green">
         <div class="infobox-icon">
            <i class="ace-icon fa fa-usd"></i>
         </div>
         <div class="infobox-data">
            <span class="infobox-data-number" id="bl_did"><?php echo number_format($bl[0]->did_amount,2);?> / <?php echo $bl[0]->did_cnt;?></span>
            <div class="infobox-content">ยอดฝาก / จำนวน</div>
         </div>
      </div>
      <div class="infobox infobox-red">
         <div class="infobox-icon">
            <i class="ace-icon fa fa-usd"></i>
         </div>
         <div class="infobox-data">
            <span class="infobox-data-number" id="bl_wid"><?php echo number_format($bl[0]->wid_amount,2);?> / <?php echo $bl[0]->wid_cnt;?></span>
            <div class="infobox-content">ยอดถอน / จำนวน</div>
         </div>
      </div>
      <?php $winloss = ($bl[0]->did_amount - $bl[0]->wid_amount);?>
      <div class="infobox infobox-<?php echo ($winloss > 0)? 'green':'red';?>">
         <div class="infobox-icon">
            <i class="ace-icon fa fa-usd"></i>
         </div>
         <div class="infobox-data">
            <span class="infobox-data-number" id="winloss"><?php echo number_format($winloss,2);?></span>
            <div class="infobox-content">แพ้-ชนะ</div>
         </div>
      </div>
      <div class="infobox infobox-blue2">
         <div class="infobox-icon">
            <i class="ace-icon fa fa-usd"></i>
         </div>
         <div class="infobox-data">
            <span class="infobox-data-number" id="diff"><?php echo number_format($bl[0]->diff,2);?></span>
            <div class="infobox-content">ส่วนต่าง</div>
         </div>
      </div>
      <div class="infobox infobox-blue">
         <div class="infobox-icon">
            <i class="ace-icon fa fa-exchange"></i>
         </div>
         <div class="infobox-data">
            <span class="infobox-data-number" id="cnt_banklog"><?php echo number_format($cnt_banklog->amt,2).' / '.$cnt_banklog->cnt;?></span>
            <div class="infobox-content">รอผูกธนาคาร / จำนวน</div>
         </div>
      </div>
      <div class="infobox infobox-blue">
         <div class="infobox-icon">
            <i class="ace-icon fa fa-exchange"></i>
         </div>
         <div class="infobox-data">
            <span class="infobox-data-number" id="cnt_did"><?php echo $cnt_did->cnt;?></span>
            <div class="infobox-content">กำลังฝาก</div>
         </div>
      </div>
      <div class="infobox infobox-blue">
         <div class="infobox-icon">
            <i class="ace-icon fa fa-exchange"></i>
         </div>
         <div class="infobox-data">
            <span class="infobox-data-number" id="cnt_wid"><?php echo $cnt_wid->cnt;?></span>
            <div class="infobox-content">กำลังถอน</div>
         </div>
      </div>
      <?php foreach ($agentlist as $key => $v):?>
         <div class="infobox infobox-<?php echo ($v->run_status == '1')? 'green' : 'red';?>">
            <div class="infobox-icon">
               <i class="ace-icon fa fa-cogs"></i>
            </div>
            <div class="infobox-data">
               <span class="infobox-data-number" id="<?php echo $v->user;?>"><?php echo number_format($v->total_member_credit,2);?> / <?php echo ($v->run_status == '1')? 'RUN' : 'STOP';?></span>
               <div class="infobox-content"><?php echo $v->user;?> </div>
            </div>
         </div>
      <?php endforeach;?>
      <?php foreach ($banklist as $key => $v):?>
         <div class="infobox infobox-<?php echo ($v->run_status == '1')? 'green' : 'red';?>">
            <div class="infobox-icon">
               <i class="ace-icon fa fa-cogs"></i>
            </div>
            <div class="infobox-data">
               <span class="infobox-data-number" id="<?php echo $v->user;?>"><?php echo number_format($v->balance,2);?> / <?php echo ($v->run_status == '1')? 'RUN' : 'STOP';?></span>
               <div class="infobox-content"><?php echo $v->user;?> </div>
            </div>
         </div>
      <?php endforeach;?>
   </div>
   <div class="vspace-12-sm"></div>
</div>

<script type="text/javascript">

   var cnt_dash = 0;
   cntTimedash(); 
   function cntTimedash(){
      if(cnt_dash == 0){
         var int = setInterval(function(){ 
            if(cnt_dash <= 0){
               cnt_dash = 10;
               int = '';
               get_dashboard();
            }else{
               cnt_dash--;
            }
         }, 1000*1);
         cnt_dash++;
      }
   }
   function get_dashboard(){
      $.ajax({
         type: "GET",
         url: '<?php echo base_url();?>admin/get_dashboard',
         dataType: 'json',
         success: function(res){
            if(res.status){
               res.data.bl.forEach(function(k,v){
                  $('#bl_did').html(`${numberWithCommas(k.did_amount)} / ${k.did_cnt}`);
                  $('#bl_wid').html(`${numberWithCommas(k.wid_amount)} / ${k.wid_cnt}`);
                  $('#winloss').html(`${numberWithCommas((k.did_amount - k.wid_amount).toFixed(2))}`);
                  $('#diff').html(`${numberWithCommas(k.diff)}`);
               });

               $('#cnt_banklog').html(`${(res.data.cnt_banklog.amt > 0) ? numberWithCommas(res.data.cnt_banklog.amt) : '0.00'} / ${res.data.cnt_banklog.cnt}`);
               $('#cnt_did').html(`${res.data.cnt_did.cnt}`);
               $('#cnt_wid').html(`${res.data.cnt_wid.cnt}`);

               res.data.agentlist.forEach(function(k,v){
                  $('#'+k.user).html(`${numberWithCommas(k.total_member_credit)} / ${(k.run_status == '1') ? 'RUN' : 'STOP'}`);
               });

               res.data.banklist.forEach(function(k,v){
                  $('#'+k.user).html(`${numberWithCommas(k.balance)} / ${(k.run_status == '1') ? 'RUN' : 'STOP'}`);
               });
            }
         }
      });
   }
</script>
<?php else:?>
   <h1 class="text-center">Welcome <?php echo $this->config->config['website'];?> Dashboard</h1>
<?php endif;?>

