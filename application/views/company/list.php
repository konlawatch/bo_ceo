<?php $i = ($this->uri->segment(3) != '') ? (($this->uri->segment(3) - 1) * 50) + 1 : 1;?>
<?php if (isset($data) && count($data) >= 1): ?>
   <?php foreach ($data as $item): ?>
         <tr>
            <td class="text-center"><?php echo $i; ?></td>
            <td class="text-center"><?php echo $item->name; ?></td>
            <td class="text-center"><?php echo $item->desc; ?></td>
            <!-- <td>
               <?php if($item->img_url != ''):?>
                  <img class='img-responsive' style="width:30px;"  src="<?php echo (isset($data)) ?  $item->img_url  : "255x150.png"?>"/>
               <?php endif;?>      
            </td> -->
            <!-- <td class="text-center">
               <?php echo $item->created_by;?>
            </td> -->
            <td class="text-center">
               <?php echo $item->created_at;?>
            </td>
            <td class="text-center">
               <div class="btn-group">
                  <a class="fa fa-pencil-square-o btn btn-minier btn-warning" href="#" onclick="edit_data('<?php echo $item->id;?>');"> แก้ไข</a>
                  <a class="fa fa-trash-o btn-minier btn btn-danger" onclick="return confirm('ต้องการลบ <?php echo $item->id; ?> ?')" href="<?php echo base_url($this->uri->segment(1).'/delete/' . $item->id); ?>"> ลบ</a>
               </div>
           </td>
      </tr>
      <?php $i++;?>
   <?php endforeach;?>
   <?php else: ?>
      <tr>
         <td  colspan="10" class="text-danger text-center"> ไม่มีข้อมูล. </td>
      </tr>
<?php endif;?>