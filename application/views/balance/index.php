<div class="row">
   <div class="col-xs-12">
      <span style="font-size: 18px;">งวดวันที่ : <?php echo $bldate;?></span> &nbsp;&nbsp;
      <input type="hidden" name="cdate" id="cdate" value="<?php echo $bldate;?>">
   </div>
   <div class="col-xs-12">
      <hr/>
      <div class="table-responsive">
         <table class="table table-bordered text-center" id="dataDetail">
             <thead>
                 <tr>
                     <th class="text-center" scope="col">งวดวันที่</th>
                     <th class="text-center" scope="col">รายการฝาก</th>
                     <th class="text-center" scope="col">ยอดฝาก</th>
                     <th class="text-center" scope="col">รายการถอน</th>
                     <th class="text-center" scope="col">ยอดถอน</th>
                     <th class="text-center" scope="col">+/- ฝาก-ถอน</th>
                     <th class="text-center" scope="col">+/- ธนาคาร</th>
                     <th class="text-center" scope="col">ยอดส่วนต่าง</th>
                     <th class="text-center" scope="col">เวลาคำนวนล่าสุด</th>
                     <th class="text-center" scope="col">สถานะ</th>
                 </tr>
             </thead>
             <tbody>
                  <?php $diff = 0;?>
                  <?php foreach ($balancelist as $item): ?>
                     <?php $diff = $item->didwid_bl - $item->bank_bl;?>
                     <td><?php echo $item->bdate;?></td>
                     <td><?php echo $item->did_cnt;?></td>
                     <td><?php echo number_format($item->did_amount,2);?></td>
                     <td><?php echo $item->wid_cnt;?></td>
                     <td><?php echo number_format($item->wid_amount,2);?></td>
                     <td><?php echo number_format($item->didwid_bl,2);?></td>
                     <td><?php echo number_format($item->bank_bl,2);?></td>
                     <td class="<?php echo ($diff == 0)? 'green' : 'red';?>"><?php echo number_format($diff,2);?></td>
                     <td><?php echo $item->updated_at;?></td>
                     <td><?php echo $item->flag;?></td>
                  <?php endforeach;?>
             </tbody>
         </table>
      </div>
   </div>
   <div class="col-md-8 col-sm-12">
      <div class="row">
         <div class="col-sm-12">
            <div class="widget-box widget-color-blue2 ui-sortable-handle">
               <div class="widget-header">
                  <h5 class="widget-title bigger ">
                     <i class="ace-icon fa fa-bank"></i>
                     เงินสดในบัญชี
                  </h5>
               </div>
               <div class="widget-body">
                  <div class="widget-main no-padding">
                     <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover text-right">
                           <thead class="thin-border-bottom">
                              <tr>
                                 <th>ธนาคาร</th>
                                 <th>ชื่อบัญชี</th>
                                 <th>ยอดเงินตั้งต้น</th>
                                 <th>ยอดเงินปัจจุบัน</th>
                                 <th>รายการฝาก</th>
                                 <th>ยอดฝาก</th>
                                 <th>รายการถอน</th>
                                 <th>ยอดถอน</th>
                                 <th >+ / -</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php 
                                 $sum_total_st=$sum_total_et = $sum_total_tt = $sum_total_didcnt = $sum_total_didamt = $sum_total_widcnt = $sum_total_widamt = 0;
                                 $other = array();
                              ?>
                              <?php foreach ($banklist as $item):?>
                                 <?php 

                                    foreach ($otherlist as $it){
                                       if($it->type == 'O'){
                                          $bankno = explode('-', $it->bankno);
                                          if($bankno[0] == $item->bankno){
                                             if($it->bf_amount > 0){
                                                $item->wid_amt -= $it->bf_amount;
                                             }else{
                                                $item->did_amt += $it->bf_amount;
                                             }
                                          }
                                          $other[$bankno[0]] = $it->bf_amount;
                                       }
                                    }

                                    $sum_total_st += $item->bf_amount;
                                    $sum_total_et += $item->af_amount;
                                    $diffbank = 0;
                                    $sum_total_didcnt += $item->did_cnt;
                                    $sum_total_didamt += $item->did_amt;
                                    $sum_total_widcnt += $item->wid_cnt;
                                    $sum_total_widamt += $item->wid_amt;
                                    // if(($item->bf_amount - $item->af_amount) > 0 && ($item->did_amt - $item->wid_amt) > 0){
                                    //    $diffbank = ($item->bf_amount - $item->af_amount) - ($item->did_amt - $item->wid_amt);
                                    // }else{
                                    //    $diffbank = ($item->bf_amount - $item->af_amount) + ($item->did_amt - $item->wid_amt);
                                    // }
                                    $diffbank = ($item->bf_amount + $item->did_amt) - ($item->af_amount + $item->wid_amt);
                                    // $diffbank =  (($item->bf_amount - $item->af_amount) > 0 && ($item->did_amt - $item->wid_amt) > 0) ? ($item->bf_amount - $item->af_amount) - ($item->did_amt - $item->wid_amt) : ($item->bf_amount - $item->af_amount) + ($item->did_amt - $item->wid_amt);
                                    

                                    $sum_total_tt += $diffbank;
                                 ?>
                                 <tr>
                                    <td class="text-center"><?php echo '<img src="'.base_url().'assets/images/banklogo/'.$item->bankid.'.png" alt="'.$item->bankid.'" class="img_banklogo"> '.$item->bankid;?></td>
                                    <td class="text-center"><?php echo $item->name;?></td>
                                    <td ><?php echo number_format($item->bf_amount,2);?></td>
                                    <td ><?php echo number_format($item->af_amount,2);?></td>
                                    <td ><?php echo $item->did_cnt;?></td>
                                    <?php if($item->did_amt < 0):?>
                                       <td ><?php echo number_format(($item->did_amt - $other[$item->bankno]) ,2);?></td>
                                    <?php else:?>
                                       <td ><?php echo number_format($item->did_amt,2);?></td>
                                    <?php endif;?>
                                    <td ><?php echo $item->wid_cnt;?></td>
                                    <?php if($item->wid_amt < 0):?>
                                       <td ><?php echo number_format(($item->wid_amt + $other[$item->bankno]) ,2);?></td>
                                    <?php else:?>
                                       <td ><?php echo number_format($item->wid_amt,2);?></td>
                                    <?php endif;?>
                                    <!-- <td ><?php echo number_format($item->wid_amt,2);?></td> -->
                                    <td class="<?php echo ($diffbank == 0)? 'green' : 'red';?>"><?php echo number_format($diffbank,2);?></td>
                                 </tr>
                              <?php endforeach;?>
                           </tbody>
                           <tfoot class="text-right">
                              <tr>
                                 <td colspan="2">ยอดรวม</td>
                                 <td id="sum_total_st"><?php echo number_format($sum_total_st,2);?></td>
                                 <td id="sum_total_et"><?php echo number_format($sum_total_et,2);?></td>
                                 <td id="sum_total_didcnt"><?php echo $sum_total_didcnt;?></td>
                                 <td id="sum_total_didamt"><?php echo number_format($sum_total_didamt,2);?></td>
                                 <td id="sum_total_widcnt"><?php echo $sum_total_widcnt;?></td>
                                 <td id="sum_total_widamt"><?php echo number_format($sum_total_widamt,2);?></td>
                                 <td id="sum_total_tt" class="<?php echo ($sum_total_tt == 0)? 'green' : 'red';?>"><?php echo number_format($sum_total_tt,2);?></td>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php foreach ($agentlist as $key => $val):?>
            <div class="col-sm-12">
               <div class="widget-box widget-color-green2 ui-sortable-handle">
                  <div class="widget-header">
                     <h5 class="widget-title bigger ">
                        <i class="ace-icon fa fa-pencil-square-o"></i>
                        Agent <?php echo $key;?>
                     </h5>
                  </div>
                  <div class="widget-body">
                     <div class="widget-main no-padding">
                        <div class="table-responsive">
                           <table class="table table-striped table-bordered table-hover text-right">
                              <thead class="thin-border-bottom">
                                 <tr>
                                    <th width="15%">เอเย่นต์</th>
                                    <th>ยอดตั้งต้น</th>
                                    <th>ยอดปัจจุบัน</th>
                                    <th>เครดิตที่ปรับ</th>
                                    <th>รายการฝาก</th>
                                    <th>ยอดฝาก</th>
                                    <th>ยอดโบนัส</th>
                                    <th>รายการถอน</th>
                                    <th>ยอดถอน</th>
                                    <th >+ / -</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php 
                                    $sum_total_mcredit = $sum_total_did_cnt = $sum_total_did_amt = $sum_total_wid_cnt = $sum_total_wid_amt = $sum_total_did_bonus = $sum_total_creditchange = 0;
                                    $sum_total_mcredit_now = 0;
                                    $sum_total_mcredit_total = 0;
                                    $other = array();
                                 ?>
                                 <?php foreach ($val as $k => $v):?>
                                    <?php 

                                       foreach ($otherlist as $it){
                                          if($it->type == 'G'){
                                             $ago = explode('-', $it->bankno);
                                             if($ago[0] == $v->name){
                                                if($it->bf_amount > 0){
                                                   $v->credit_change += $it->bf_amount;
                                                }else{
                                                   $v->credit_change -= $it->bf_amount;
                                                }
                                             }
                                             $other[$ago[0]] = $it->bf_amount;
                                          }
                                       }

                                       // debug($other);

                                       $sum_total_did_cnt += $v->did_cnt;
                                       $sum_total_did_amt += $v->did_amt;
                                       $sum_total_did_bonus += $v->did_bonus;
                                       $sum_total_wid_cnt += $v->wid_cnt;
                                       $sum_total_wid_amt += $v->wid_amt;
                                       $sum_total_mcredit += $v->bf_amount;
                                       $sum_total_mcredit_now += $v->af_amount;
                                       $sum_total_creditchange += $v->credit_change;
                                       $diff =  (($v->af_amount - $v->bf_amount) > 0) ?  ($v->af_amount - $v->bf_amount) - $v->credit_change :  ($v->af_amount - $v->bf_amount) - $v->credit_change;

                                       $sum_total_mcredit_total += $diff;
                                    ?>
                                    <tr>
                                       <td class="text-left"><?php echo $v->name;?></td>
                                       <td><?php echo number_format($v->bf_amount,2);?></td>
                                       <td><?php echo number_format($v->af_amount,2);?></td>
                                       <td><?php echo number_format($v->credit_change,2);?></td>
                                       <td><?php echo $v->did_cnt;?></td>
                                       <?php if($v->did_amt < 0):?>
                                          <td ><?php echo number_format(($v->did_amt - $other[$v->name]) ,2);?>d</td>
                                       <?php else:?>
                                          <td ><?php echo number_format($v->did_amt,2);?></td>
                                       <?php endif;?>
                                       <td><?php echo number_format($v->did_bonus,2);?></td>
                                       <td><?php echo $v->wid_cnt;?></td>
                                       <?php if($v->wid_amt < 0):?>
                                          <td ><?php echo number_format(($v->wid_amt + $other[$v->name]) ,2);?>w</td>
                                       <?php else:?>
                                          <td ><?php echo number_format($v->wid_amt,2);?></td>
                                       <?php endif;?>
                                       <td class="<?php echo ($diff == 0)? 'green' : 'red';?>"><?php echo number_format($diff,2);?></td>
                                    </tr>
                                 <?php endforeach;?>
                              </tbody>
                              <tfoot class="text-right">
                                 <tr>
                                    <td >ยอดรวม</td>
                                    <td id="sum_total_mcredit"><?php echo number_format($sum_total_mcredit,2);?></td>
                                    <td id="sum_total_mcredit_now"><?php echo number_format($sum_total_mcredit_now,2);?></td>
                                    <td id="sum_total_mcredit_change"><?php echo number_format($sum_total_creditchange,2);?></td>
                                    <td id="sum_total_did_cnt"><?php echo $sum_total_did_cnt;?></td>
                                    <td id="sum_total_did_amt"><?php echo number_format($sum_total_did_amt,2);?></td>
                                    <td id="sum_total_did_amt"><?php echo number_format($sum_total_did_bonus,2);?></td>
                                    <td id="sum_total_wid_cnt"><?php echo $sum_total_wid_cnt;?></td>
                                    <td id="sum_total_wid_amt"><?php echo number_format($sum_total_wid_amt,2);?></td>
                                    <td id="sum_total_mcredit_total" class="<?php echo ($sum_total_mcredit_total == 0)? 'green' : 'red';?>"><?php echo number_format($sum_total_mcredit_total,2);?></td>
                                 </tr>
                              </tfoot>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         <?php endforeach;?>
      </div>
   </div>
   <div class="col-md-4 col-sm-12">
      <div class="row">
         <div class="col-sm-12">
            <div class="widget-box widget-color-orange ui-sortable-handle">
               <div class="widget-header">
                  <h5 class="widget-title bigger ">
                     <i class="ace-icon fa fa-pencil-square-o"></i>
                     รายการอื่นๆ
                  </h5>
               </div>
               <div class="widget-body">
                  <div class="widget-main no-padding">
                     <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover  text-right" id="tb_other">
                           <thead class="thin-border-bottom">
                              <tr>
                                 <th>รายการ</th>
                                 <th>เลขบช/Agent</th>
                                 <th>ยอดเงิน + ค่าธรรมเนียม</th>
                                 <th>หมายเหตุ</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php 
                                 $sum_amt = 0;
                                 // $sum_fee = 0;
                              ?>
                              <?php foreach ($otherlist as $item): ?>
                                 <?php 
                                    $sum_amt += $item->bf_amount;
                                    // $sum_fee += $item->fee;
                                    $bankno = explode('-', $item->bankno);
                                 ?> 
                                 <tr>
                                    <td class="other_data" data-row_id="<?php echo $item->id;?>">
                                       <?php if($item->type == 'O'):?>
                                          <?php echo ($item->bf_amount > 0) ? '+ ฝาก' : '- ถอน'; ?>
                                       <?php else:?>
                                          <?php echo ($item->bf_amount > 0) ? '+ Agent' : '- Agent'; ?>
                                       <?php endif;?>
                                    </td>
                                    <td class="other_data" data-row_id="<?php echo $item->id;?>"><?php echo $item->bankid.'-'.$bankno[0]; ?></td>
                                    <td class="other_data" data-row_id="<?php echo $item->id;?>" data-column_name="bf_amount" data-oldval="<?php echo $item->bf_amount;?>" contenteditable><?php echo $item->bf_amount; ?></td>
                                    <td class="other_data" data-row_id="<?php echo $item->id;?>" data-column_name="name" data-oldval="<?php echo $item->name;?>" contenteditable><?php echo $item->name; ?></td>
                                 </tr>
                              <?php endforeach;?>  
                           </tbody>
                           <tfoot>
                              <tr>
                                 <td></td>
                                 <td>ยอดรวม</td>
                                 <td id="sum_input_amt"><?php echo number_format($sum_amt,2);?></td>
                                 <td></td>
                                 <td></td>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   function new_period(){
      location.href = '<?php echo base_url();?>balance/new_period';
   }

   function cal_period(date){
      location.href = '<?php echo base_url();?>balance/cal_period/'+date;
   }

   function close_period(date){
      if(confirm('ต้องการปิดยอดวันที่ '+date+ ' ?')){
         location.href = '<?php echo base_url();?>balance/close_period/'+date;
      }
   }

   function showlist(id){
      if(id == '+B' || id == '-B'){
         $('#input_bank').show();
         $('#input_ag').hide();
      }else{
         $('#input_bank').hide();
         $('#input_ag').show();
      }
      console.log(id);
   }
   // get_cal_period();
   // function get_cal_period(){
   //    var cd  = $('#cdate').val();
   //    if(cd == '')
   //    {
   //       alert('no period');
   //       return false;
   //    }
   //    $.ajax({
   //       url : "<?php echo base_url(); ?>balance/get_cal_period",
   //       type : "GET",
   //       dataType : 'json',
   //       data :{ 
   //          cdate  : cd, 
   //       },
   //       success:function(res){
   //          console.log(res);
   //       }
   //    });
   // }

   function loadother()
   {
      $.ajax({
         url : "<?php echo base_url(); ?>balance/loadother",
         type : "POST",
         dataType : 'json',
         data :{ 
            date  : '<?php echo $bldate;?>', 
         },
         success : function(res){
            var tb = '';
            var sum_amt = 0;
            var sum_fee = 0;
            if(res.status){
               // tb += `<tr>
               //          <td width="30%">
               //             <select class="form-control"  name="input_bank" id="input_bank">
               //                <option value="">-- กรุณาเลือก --</option>
               //                <?php foreach ($bankidlist as $k => $v):?>
               //                   <option value="<?php echo $v['bankid'].'-'.$v['bankno'];?>">
               //                      <?php echo $v['bankshow'];?>   
               //                   </option>
               //                <?php endforeach;?>
               //             </select>
               //          </td>
               //          <td width="30%" contenteditable>
               //             <select class="form-control"  name="input_list" id="input_list">
               //                <option value="">-- กรุณาเลือก --</option>
               //                <option value="+">ฝาก</option>
               //                <option value="-">ถอน</option>
               //             </select>
               //          </td>
               //          <td width="20%" id="input_amt"  contenteditable></td>
               //          <td width="20%" id="input_note"  contenteditable></td>
               //          <td>
               //             <div class="btn-group">
               //                <button type="button" class="fa fa-check btn btn-minier btn-success" onclick="addother();"> เพิ่ม</button>
               //             </div>
               //          </td>
               //       </tr>`;
               res.data.forEach(function(k,v){
                  sum_amt += parseFloat(k.bf_amount);
                  sum_fee += parseFloat(k.fee);
                  var x = '';
                  if(k.type == 'O'){
                     x = (k.bf_amount > 0) ? '+ ฝาก' : '- ถอน';
                  }else{
                     x = (k.bf_amount > 0) ? '+ Agent' : '- Agent';
                  }
                  tb += `<tr>
                           <td class="other_data" data-row_id="${k.id}">
                              ${x}
                           </td>
                           <td class="other_data" data-row_id="${k.id}">${k.bankid}-${k.bankno}</td>
                           <td class="other_data" data-row_id="${k.id}" data-column_name="bf_amount" data-oldval="${k.bf_amount}" contenteditable>${k.bf_amount}</td>
                           <td class="other_data" data-row_id="${k.id}" data-column_name="input_note" data-oldval="${k.name}" contenteditable>${k.name}</td>
                           <td>
                              <div class="btn-group">
                                 <button type="button" class="fa fa-trash-o btn btn-minier btn-danger" onclick="deldata(this,'${k.id}','${k.bankid}-${k.bankno}');"> ลบ</button>
                              </div>
                           </td>
                        </tr>`;
               });
            }
            $('#tb_other tbody').html((tb == '')? '<tr><td  colspan="7" class="text-danger text-center"> ไม่มีข้อมูล. </td></tr>' : tb);

            $('#sum_input_amt').text(numberWithCommas(parseFloat(sum_amt).toFixed(2)));
            // $('#sum_input_fee').text(numberWithCommas(parseFloat(sum_fee).toFixed(2)));
         }
      });
   }

   function addother(){
      var input_bank  = $('#input_bank').val();
      var input_ag    = $('#input_ag').val();
      var input_list  = $('#input_list').val();
      var input_amt   = $('#input_amt').text();
      var input_note   = $('#input_note').text();
      if(input_list == '')
      {
         $('#input_list').focus();
         return false;
      }
      if(input_amt == '')
      {
         $('#input_amt').focus();
         return false;
      }
      $.ajax({
         url : "<?php echo base_url(); ?>balance/addother",
         type : "POST",
         dataType : 'json',
         data :{ 
            bank  : input_bank, 
            ag    : input_ag, 
            name  : input_note, 
            bf_amount   : (input_list == '+B' || input_list == '+A')? input_amt : '-' + input_amt,
         },
         success:function(res){
            if(res.status){
               loadother();
               $('#input_list').val('');
               $('#input_amt').text('');
               $('#input_note').text('');
               $('#input_bank').val('').hide();
               $('#input_ag').val('').hide();
            }else{
               alert(res.msg);
            }
         }
      });
   }
   function addbank(){
      var input_list  = $('#input_list').val();
      var input_amt   = $('#input_amt').text();
      if(input_list == '')
      {
         $('#input_list').focus();
         return false;
      }
      if(input_amt == '')
      {
         $('#input_amt').focus();
         return false;
      }
      $.ajax({
         url : "<?php echo base_url(); ?>balance/addother",
         type : "POST",
         dataType : 'json',
         data :{ 
            name  : input_note, 
            bf_amount   : input_amt,
         },
         success:function(res){
            if(res.status){
               // searchdata();
               // location.reload();
            }else{
               alert(res.msg);
            }
         }
      });
   }
   function deldata(th,id,name){
      if(confirm("ต้องการลบ "+name+" ?")){
         $.ajax({
            url  : "<?php echo base_url(); ?>balance/delete/"+id,
            type : "POST",
            dataType: 'json',
            success:function(res){
               if(res.status){
                  // location.reload();
                  $(th).closest("tr").remove();
               }else{
                  alert(res.msg);
               }
            }
         });
      }
   }
   $(document).on('blur', '.other_data', function(){
      var id = $(this).data('row_id');
      var table_column = $(this).data('column_name');
      var oldval = $(this).data('oldval');
      var value = $(this).text();
      if(value != oldval){
         $(this).data('oldval',value);
         $.ajax({
            type : "POST",
            url  : "<?php echo base_url(); ?>balance/update/"+id,
            data : {
               oldval : oldval,
               table_column : table_column,
               value : value
            },
            dataType: 'json',
            success:function(res)
            {
               if(res.status){
                  // location.reload();
                  loadother();
               }else{
                  alert(res.msg);
               }
            }
         });
      }
   });
</script>
