<div class="row">
    <?php 
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
    ?>
    <?php if ($error) :?>
        <div class="pull-left alert alert-danger no-margin alert-dismissable">
            <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>
            <?php echo $error; ?>
        </div>
    <?php endif;?>
    <?php if ($success) :?>
        <div class="pull-left alert alert-success no-margin alert-dismissable">
            <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>
            <?php echo $success; ?>
        </div>
    <?php endif;?>
    <div class="col-xs-12">
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo (isset($data->id)) ? base_url('/customer/update/' . $data->id) : base_url('/customer/create'); ?>">
            <div class="clearfix">
                <div class="pull-right">
                <button class="btn btn-sm btn-success" type="submit">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    บันทึก
                </button>
                &nbsp; &nbsp;
                <a type="button" class="btn btn-sm" onclick="goBack();">ย้อนกลับ</a>
                </div>
            </div>
            <div class="form-group">
                <?php if(isset($data)):?>
                    <input type="hidden" name="oldval" value="<?php echo htmlspecialchars(json_encode($data)); ?>" />
                <?php endif;?>
                <label class="col-md-2 control-label" for="cdate">วันที่สมัคร</label>
                <div class="col-sm-6">
                    <div class="input-group">
                        <input class="form-control date-picker" name="cdate" id="cdate" type="text" data-date-format="dd-mm-yyyy" value="<?php echo (isset($data->cdate)) ? date('d-m-Y',strtotime($data->cdate))  : ''; ?>">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">ชื่อลูกค้า</label>
                <div class="col-md-3">
                    <input type="text" class="form-control" name="name" placeholder="ชื่อ" value="<?php echo (isset($data->name)) ? $data->name : ''; ?>">
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" name="lname" placeholder="นามสกุล" value="<?php echo (isset($data->lname)) ? $data->lname : ''; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="tel" class="col-md-2 control-label">เบอร์ติดต่อ</label>
                <div class="col-md-6">
                    <input type="text" class="form-control input-mask-phone" name="tel" placeholder="เบอร์ติดต่อ" value="<?php echo (isset($data->tel)) ? $data->tel : ''; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="lineid" class="col-md-2 control-label">ไลน์ไอดี</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="lineid" placeholder="ไลน์ไอดี" value="<?php echo (isset($data->lineid)) ? $data->lineid : ''; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="fromdata" class="col-md-2 control-label">ช่องทางสมัคร</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="fromdata" placeholder="ช่องทางสมัคร" value="<?php echo (isset($data->fromdata)) ? $data->fromdata : ''; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="bankid" class="col-md-2 control-label">ธนาคาร</label>
                <div class="col-md-3">
                    <?php if (isset($data->bankid)) :?>
                        <select class="form-control" id="form-field-select-1" name="bankid">
                            <?php foreach ($banklist as $k => $v):?>
                                <?php if($k == 0):?>
                                    <option value="0">เลือกธนาคาร</option>
                                <?php else:?>
                                    <option value="<?php echo $k;?>" <?php echo ($data->webid == $k) ? 'selected' : ''; ?>><?php echo $v;?></option>
                                <?php endif;?>
                            <?php endforeach;?>
                        </select>
                    <?php else :?>
                        <select class="form-control" id="form-field-select-1" name="bankid">
                            <?php foreach ($banklist as $k => $v):?>
                                <?php if($k == 0):?>
                                    <option value="0">เลือกธนาคาร</option>
                                <?php else:?>
                                    <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                <?php endif;?>
                            <?php endforeach;?>
                        </select>
                    <?php endif;?>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" name="bankno" placeholder="เลขที่บัญชี" value="<?php echo (isset($data->bankno)) ? $data->bankno : ''; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="webid" class="col-md-2 control-label">web</label>
                <div class="col-md-6">
                    <?php if (isset($data->webid)) :?>
                        <select class="form-control" id="form-field-select-1" name="webid">
                            <?php foreach ($sitelist as $k => $v):?>
                                <?php if($k == 0):?>
                                    <option value="0">เลือกเว็บ</option>
                                <?php else:?>
                                    <option value="<?php echo $k;?>" <?php echo ($data->webid == $k) ? 'selected' : ''; ?>><?php echo $v;?></option>
                                <?php endif;?>
                            <?php endforeach;?>
                        </select>
                    <?php else :?>
                        <select class="form-control" id="form-field-select-1" name="webid">
                            <?php foreach ($sitelist as $k => $v):?>
                                <?php if($k == 0):?>
                                    <option value="0">เลือกเว็บ</option>
                                <?php else:?>
                                    <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                <?php endif;?>
                            <?php endforeach;?>
                        </select>
                    <?php endif;?>
                </div>
            </div>
            <div class="form-group">
                <label for="other" class="col-md-2 control-label">หมายเหตุ</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="other" placeholder="หมายเหตุ" value="<?php echo (isset($data->other)) ? $data->other : ''; ?>">
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $('.input-mask-phone').mask('999-999-9999');
</script>