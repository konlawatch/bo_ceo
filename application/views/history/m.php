<link rel="stylesheet" href="<?php echo base_url();?>assets/css/daterangepicker.min.css" />
<style type="text/css">
   .table-hover>tbody>tr:hover{
      color: #000;
   }
   .tb_1_4 {
      background-color: #87B87Fe7;
      /*color: #fff;*/
   }
   .tb_2_4 {
      background-color: #FFB752;
      /*color: #fff;*/
   }
   .tb_3_4{
      background-color: #87b87f99;
      /*color: #fff;*/
   }
   .tb_4_4{
      background-color: #ffb7529c;
      /*color: #fff;*/
   }
   .btn_1_4 {
      background-color: #87B87Fe7!important;
      border-color: #87B87Fe7!important;
      color: #fff!important;
      border-width: 1px;
      text-shadow: none!important;
   }
   .btn_2_4 {
      background-color: #FFB752!important;
      border-color: #FFB752!important;
      color: #fff!important;
      border-width: 1px;
      text-shadow: none!important;
   }
   .btn_3_4 {
      background-color: #87b87f99!important;
      border-color: #87b87f99!important;
      color: #fff!important;
      border-width: 1px;
      text-shadow: none!important;
   }
   .btn_4_4 {
      background-color: #ffb7529c!important;
      border-color: #ffb7529c!important;
      color: #fff!important;
      border-width: 1px;
      text-shadow: none!important;
   }
   .tb_2_6,.tb_1_6 {
      background-color: #f44336;
      color: #fff;
   }
</style>
<div class="row">
   <div class="col-xs-12">
      <div class="row">
         <div class="col-md-12">
            <form class="form-inline">
               <select class="form-control col-md-3" name="web1" id="web1" onchange="get_agent_byweb(this.value,'1');">
                <?php foreach ($sitelist as $k => $v):?>
                  <?php if($k == 0):?>
                      <option value="0">-- เลือกเว็บ --</option>
                  <?php else:?>
                      <option value="<?php echo $v;?>" <?php echo ('TS911' == $v) ? '' : ''; ?>><?php echo $v;?></option>
                  <?php endif;?>
                <?php endforeach;?>
               </select>
               <select class="form-control col-md-3" id="ag1" name="ag1">
                  <option value="">-- เลือกเอเย่นต์ --</option>
                  <?php foreach ($aglist as $k):?>
                    <option value="<?php echo $k['user'];?>"><?php echo $k['user'];?></option>
                  <?php endforeach;?>
               </select>
               <div class="form-group col-sm-3">
                  <div class="input-daterange input-group">
                     <input type="text" class="input-sm form-control" name="st_cdate" id="st_cdate" value="<?php echo date('d-m-Y',strtotime($bdate));?>" />
                     <span class="input-group-addon">
                        <i class="fa fa-exchange"></i>
                     </span>
                     <input type="text" class="input-sm form-control" name="et_cdate" id="et_cdate" value="<?php echo date('d-m-Y',strtotime($bdate));?>" />
                  </div>
               </div>
               <input type="text" class="input-sm" name="userid" id="userid" placeholder="Username"> &nbsp;
               <select class="form-control input-sm" id="type">
                  <option value="">ทั้งหมด</option>
                  <option value="1">ฝาก</option>
                  <option value="2">ถอน</option>
               </select>
               <label>จำนวนแถว : </label>
               <select class="form-control input-sm" id="per_page">
                  <option value="300">300</option>
                  <option value="500">500</option>
                  <option value="1000">1000</option>
                  <option value="2000">2000</option>
                  <option value="3000">3000</option>
               </select>
               <button type="button" class="btn btn-info btn-sm" onclick="searchdata();">
                  <i class="ace-icon fa fa-search bigger-110"></i>ค้นหา
               </button>
               <button type="reset" class="btn btn-warning btn-sm">
                  <i class="ace-icon fa fa-refresh bigger-110"></i>
               </button> | 
               <button type="button" class="btn btn_1_4 btn-sm">ฝาก</button>
               <button type="button" class="btn btn_2_4 btn-sm">ถอน</button>
               <button type="button" class="btn btn_3_4 btn-sm">+โบนัส</button>
               <button type="button" class="btn btn_4_4 btn-sm">-โบนัส</button>
            </form>
         </div>
      </div>
      <div class="hr dotted"></div>
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                           <th class="text-center">ลำดับ</th>
                           <th class="text-center">ยอดวันที่</th>
                           <th class="text-center">เว็บ</th>
                           <th class="text-center">ชื่อลูกค้า</th>
                           <th class="text-center">ยูสเซอร์</th>
                           <th class="text-center">ยอดฝาก</th>
                           <th class="text-center">โบนัส</th>
                           <th class="text-center">ยอดถอน</th>
                           <th class="text-center">วันที่</th>
                           <th class="text-center">เวลา</th>
                           <th class="text-center">จากบัญชี</th>
                           <th class="text-center">เข้าบัญชี</th>
                           <th class="text-center">สร้างโดย</th>
                           <th class="text-center">เครดิตก่อน</th>
                           <th class="text-center">ยอดทราน</th>
                           <th class="text-center">เครดิตหลัง</th>
                           <th class="text-center">ปรับเครดิต</th>
                           <th class="text-center">เช็คแบงค์/โอนเงิน</th>
                           <th class="text-center">แจ้งเตือน</th>
                           <th class="text-center">หมายเหตุ</th>
                        </tr>
                     </thead>
                     <tbody>
                        
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="<?php echo base_url();?>assets/js/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/js/daterangepicker.min.js"></script>
<script type="text/javascript">
   $('.input-daterange').datepicker({
      autoclose : true,
      format : 'dd-mm-yyyy'
   });
   var page  = 0; 
   var company = '<?php echo $_GET['company'];?>';
   searchdata();
   function searchdata(){
      $('#loading').show();
      $.ajax({
         type: "POST",
         url: '<?php echo base_url();?>history/s',  
         data: {
            company      : company,
            web      : $('#web1').val(),
            ag       : $('#ag1').val(),
            st       : $('#st_cdate').val(),
            et       : $('#et_cdate').val(),
            type     : $('#type').val(),
            userid   : $('#userid').val(),
            page     : page,
            per_page : $('#per_page').val(),
         },
         dataType: 'json',
         success: function(res,s,y){
            if(res.status){
               $('#dataTable tbody').html(res.data);
            }else{
               alert(res.msg);
            }
            $('#loading').hide();
         },
      });
   }
   function get_agent_byweb(val,i){
        $('#ag'+i).html('<option value="0">-- เลือกเอเย่นต์ --</option>');
        if(val != '0'){
            $.ajax({
                type: "GET",
                url: '<?php echo base_url();?>agent/get_agent_byweb',  
                data: {
                    company     : company,
                    web     : val,
                },
                dataType: 'json',
                success: function(res){
                    if(res.status){
                        var sl = '<option value="0">-- เลือกเอเย่นต์ --</option>';

                        res.data.forEach(function(k,v){
                            sl +=`<option value="${k.user}">${k.user}</option>`;
                        });
                        $('#ag'+i).html(sl);
                    }else{
                        // alert(res.msg);
                    }
                },
            });
        }
    }
</script>
