<?php 
   $i = ($this->uri->segment(3) != '') ? (($this->uri->segment(3) - 1) * 50) + 1 : 1;
   $sum_dep = $sum_bo = $sum_wid = $sum_com = 0;
?>
<?php if (isset($data) && count($data) >= 1): ?>
   <?php foreach ($data as $item): ?>
         <?php 
            $modal_data = array();
            if($item->type == '1' || $item->type == '3'){
               $sum_dep += $item->amount;
               $sum_bo += $item->bvalue;
            }else{
               $sum_wid += $item->amount;
            }
            $modal_data = array(
               'name'       => $item->name,
               'userid'     => $item->userid,
               'amount'     => $item->amount,
               'frombankid' => $item->frombankid,
               'frombank'   => $item->frombank,
               'tobankid'   => $item->tobankid,
               'tobank'     => $item->tobank,
               'date'       => date('d-m-Y',strtotime($item->cdate)),
               'time'       => $item->ctime,
            );
         ?>
         <tr class="tb_<?php echo $item->type.'_'.$item->status;?>">
            <td><?php echo $i; ?></td>
            <td class="text-center"><?php echo ($item->bdate != '0000-00-00') ? date('d-m-Y',strtotime($item->bdate)) : '-'; ?></td>
            <td><?php echo $item->web; ?></td>
            <td><?php echo $item->name; ?></td>
            <td><?php echo $item->userid; ?></td>
            <td class="text-right"><?php echo ($item->type == '1' || $item->type == '3')? number_format($item->amount,2) : '-'; ?></td>
            <td class="text-right"><?php echo ($item->bvalue > 0)? $item->bvalue : '-'; ?></td>
            <td class="text-right"><?php echo ($item->type == '2' || $item->type == '4')? number_format($item->amount,2) : '-'; ?></td>
            <td class="text-center"><?php echo date('d-m-Y',strtotime($item->cdate)); ?></td>
            <td><?php echo $item->ctime; ?></td>
            <td class="text-left"><?php echo ($item->frombankid != '')? '<img src="'.base_url().'assets/images/banklogo/'.$item->frombankid.'.png" alt="'.$item->frombankid.'" class="img_banklogo"> '.$item->frombankid.'-'.$item->frombank : $item->frombank; ?></td>
            <td class="text-left"><?php echo ($item->tobankid != '')? '<img src="'.base_url().'assets/images/banklogo/'.$item->tobankid.'.png" alt="'.$item->tobankid.'" class="img_banklogo"> '.$item->tobankid.'-'.$item->tobank : $item->tobank; ?></td>
            <td><?php echo $item->created_by; ?></td>
            <td class="text-right"><?php echo number_format($item->bf_credit,2); ?></td>
            <td class="text-right"><?php echo number_format($item->tf_credit,2); ?></td>
            <td class="text-right"><?php echo number_format($item->af_credit,2); ?></td>
            <td id="<?php echo $item->id;?>_upcredit">
               <?php if($item->status == '1'):?>
                  รอปรับเครดิต
               <?php else:?>
                  <?php echo $item->credit_by;?>
               <?php endif;?>
            </td>
            <td>
               <?php if($item->status == '2'):?>
                  <?php if($item->type == '1'):?>
                    รอเช็คแบงค์
                  <?php elseif($item->type == '2'):?>
                    รอโอนเงิน
                  <?php endif;?>
               <?php else:?>
                  <?php echo $item->bank_by;?>
               <?php endif;?>
            </td>
            <td id="<?php echo $item->id;?>_msg">
               <?php if($item->status == '3'):?>
                  <i class="ace-icon fa fa-spinner fa-spin red bigger-125"></i>
               <?php else:?>
                  <?php echo ($item->type == '4') ? $item->other : $item->bot_msg; ?>
               <?php endif;?>
            </td>
            <td>
               <?php echo $item->other;?>
            </td>
         </tr>
      <?php $i++;?>
   <?php endforeach;?>
   <?php $wl = $sum_dep - $sum_wid;?>
   <tr>
      <td colspan="5" class="text-right">ยอดรวม</td>
      <td id="sum_dep" class="text-right"><?php echo number_format($sum_dep,2);?></td>
      <td id="sum_bo" class="text-right"><?php echo number_format($sum_bo,2);?></td>
      <td id="sum_wid" class="text-right"><?php echo number_format($sum_wid,2);?></td>
      <td class="text-right <?php echo ($wl < 0) ? 'red': '';?>"><?php echo number_format($wl,2);?></td>
      <td colspan="14"></td>
   </tr>
   <?php else: ?>
      <tr>
         <td  colspan="20" class="text-danger text-center"> ไม่มีข้อมูล. </td>
      </tr>
<?php endif;?>