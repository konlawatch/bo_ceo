<div class="row">
   <div class="col-xs-12">
      <div class="clearfix">
         <div class="pull-right">
            <a class="btn btn-sm btn-success pull-right" href="<?php echo base_url(); ?>acclist/add"><i class="fa fa-plus"></i> เพิ่มผู้ใช้งาน</a>
         </div>
      </div>
      <div class="hr dotted"></div>
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                           <th>ลำดับ</th>
                           <th>รหัสผู้ใช้งาน</th>
                           <th>ชื่อผู้ใช้งาน</th>
                           <th>สิทธิการใช้งาน</th>
                           <th>จัดการ</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php $i = ($this->uri->segment(3) != '') ? (($this->uri->segment(3) - 1) * 50) + 1 : 1;?>
                        <?php if (isset($data) && count($data) >= 1): ?>
                           <?php foreach ($data as $item): ?>
                                 <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $item->username; ?></td>
                                    <td><?php echo $item->name; ?></td>
                                    <td><?php echo $item->role; ?></td>
                                    <td>
                                       <div class="btn-group">
                                          <a class="fa fa-pencil-square-o btn btn-minier btn-warning" href="<?php echo base_url('acclist/edit/' . $item->userId); ?>"> แก้ไข</a>
                                          <a class="fa fa-trash-o btn-minier btn btn-danger" onclick="return confirm('ต้องการลบ <?php echo $item->name; ?> ?')" href="<?php echo base_url('acclist/delete/' . $item->userId); ?>"> ลบ</a>
                                       </div>
                                   </td>
                              </tr>
                              <?php $i++;?>
                           <?php endforeach;?>
                           <?php else: ?>
                              <tr>
                                 <td  colspan="10" class="text-danger text-center"> ไม่มีข้อมูล. </td>
                              </tr>
                        <?php endif;?>
                     </tbody>
                  </table>
               </div>
               <div class="col-md-12" style="text-align:center;"><?php echo $pagination; ?></div>
            </div>
         </div>
      </div>
   </div>
</div>
