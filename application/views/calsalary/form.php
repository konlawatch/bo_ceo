
<div class="row">
    <div class="col-xs-12">
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="#">

            <div class="form-group">
                <?php if(isset($data)):?>
                    <input type="hidden" name="old_val" value="<?php echo htmlspecialchars(json_encode($data)); ?>" />
                <?php endif;?>
            </div>

           <!--  <div class="form-group">
                <label for="cstatus" class="col-md-2 control-label"> ชื่อบริษัท <span class="text-danger">*</span></label>
                <div class="col-md-10">

                    <?php if (isset($data->com_name)) :?>
                        <select class="form-control" id="status" name="status" onchange="sd();" readonly>
                          <option value="0">กรุณาเลือกชื่อบริษัท</option>
                            <?php foreach ($company as $k => $v):?>
                                    <option value="<?php echo $v['id'];?>" <?php echo ($data->com_name == $v['name']) ? 'selected' : ''; ?>><?php echo $v['name'];?></option>       
                            <?php endforeach;?>
                        </select>
                    <?php else :?>
                        <select class="form-control" id="status" name="status" onchange="sd();" readonly>
                            <option value="0">กรุณาเลือกชื่อบริษัท</option>
                            <?php foreach ($company as $k => $v):?>
                                    <option value="<?php echo $v['id'];?>"><?php echo $v['name'];?></option>     
                            <?php endforeach;?>
                        </select>
                    <?php endif;?>
                    
                </div>
            </div> -->

            <!-- <div class="form-group">
                <label for="depart" class="col-md-2 control-label"> ชื่อแผนก <span class="text-danger">*</span></label>
                <div class="col-md-10">
                     <select class="form-control" id="depart" name="depart" onchange="positions()" readonly>
                        <option value="<?php echo (isset($data->depart_id)) ? $data->depart_id : ''; ?>"><?php echo (isset($data->depart_name)) ? $data->depart_name : '-- กรุณาเลือกชื่อแผนก --'; ?></option>
                     </select>
                </div>
            </div>

            <div class="form-group">
                <label for="positions" class="col-md-2 control-label">ชื่อตำแหน่ง <span class="text-danger" >*</span></label>
                <div class="col-md-10">
                    <select class="form-control" id="posi" name="posi" readonly>
                        <option value="<?php echo (isset($data->posi_id)) ? $data->posi_id : ''; ?>"><?php echo (isset($data->posi_name)) ? $data->posi_name : '-- กรุณาเลือกตำแหน่ง --'; ?></option>
                     </select> 
                </div>
            </div> -->
            <div class="form-group">
                <label for="title" class="col-md-2 control-label"> ชื่อ-นามสกุล <span class="text-danger">*</span></label>
                <div class="col-md-10">  
                    <input type="text" class="form-control" name="title" id="title" value="<?php echo (isset($data->name)) ? $data->name : ''; ?>" readonly>
                </div>
            </div>

            <div class="form-group">
                <label for="cstatus" class="col-md-2 control-label">สถานะการลา <span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <select class="form-control" id="desc" name="desc" onchange="sdw();">
                        <option value="">กรุณาเลือกสถานะ</option>
                        <?php foreach ($status_w as $k => $v):?>
                                <option value="<?php echo $v;?>"><?php echo $v;?></option>     
                        <?php endforeach;?>
                    </select>
                </div>

            </div>

           <!--  <?php if (isset($data->desc)) :?>
                        <select class="form-control" id="desc" name="desc">
                        <option value="<?php echo (isset($data->desc)) ? $data->desc : ''; ?>"><?php echo (isset($data->desc)) ? $data->desc : '-- กรุณาเลือกชื่อแผนก --'; ?></option>
                        <?php foreach ($status_w as $k => $v):?>
                               <option value="<?php echo $k;?>"><?php echo $v;?></option>       
                        <?php endforeach;?>
                    </select>
                    <?php else :?>
                        <select class="form-control" id="desc" name="desc">
                        <option value="0">กรุณาเลือกสถานะ2</option>
                        <?php foreach ($status_w as $k => $v):?>
                                <option value="<?php echo $k;?>"><?php echo $v;?></option>     
                        <?php endforeach;?>
                    </select>
                    <?php endif;?> -->

            <div class="form-group">
                <label for="desc" class="col-md-2 control-label"> จำนวนเงินที่หัก <span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="amount" id="amount" value="<?php echo (isset($data->amount)) ? $data->amount : ''; ?>">
                    
                </div>
            </div>

           <!--  <div class="form-group">
                <label for="desc" class="col-md-2 control-label">ชื่อเล่น <span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="desc" id="desc" value="<?php echo (isset($data->desc)) ? $data->desc : ''; ?>" readonly>
                    
                </div>
            </div> -->
            <!-- <div class="form-group">
                <label for="tel" class="col-md-2 control-label"> เบอร์โทรศัพท์ <span class="text-danger">*</span></label>
                <div class="col-md-10">
                    
                    <input type="text" class="form-control" name="tel" id="tel" value="<?php echo (isset($data->tel)) ? $data->tel : ''; ?>" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="starting_date" class="col-md-2 control-label"> วันที่เริ่มงาน <span class="text-danger">*</span></label>
                <div class="col-md-10">
                    
                    <input type="date" class="form-control" name="starting_date" id="starting_date" value="<?php echo (isset($data->starting_date)) ? $data->starting_date : ''; ?>" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="cstatus" class="col-md-2 control-label">สถานะการลา <span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <select class="form-control" id="status_w" name="status_w" onchange="sdw();">
                        <option value="0">กรุณาเลือกสถานะ</option>
                        <?php foreach ($status_w as $k => $v):?>
                                <option value="<?php echo $k;?>"><?php echo $v;?></option>     
                        <?php endforeach;?>
                    </select>
                </div>
            </div> -->
            <div class="form-group" id="a" style="display: none;">
                <label for="date" class="col-md-2 control-label"> วันที่ <span class="text-danger">*</span></label>
                <div class="col-md-10">
                    
                    <input type="date" class="form-control" name="date1" id="date1" value="">
                </div>
                <label for="date" class="col-md-2 control-label"> ถึงวันที่ <span class="text-danger">*</span></label>
                <div class="col-md-10">
                    
                    <input type="date" class="form-control" name="date2" id="date2" value="">
                </div>
            </div>

            <div class="form-group" id="b" style="display: none;">
                <label for="time" class="col-md-2 control-label"> เวลา <span class="text-danger">*</span></label>
                <div class="col-md-10">
                    
                    <input type="text" class="form-control" name="time" id="time" value="">
                </div>
            </div>

            <!-- <div class="form-group" id="c" style="display: none;">
                <label for="file" class="col-md-2 control-label">รูปภาพ</label>
                <div class="col-md-10">
                    <div class="input-group">
                        <input type="file" name="img" class="file" accept="image/*" id="fi">
                        <input type="text" class="form-control" disabled placeholder="อัพโหลดรูปภาพ..." id="file">
                        <span class="input-group-addon " onclick="browse_file();">เลือก...</span>
                    </div>
                    <img src="<?php echo (isset($data->img_url)) ? base_url().'assets/uploads/'.$data->img_url : ''; ?>" id="preview" class="img-thumbnail">
                </div>
            </div> -->
           
        </form>
    </div>
</div>
<script type="text/javascript">

    function sdw(){
         
        //var data_de = $('#status :selected').text();
        var x = document.getElementById("desc").value;
        //alert(x);
        switch(x) {
          case "ขาด":
            document.getElementById("a").style.display = "none";
            document.getElementById("b").style.display = "none";
            // document.getElementById("c").style.display = "none";
            break;
          case "ลากิจ":
            document.getElementById("a").style.display = "none";
            document.getElementById("b").style.display = "none";
            // document.getElementById("c").style.display = "none";
            break;
          case "มาสาย":
            document.getElementById("a").style.display = "none";
            document.getElementById("b").style.display = "block";
            // document.getElementById("c").style.display = "none";
            break;
          case "ลาป่วย":
            document.getElementById("a").style.display = "block";
            document.getElementById("b").style.display = "none";
            // document.getElementById("c").style.display = "block";
            break;
          case "พักร้อน":
            document.getElementById("a").style.display = "block";
            document.getElementById("b").style.display = "none";
            // document.getElementById("c").style.display = "none";
            break;
          default:
            // code block
}
    }
    
    function browse_file(){
        $('.file').click();
    }

    function clear_form(){
        $('#preview').attr('src','');
    }

    $('input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $("#file").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
            // get loaded data and render thumbnail.
            // document.getElementById("preview").src = e.target.result;
            $('#preview').attr('src',e.target.result);
        };
        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });

    // CKEDITOR.replace('desc', {
    //   // Define the toolbar groups as it is a more accessible solution.
    //   toolbarGroups: [{
    //       "name": "basicstyles",
    //       "groups": ["basicstyles"]
    //     },
    //     {
    //       "name": "paragraph",
    //       "groups": ["list", "blocks"]
    //     },
    //     {
    //       "name": "document",
    //       "groups": ["mode"]
    //     },
    //     {
    //       "name": "styles",
    //       "groups": ["styles"]
    //     },
    //   ],
    //   allowedContent : true,
    //   // Remove the redundant buttons from toolbar groups defined above.
    //   removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar,PasteFromWord'
    // });
</script>