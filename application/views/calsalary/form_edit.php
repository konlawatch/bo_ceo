<?php //debug($data1);?>
<div class="row">
    <div class="col-xs-12">

        <div class="row">
            <div class="col-md-10">
                <form class="form-inline" id="search1_from">
                   <!-- <input type="text" class="input-sm" name="qtitle" id="qtitle" placeholder="หัวเรื่อง"> &nbsp; -->
                    <input type="hidden" class="input-sm" name="name" id="name" value="<?php echo (isset($data->name)) ? $data->name : ''; ?>">
                    <label>วันที่ : </label>
                    <input type="date" class="form-control" name="date1" id="date1" value="">
                    <label>ถึงวันที่ : </label>
                    <input type="date" class="form-control" name="date2" id="date2" value="">
                    <button type="button" class="btn btn-info btn-sm" onclick="search_data1();">
                        <i class="ace-icon fa fa-search bigger-110"></i>Search
                    </button>
                </form>
            </div>
        </div><br>

        
       
        <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <div class="table-responsive">
                  <table class="table table-bordered text-left table-hover" id="dataTable1" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                           <th class="text-center">ลำดับ</th>
                           <th class="text-center">ชื่อ-นามสกุล</th>
                           <th class="text-center">สถานะ</th>
                           <th class="text-center">จำนวนเงินที่หัก</th>
                           <th class="text-center">วันที่ลา</th>
                           <th class="text-center"></th>
                          
                        </tr>
                     </thead>
                     <tbody id="tb_contents">
                       
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
        </div>
    </div>
</div>
<script type="text/javascript">



    function search_data1(){
      $.ajax({
         type: "POST",
         url: route+'/search_data',  
         data: $("#search1_from").serializeArray(),
         dataType: 'json',
         success: function(res,s,y){
            if(res.status){
               $('#dataTable1 tbody').html(res.data);
            }else{
               alert(res.msg);
            }
         },
      });
    }

   
    
    
    function browse_file(){
        $('.file').click();
    }

    function clear_form(){
        $('#preview').attr('src','');
    }

    $('input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $("#file").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
            // get loaded data and render thumbnail.
            // document.getElementById("preview").src = e.target.result;
            $('#preview').attr('src',e.target.result);
        };
        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });

    // CKEDITOR.replace('desc', {
    //   // Define the toolbar groups as it is a more accessible solution.
    //   toolbarGroups: [{
    //       "name": "basicstyles",
    //       "groups": ["basicstyles"]
    //     },
    //     {
    //       "name": "paragraph",
    //       "groups": ["list", "blocks"]
    //     },
    //     {
    //       "name": "document",
    //       "groups": ["mode"]
    //     },
    //     {
    //       "name": "styles",
    //       "groups": ["styles"]
    //     },
    //   ],
    //   allowedContent : true,
    //   // Remove the redundant buttons from toolbar groups defined above.
    //   removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar,PasteFromWord'
    // });
</script>