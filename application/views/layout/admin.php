<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="robots" content="noindex, nofollow">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.png">
        <title><?php echo $this->config->config['website'];?> | Admin System Log in</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fonts.googleapis.com.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/chosen.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.gritter.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace-skins.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker3.min.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/daterangepicker.min.css" />
        
        <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/ace-elements.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/ace.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/ace-extra.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/autoNumeric-1.5.4.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.gritter.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootbox.js"></script>
        <script type="text/javascript">
            var baseURL = "<?php echo base_url(); ?>";
            function goBack() {
                window.history.back();
            }
            function show_alert(title,msg,type){
                $.gritter.add({
                    title: title,
                    text: msg,
                    class_name: 'gritter-'+type 
                });
                return false;
            }
        </script>
        <style type="text/css">
            body {
                font-size: 13px;
                /*font-family: "Kanit" !important;*/
            }
            h1, h2, h3, h4, h5, h6 {
                font-family: "Kanit" !important;
            }
            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
                padding: 5px;
                vertical-align: middle;
            }
            .table>tfoot>tr>td {
                font-weight: 500;
            }
            .table>thead>tr>th {
                text-align: center;
            }
            .btn-sm {
                padding: 2px 5px;
            }
            .modal-header {
                padding: 10px;
            }
            .modal-body {
                /*padding: 5px;*/
            }
            .tb-success {
                background-color: #009688ab;
            }

            .tb-bonus {
                background-color: #87B87F;
            }
            .tb-danger {
                background-color: #f44336a3;
            }
            .tb-warning {
                background-color: #ffeb3b;
            }
            .table-hover>tbody>tr:hover {
                background-color: #ffeb3b73;
            }
            .badge-bo {
                padding: 1px 5px;
                border-radius: 4px;
                float: right;
            }
            .img_banklogo {
                border-radius: 50%;
                width: 18px;
                height: 18px;
            }
            select.form-control {
                height: 30px;
            }

            .text-red {
                color: red;
            }

            .text-green {
                color: green;
            }
        </style>
    </head>
    <body class="skin-1">
        <div id="navbar" class="navbar navbar-default ace-save-state">
            <div class="navbar-container ace-save-state" id="navbar-container">
                <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                    <span class="sr-only">Toggle sidebar</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-header pull-left">
                    <a href="<?php echo base_url();?>" class="navbar-brand">
                        <small>
                            <i class="fa fa-cogs"></i>
                            <?php echo $this->config->config['website'];?></a>
                        </small>
                    </a>
                </div>
                <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="white dropdown-modal">
                            <i class="ace-icon fa fa-bell icon-animated-bell"></i>
                            Last Login : <i class="fa fa-clock-o"></i> <?= empty($last_login) ? "First Time Login" : $last_login; ?>&nbsp;&nbsp;
                        </li>
                        <li class="light-blue dropdown-modal">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <img class="nav-user-photo" src="<?php echo base_url();?>assets/images/avatars/profile-pic.jpg" alt="<?php echo $name; ?> Profile" />
                                <span class="user-info">
                                    <small><?php echo $role_text; ?>,</small>
                                    <?php echo $name; ?>
                                </span>
                                <i class="ace-icon fa fa-caret-down"></i>
                            </a>
                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="<?php echo base_url(); ?>loadChangePass">
                                        <i class="ace-icon fa fa-cog"></i>
                                        ChangePassword
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="<?php echo base_url(); ?>logout">
                                        <i class="ace-icon fa fa-power-off"></i>
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="main-container ace-save-state" id="main-container">
            <script type="text/javascript">
                try{ace.settings.loadState('main-container')}catch(e){}
            </script>
            <div id="sidebar" class="sidebar responsive ace-save-state sidebar-fixed sidebar-scroll">
                <script type="text/javascript">
                    try{ace.settings.loadState('sidebar')}catch(e){}
                </script>
                <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                    <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                        <button class="btn btn-success">
                            <i class="ace-icon fa fa-signal"></i>
                        </button>
                        <button class="btn btn-info">
                            <i class="ace-icon fa fa-pencil"></i>
                        </button>
                        <button class="btn btn-warning">
                            <i class="ace-icon fa fa-users"></i>
                        </button>
                        <button class="btn btn-danger">
                            <i class="ace-icon fa fa-cogs"></i>
                        </button>
                    </div>
                    <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                        <span class="btn btn-success"></span>
                        <span class="btn btn-info"></span>
                        <span class="btn btn-warning"></span>
                        <span class="btn btn-danger"></span>
                    </div>
                </div>
                <ul class="nav nav-list">
                    <!-- <li class="<?php echo ($this->uri->segment(1) == 'promotions') ? 'active' : '';?>">
                        <a href="<?php echo base_url();?>promotions">
                            <i class="menu-icon fa fa-gift"></i>
                            <span class="menu-text"> ตัวอย่าง </span>
                        </a>
                        <b class="arrow"></b>
                    </li> -->
                    <?php if($role == '1'):?>
                     <li class="<?php echo ($this->uri->segment(1) == 'company') ? 'active' : '';?>">
                        <a href="<?php echo base_url();?>company">
                            <i class="menu-icon fa fa-book"></i>
                            <span class="menu-text"> บริษัท </span> 
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="<?php echo ($this->uri->segment(1) == 'department') ? 'active' : '';?>">
                        <a href="<?php echo base_url();?>department">
                            <i class="menu-icon fa fa-book"></i>
                            <span class="menu-text"> แผนก </span> 
                        </a>
                        <b class="arrow"></b>
                    </li>
                     <li class="<?php echo ($this->uri->segment(1) == 'positions') ? 'active' : '';?>">
                        <a href="<?php echo base_url();?>positions">
                            <i class="menu-icon fa fa-book"></i>
                            <span class="menu-text"> ตำแหน่ง </span> 
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="<?php echo ($this->uri->segment(1) == 'employees') ? 'active' : '';?>">
                        <a href="<?php echo base_url();?>employees">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text"> ข้อมูลพนักงาน </span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="<?php echo ($this->uri->segment(1) == 'incometype') ? 'active' : '';?>">
                        <a href="<?php echo base_url();?>incometype">
                            <i class="menu-icon fa fa-calculator"></i>
                            <span class="menu-text"> ตั้งค่าเพิ่มลด-รายได้ </span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="<?php echo ($this->uri->segment(1) == 'calsalary') ? 'active' : '';?>">
                        <a href="<?php echo base_url();?>calsalary">
                            <i class="menu-icon fa fa-calculator"></i>
                            <span class="menu-text"> คำนวนเงินเดือน </span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="<?php echo ($this->uri->segment(1) == 'clearhead') ? 'active' : '';?>">
                        <a href="<?php echo base_url();?>clearhead">
                            <i class="menu-icon fa fa-calendar"></i>
                            <span class="menu-text"> เคลียร์หัว </span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="<?php echo ($this->uri->segment(1) == 'clearagent') ? 'active' : '';?>">
                        <a href="<?php echo base_url();?>clearagent">
                            <i class="menu-icon fa fa-calendar"></i>
                            <span class="menu-text"> เคลียร์ Agent</span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <?php endif;?>
                    <?php if($role == '3' || $role == '1'):?>
                        <?php foreach($this->config->config['companylist'] as $k => $v):?>
                        <?php $get = $this->input->get();?>
                        <?php if(isset($get['company'])):?>
                            <li class="<?php echo ($this->input->get('company') == $k) ? 'active' : '';?>">
                        <?php else:?>
                            <li class="">
                        <?php endif;?>
                            <a href="<?php echo base_url();?>reportwinloss?company=<?php echo $k;?>">
                                <i class="menu-icon fa fa-list"></i>
                                <span class="menu-text"> รายงาน <?php echo $v;?></span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <?php endforeach;?>
                    <?php endif;?>
                    <?php if($role == '1' || $role == '2'):?>
                    <!-- <li class="<?php echo ($this->uri->segment(1) == 'acclist') ? 'active' : '';?>">
                        <a href="<?php echo base_url();?>acclist">
                            <i class="menu-icon fa fa-cogs"></i>
                            <span class="menu-text"> ตั้งค่ารหัสผู้ใช้งาน </span>
                        </a>
                        <b class="arrow"></b>
                    </li> -->
                    <?php endif;?>
                </ul>
                <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                    <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
                </div>
            </div>
            <div class="main-content">
                <div class="main-content-inner">
                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="#"><?php echo $pagetitle;?></a>
                            </li>
                        </ul>
                    </div>
                    <div class="page-content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="widget-box-overlay" id="loading" style="display: none;"><i class=" ace-icon loading-icon fa fa-spinner fa-spin fa-2x white"></i></div>
                                <?php echo $content;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="footer-inner">
                    <div class="footer-content">
                        <span class="bigger-120">
                            <span class="blue bolder"></span>
                            Application &copy; <?php echo $this->config->config['website'];?>.com 2019
                        </span>
                        &nbsp; &nbsp;
                        <span class="action-buttons">
                            <a href="#">
                                <i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
                            </a>

                            <a href="#">
                                <i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
                            </a>

                            <a href="#">
                                <i class="ace-icon fa fa-rss-square orange bigger-150"></i>
                            </a>
                        </span>
                    </div>
                </div>
            </div>
            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div>
        <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
            $(document).ready(function(){
                $('.number').autoNumeric();
                // cntTimecn_cal_period(); 
            });
            $(document).one('ajaxloadstart.page', function(e) {
                $.gritter.removeAll();
                $('.modal').modal('hide');
            });

            $('.date-picker').datepicker({
                autoclose : true,
                todayHighlight : true,
            }).next().on(ace.click_event, function(){
                $(this).prev().focus();
            });
            function numberWithCommas(x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
            function formatdate(x) {
                var date = new Date(x);
                return ("0" + date.getDate()).slice(-2) + '-' + ("0" + (date.getMonth()  + 1)).slice(-2) + '-' + date.getFullYear();
            }
        </script>
    </body>
</html>
