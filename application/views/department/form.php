<div class="row">
    <div class="col-xs-12">
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="#">
            <div class="form-group">
                <?php if(isset($data)):?>
                    <input type="hidden" name="old_val" value="<?php echo htmlspecialchars(json_encode($data)); ?>" />
                <?php endif;?>
            </div>

             <div class="form-group">
                <label for="cstatus" class="col-md-2 control-label"> ชื่อบริษัท <span class="text-danger">*</span></label>
                <div class="col-md-10">
                     
                    <?php if (isset($data->com_name)) :?>
                        <select class="form-control" id="status" name="status" onchange="sd();">
                          <option value="0">กรุณาเลือกชื่อบริษัท</option>
                            <?php foreach ($company as $k => $v):?>
                                    <option value="<?php echo $v['id'];?>" <?php echo ($data->com_name == $v['name']) ? 'selected' : ''; ?>><?php echo $v['name'];?></option>       
                            <?php endforeach;?>
                        </select>
                    <?php else :?>
                        <select class="form-control" id="status" name="status" onchange="sd();">
                            <option value="0">กรุณาเลือกชื่อบริษัท</option>
                            <?php foreach ($company as $k => $v):?>
                                    <option value="<?php echo $v['id'];?>"><?php echo $v['name'];?></option>     
                            <?php endforeach;?>
                        </select>
                    <?php endif;?>
                    
                </div>
            </div>

            <div class="form-group">
                <label for="userid" class="col-md-2 control-label">ชื่อแผนก <span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="title" id="title" value="<?php echo (isset($data->name)) ? $data->name : ''; ?>">
                    
                </div>
            </div>
            <div class="form-group">
                <label for="desc" class="col-md-2 control-label"> ชื่อย่อแผนก <span class="text-danger">*</span></label>
                <div class="col-md-10">
                    
                    <input type="text" class="form-control" name="desc" id="desc" value="<?php echo (isset($data->desc)) ? $data->desc : ''; ?>">
                </div>
            </div>
           <!--  <div class="form-group">
                <label for="file" class="col-md-2 control-label">รูปภาพ</label>
                <div class="col-md-10">
                    <div class="input-group">
                        <input type="file" name="img" class="file" accept="image/*" id="fi">
                        <input type="text" class="form-control" disabled placeholder="อัพโหลดรูปภาพ..." id="file">
                        <span class="input-group-addon " onclick="browse_file();">เลือก...</span>
                    </div>
                    <img src="<?php echo (isset($data->img_url)) ? base_url().'assets/uploads/'.$data->img_url : ''; ?>" id="preview" class="img-thumbnail">
                </div>
            </div> -->
           
        </form>
    </div>
</div>
<script type="text/javascript">
    function browse_file(){
        $('.file').click();
    }

    function clear_form(){
        $('#preview').attr('src','');
    }

    $('input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $("#file").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
            // get loaded data and render thumbnail.
            // document.getElementById("preview").src = e.target.result;
            $('#preview').attr('src',e.target.result);
        };
        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });

    // CKEDITOR.replace('desc', {
    //   // Define the toolbar groups as it is a more accessible solution.
    //   toolbarGroups: [{
    //       "name": "basicstyles",
    //       "groups": ["basicstyles"]
    //     },
    //     {
    //       "name": "paragraph",
    //       "groups": ["list", "blocks"]
    //     },
    //     {
    //       "name": "document",
    //       "groups": ["mode"]
    //     },
    //     {
    //       "name": "styles",
    //       "groups": ["styles"]
    //     },
    //   ],
    //   allowedContent : true,
    //   // Remove the redundant buttons from toolbar groups defined above.
    //   removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar,PasteFromWord'
    // });
</script>