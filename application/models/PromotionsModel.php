<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class PromotionsModel extends CI_Model {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();

	}

	public function create($data = array()) {
		$this->db->insert('tb_promotions', $data);
		return $this->db->insert_id();

	}

	public function update($id, $data = array()) {

		$this->db->where('id', $id);
		return $this->db->update('tb_promotions', $data);

	}

	public function search_data($p,$page,$per_page) {
		$con = '';
		if($p != ''){
			if(isset($p['qtitle'])){
				if($p['qtitle'] != ''){
					$con .= " AND title like '%".$p['qtitle']."%'";
				}
			}

			if(isset($p['qstatus'])){
				if($p['qstatus'] != ''){
					$con .= " AND status = '".$p['qstatus']."'";
				}
			}
		}
		$sql = "SELECT * FROM tb_promotions WHERE 1=1 $con ORDER BY id DESC LIMIT $page,$per_page";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_promotions() {
        $sql   = "select * from tb_promotions where status = '1' ";
        $query = $this->db->query($sql);
        $row   = array();
        if($query->num_rows() > 0){
            $row = $query->result_array();
        }

        return $row;
    }

	public function get_data_byid($id) {
		$this->db->from('tb_promotions');
		$this->db->where('id', $id);
		return $this->db->get()->row();
	}

	public function inslogs($d) {
		$data = array(
			'menu'     	 => 'deposit',
			'did'  	 	 => $d['did'],
			'col'   	 => $d['col'],
			'oldval'   	 => $d['oldval'],
			'newval'   	 => $d['newval'],
			'created_by' => $d['created_by'],
		);
		return $this->db->insert('tb_logs', $data);
	}
}
