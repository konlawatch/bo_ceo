<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class IncometypeModel extends CI_Model {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();

	} 

	public function create($data = array()) {
		//debug($data,true);

		$this->db->insert('tb_incometype', $data);
		return $this->db->insert_id();

	}

	public function update($id, $data = array()) {

		$this->db->where('id', $id);
		return $this->db->update('tb_incometype', $data);

	}

	public function search_data($p,$page,$per_page) {
		$con = '';
		$p['qstatus'] = 1;
		if($p != ''){ 
			if(isset($p['qtitle'])){
				if($p['qtitle'] != ''){
					$con .= " AND name like '%".$p['qtitle']."%'";
				}
			}

			if(isset($p['qstatus'])){
				if($p['qstatus'] != ''){
					$con .= " AND status = '".$p['qstatus']."'";
				}
			}
		}
		$sql = "SELECT * FROM tb_incometype WHERE 1=1 $con ORDER BY id DESC LIMIT $page,$per_page";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_company() {
        $sql   = "select * from tb_incometype where status = '1' ";
        $query = $this->db->query($sql);
        $row   = array();
        if($query->num_rows() > 0){
            $row = $query->result_array();
        }

        return $row;
    }

	public function get_data_byid($id) {
		$this->db->from('tb_incometype');
		$this->db->where('id', $id);
		return $this->db->get()->row();
	}

	public function inslogs($d) {
		$data = array(
			'menu'     	 => 'deposit',
			'did'  	 	 => $d['did'],
			'col'   	 => $d['col'],
			'oldval'   	 => $d['oldval'],
			'newval'   	 => $d['newval'],
			'created_by' => $d['created_by'],
		);
		return $this->db->insert('tb_logs', $data);
	}

	public function data_company() {

		$sql = "SELECT * FROM tb_company where status = '1' ";

		
		$query = $this->db->query($sql);
		return $query->result_array();

	}

	public function data_com($d) {

		$sql = "SELECT * FROM tb_company where id = '$d' ";
		$query = $this->db->query($sql);
		return $query->result_array();

	}
}
