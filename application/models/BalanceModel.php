<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class BalanceModel extends CI_Model {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();

	}

	public function createbl($d = null) {
		$bdate 		= $d['bdate'];
		$did_cnt 	= $d['did_cnt'];
		$did_amount = $d['did_amount'];
		$wid_cnt 	= $d['wid_cnt'];
		$wid_amount = $d['wid_amount'];
		$bonus_amount = $d['bonus_amount'];
		$diff   	= $d['diff'];
        $bank_bl    = $d['bank_bl'];
        $didwid_bl  = $d['didwid_bl'];
		$flag 		= $d['flag'];
		$created_by = $d['created_by'];
		$created_at = $d['created_at'];
		$date 		= date('Y-m-d H:i:s');
		$sql = "INSERT INTO tb_balance (bdate, did_cnt, did_amount, wid_cnt, wid_amount , bonus_amount , diff ,bank_bl,didwid_bl, flag, created_by, created_at) 
				VALUES('$bdate', '$did_cnt', '$did_amount', '$wid_cnt', '$wid_amount' , '$bonus_amount' , '$diff' ,'$bank_bl','$didwid_bl', '$flag', '$created_by',
				 '$created_at')
                ON DUPLICATE KEY UPDATE did_cnt = '$did_cnt',did_amount = '$did_amount',wid_cnt = '$wid_cnt',wid_amount = '$wid_amount',bonus_amount = '$bonus_amount',diff = '$agent_bl',diff = '$bank_bl',didwid_bl = '$didwid_bl',updated_at = '$date' ";

        if ($this->db->query($sql)) {
            return true;
        } else {
            return false;
        }
	}

	public function create($d = null) {
		$cdate 		= $d['cdate'];
		$web 		= $d['web'];
		$type 		= $d['type'];
		$name 		= $d['name'];
		$bankid 	= $d['bankid'];
		$bankno 	= $d['bankno'];
		$bf_amount 	= $d['bf_amount'];
		$af_amount  = $d['af_amount'];
		$fee 		= $d['fee'];
		$status 	= $d['status'];
		$created_by = $d['created_by'];
		$created_at = $d['created_at'];
		$date 		= date('Y-m-d H:i:s');
		$did_cnt    = 0;
        $did_amt    = 0;
        $did_bonus  = 0;
        $wid_cnt    = 0;
        $wid_amt    = 0;
        $credit_change = 0;
		$sql = "INSERT INTO tb_balance_list (cdate, web, type, name, bankid , bankno , bf_amount ,af_amount, fee , status, created_by, created_at) 
				VALUES('$cdate', '$web', '$type', '$name', '$bankid' , '$bankno' , '$bf_amount' ,'$af_amount', '$fee', '$status', '$created_by', '$created_at')
                ON DUPLICATE KEY UPDATE af_amount = '$af_amount',fee = '$fee',updated_at = '$date' ,did_cnt = '$did_cnt',did_amt = '$did_amt',did_bonus = '$did_bonus'
                ,wid_cnt = '$wid_cnt',wid_amt = '$wid_amt',credit_change = '$credit_change' ";

        if ($this->db->query($sql)) {
            // return ($this->db->affected_rows() != 1) ? false : true;
            return true;
        } else {
            return false;
        }
	}

	public function update($id, $d = null) {
		$this->db->where('id', $id);
		return $this->db->update('tb_balance_list', $d);
	}

	public function updatelist($cdate,$bankno, $d = null) {
		$this->db->where('cdate', $cdate);
		$this->db->where('bankno', $bankno);
		return $this->db->update('tb_balance_list', $d);
	}

	public function update_balance($bdate, $d = null) {
		$this->db->where('bdate', $bdate);
		return $this->db->update('tb_balance', $d);
	}

	public function get_otherlist($date,$c) {
		$this->db = $this->load->database($c, TRUE);
		$sql   = "select * from tb_balance_list where status = '1' and cdate = '$date' and type in ('O','G') order by created_at";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_banklist($date,$c) {
		$this->db = $this->load->database($c, TRUE);
		$sql   = "select * from tb_balance_list where status = '1' and cdate = '$date' and type = 'B' ";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_agentlist($date,$c) {
		$this->db = $this->load->database($c, TRUE);
		$sql   = "select * from tb_balance_list where status = '1' and cdate = '$date' and type = 'A' ";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_otherlist_ye($date,$c) {
		$this->db = $this->load->database($c, TRUE);
		$sql   = "select * from tb_balance_list where status = '2' and cdate = '$date' and type = 'O' ";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_banklist_ye($date,$c) {
		$this->db = $this->load->database($c, TRUE);
		$sql   = "select * from tb_balance_list where status = '2' and cdate = '$date' and type = 'B' ";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_agentlist_ye($date,$c) {
		$this->db = $this->load->database($c, TRUE);
		$sql   = "select * from tb_balance_list where status = '2' and cdate = '$date' and type = 'A' ";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_balance() {
		$sql   = "select * from tb_balance where flag = 'N' order by id desc limit 1 ";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_balance_bydate($date,$c) {
		$this->db = $this->load->database($c, TRUE);
		$sql   = "select * from tb_balance where bdate = '".$date."' order by id desc limit 1 ";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_balancelist($date) {
		$sql   = "select * from tb_balance_list where status = '1' and cdate = '$date' and type IN('O','B') ";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_balancelist_old($date) {
		$sql   = "select * from tb_balance_list where status = '2' and cdate = '$date' and type IN('O','B') ";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function del_data($id) {
		$sql = "update tb_balance_list set status = '0' where id = '".$id."' ";
		return $this->db->query($sql);
	}

 	public function ins_bllist($d = array()){
        $date = date('Y-m-d H:i:s');
        $bdate 	   = '';
        $type 	   = '';
        $name 	   = '';
        $bankid    = '';
        $bankno    = '';
        $bf_amount = '';
        $af_amount = '';
        $fee 	   = '';
        $status    = '';
        $sql = "INSERT INTO tb_balance_list (web, uagent, userid, balance, winlose , credit , company ,status, created_by,created_at) VALUES('" . $d['web'] . "', '" . $d['uagent'] . "','" . $d['userid'] . "', '" . $d['balance'] . "', '" . $d['winlose'] . "','".$d['credit']."','".$d['company']."','1','BOT','$date')
                ON DUPLICATE KEY UPDATE balance = '" . $d['balance'] . "',winlose = '" . $d['winlose'] . "',credit = '" . $d['credit'] . "' ";

        if ($this->db->query($sql)) {
            return ($this->db->affected_rows() != 1) ? false : true;
        } else {
            return false;
        }
    }

    public function getdidcnt_bybank($cdate = ''){
    	if($cdate != ''){
    		$sql 	= "select tobank as uid , count(*) as cnt,sum(amount) as amt,sum(bvalue) as bonus from tb_transec where type = '1' and status = '4' and bdate = '$cdate' group by tobank ";
    	}else{
    		$sql 	= "select tobank as uid , count(*) as cnt,sum(amount) as amt,sum(bvalue) as bonus from tb_transec where type = '1' and status = '4' and flag ='N' group by tobank ";
    	}
    	// if($cdate != ''){
    	// 	$sql 	= "select bankno as uid , 
					// 	(select count(*) from tb_transec where type = '1' and status = '4' and flag ='N' and tobank = bankno and cdate = '$cdate') as did_cnt,
					// 	(select COALESCE(sum(amount),0) from tb_transec where type = '1' and status = '4' and flag ='N' and tobank = bankno and cdate = '$cdate') as did_amt ,
					// 	(select COALESCE(sum(bvalue),0) from tb_transec where type = '1' and status = '4' and flag ='N' and tobank = bankno and cdate = '$cdate') as bonus,
					// 	(select count(*) from tb_transec where type = '2' and status = '4' and flag ='N' and frombank = bankno and cdate = '$cdate') as wid_cnt,
					// 	(select COALESCE(sum(amount),0) from tb_transec where type = '2' and status = '4' and flag ='N' and frombank = bankno and cdate = '$cdate') as wid_amt 
					// from tb_bank  
					// where status = '1'";
    	// }else{
    	// 	$sql 	= "select bankno as uid , 
					// 	(select count(*) from tb_transec where type = '1' and status = '4' and flag ='N' and tobank = bankno) as did_cnt,
					// 	(select COALESCE(sum(amount),0) from tb_transec where type = '1' and status = '4' and flag ='N' and tobank = bankno) as did_amt ,
					// 	(select COALESCE(sum(bvalue),0) from tb_transec where type = '1' and status = '4' and flag ='N' and tobank = bankno) as bonus,
					// 	(select count(*) from tb_transec where type = '2' and status = '4' and flag ='N' and frombank = bankno) as wid_cnt,
					// 	(select COALESCE(sum(amount),0) from tb_transec where type = '2' and status = '4' and flag ='N' and frombank = bankno) as wid_amt  
					// from tb_bank  
					// where status = '1'";
    	// }
		$query  = $this->db->query($sql);
		return $query->result();
    }

    public function getwidcnt_bybank($cdate = ''){
    	if($cdate != ''){
    		$sql 	= "select frombank as uid , count(*) as cnt,sum(amount) as amt from tb_transec where type = '2' and status = '4' and bdate = '$cdate' group by frombank ";
		}else{
    		$sql 	= "select frombank as uid , count(*) as cnt,sum(amount) as amt from tb_transec where type = '2' and status = '4' and flag ='N' group by frombank ";
    	}
    	// if($cdate != ''){
    	// 	$sql 	= "select bankno as uid , 
					// 	(select count(*) from tb_transec where type = '2' and status = '4' and flag ='N' and frombank = bankno and cdate = '$cdate') as cnt,
					// 	(select COALESCE(sum(amount),0) from tb_transec where type = '2' and status = '4' and flag ='N' and frombank = bankno and cdate = '$cdate') as amt
					// from tb_bank  
					// where status = '1'";
    	// }else{
    	// 	$sql 	= "select bankno as uid , 
					// 	(select count(*) from tb_transec where type = '2' and status = '4' and flag ='N' and frombank = bankno) as cnt,
					// 	(select COALESCE(sum(amount),0) from tb_transec where type = '2' and status = '4' and flag ='N' and frombank = bankno) as amt 
					// from tb_bank  
					// where status = '1'";
    	// }

		$query  = $this->db->query($sql);
		return $query->result();
    }

    public function getdidcnt_byagent($cdate = ''){
     	if($cdate != ''){
     		$sql 	= "select b.name as uid , count(*) as cnt,sum(amount) as amt,sum(bvalue) as bonus from tb_transec a
    				inner join tb_agent b on b.name = a.agent and a.web = b.web 
    				where a.type = '1' and a.status = '4' and b.status = '1' and a.bdate = '$cdate' group by b.name ";
 		}else{
    		$sql 	= "select b.name as uid , count(*) as cnt,sum(amount) as amt,sum(bvalue) as bonus from tb_transec a
    				inner join tb_agent b on b.name = a.agent and a.web = b.web 
    				where a.type = '1' and a.status = '4' and b.status = '1' and a.flag ='N' group by b.name ";
		}
		// if($cdate != ''){
  //   		$sql 	= "select b.name as uid , 
		// 				(select count(*) from tb_transec where type = '1' and status = '4' and flag ='N' and agent = b.name and cdate = '$cdate') as did_cnt,
		// 				(select COALESCE(sum(amount),0) from tb_transec where type = '1' and status = '4' and flag ='N' and agent = b.name and cdate = '$cdate') as did_amt,
		// 				(select COALESCE(sum(bvalue),0) from tb_transec where type = '1' and status = '4' and flag ='N' and agent = b.name and cdate = '$cdate') as bonus,
		// 				(select count(*) from tb_transec where type = '2' and status = '4' and flag ='N' and agent = b.name and cdate = '$cdate' ) as wid_cnt,
		// 				(select COALESCE(sum(amount),0) from tb_transec where type = '2' and status = '4' and flag ='N' and agent = b.name and cdate = '$cdate' ) as wid_amt 
		// 			from tb_agent  b
		// 			where b.status = '1'";
		// }else{
  //   		$sql 	= "select b.name as uid , 
		// 				(select count(*) from tb_transec where type = '1' and status = '4' and flag ='N' and agent = b.name ) as did_cnt,
		// 				(select COALESCE(sum(amount),0) from tb_transec where type = '1' and status = '4' and flag ='N' and agent = b.name ) as did_amt,
		// 				(select COALESCE(sum(bvalue),0) from tb_transec where type = '1' and status = '4' and flag ='N' and agent = b.name) as bonus,
		// 				(select count(*) from tb_transec where type = '2' and status = '4' and flag ='N' and agent = b.name ) as wid_cnt,
		// 				(select COALESCE(sum(amount),0) from tb_transec where type = '2' and status = '4' and flag ='N' and agent = b.name ) as wid_amt 
		// 			from tb_agent  b
		// 			where b.status = '1'";
		// }
		$query  = $this->db->query($sql);
		return $query->result();
    }

    public function getwidcnt_byagent($cdate = ''){
    	if($cdate != ''){
    		$sql 	= "select b.name as uid , count(*) as cnt,sum(amount) as amt from tb_transec a
    				inner join tb_agent b on b.name = a.agent and a.web = b.web 
    				where a.type = '2' and a.status = '4' and b.status = '1' and a.bdate = '$cdate' group by b.name ";
		}else{
    		$sql 	= "select b.name as uid , count(*) as cnt,sum(amount) as amt from tb_transec a
    				inner join tb_agent b on b.name = a.agent and a.web = b.web 
    				where a.type = '2' and a.status = '4' and b.status = '1' and a.flag ='N' group by b.name ";
		}
		// if($cdate != ''){
  //   		$sql 	= "select b.name as uid , 
		// 				(select count(*) from tb_transec where type = '2' and status = '4' and flag ='N' and agent = b.name and cdate = '$cdate' ) as cnt,
		// 				(select COALESCE(sum(amount),0) from tb_transec where type = '2' and status = '4' and flag ='N' and agent = b.name and cdate = '$cdate' ) as amt 
		// 			from tb_agent  b
		// 			where b.status = '1'";
		// }else{
  //   		$sql 	= "select b.name as uid , 
		// 				(select count(*) from tb_transec where type = '2' and status = '4' and flag ='N' and agent = b.name ) as cnt,
		// 				(select COALESCE(sum(amount),0) from tb_transec where type = '2' and status = '4' and flag ='N' and agent = b.name ) as amt 
		// 			from tb_agent  b
		// 			where b.status = '1'";
		// }

		$query  = $this->db->query($sql);
		return $query->result();
    }

    public function getag_changecredit($agid){
    	// $sql 	= "select sum(af_credit-bf_credit) as credit_change  from tb_transec a
					// inner join tb_agent b on b.name = left(a.userid,length(b.name)) and a.web = b.web 
					// where a.status = '4' and b.status = '1' and a.flag ='N' and b.name = '$agid' group by b.name order by userid";
		$sql 	= "select sum(af_credit-bf_credit) as credit_change  from tb_transec a
					where a.status = '4' and a.flag ='N' and a.agent = '$agid' group by a.agent order by userid";
		$query  = $this->db->query($sql);
		return $query->result();
    }

    public function getag_changecredit_bydate($agid,$cdate){
    	// $sql 	= "select sum(af_credit-bf_credit) as credit_change  from tb_transec a
					// inner join tb_agent b on b.name = left(a.userid,length(b.name)) and a.web = b.web 
					// where a.status = '4' and b.status = '1' and a.cdate ='$cdate' and b.name = '$agid' group by b.name order by userid";

		$sql 	= "select sum(af_credit-bf_credit) as credit_change  from tb_transec a
					where a.status = '4' and a.agent = '$agid' and a.bdate ='$cdate'
					group by a.agent order by userid";

		$query  = $this->db->query($sql);
		return $query->result();
    }

    public function get_cntbanklog(){
    	$sql 	= "select count(*) as cnt,sum(amount) as amt from tb_banklog where status = '1' ";
		$query  = $this->db->query($sql);
		return ($query->num_rows() > 0) ? $query->result()[0] : 0;
    }

    public function get_cntrundid(){
    	$sql 	= "select count(*) as cnt from tb_transec a where a.type in('1','3') and a.status in('1','2','3','5','7') and a.flag ='N'";
		$query  = $this->db->query($sql);
		return ($query->num_rows() > 0) ? $query->result()[0] : 0;
    }

    public function get_cntrunwid(){
    	$sql 	= "select count(*) as cnt from tb_transec a where a.type in('2','4') and a.status in('1','2','3','5','7') and a.flag ='N'";
		$query  = $this->db->query($sql);
		return ($query->num_rows() > 0) ? $query->result()[0] : 0;
    }

    public function closebl($bdate) {
    	$d = array(
			'flag' => 'Y'
		);
		$this->db->where('bdate', $bdate);
		$this->db->where('flag', 'N');
		return $this->db->update('tb_balance', $d);
	}

	public function closebl_list($cdate) {

		$sql 	= "update tb_balance_list set status = '2' where cdate = '$cdate' and status = '1' ";
		return $this->db->query($sql);
	}

	public function close_transsec($bdate) {
		$d = array(
			'bdate' => $bdate,
			'flag' => 'Y',
		);
		$this->db->where('flag', 'N');
		$this->db->where('bdate', '0000-00-00');
		return $this->db->update('tb_transec', $d);
	}

	public function clear_api_didwid($cdate) {
		$d = array(
			'status' => '0'
		);
		$this->db->where('cdate', $cdate);
		$this->db->where('bdate', '0000-00-00');
		$this->db->where('status', '9');
		return $this->db->update('tb_transec', $d);
	}

	public function inslogs($d) {
		$data = array(
			'menu'     	 => 'balance',
			'did'  	 	 => $d['did'],
			'col'   	 => $d['col'],
			'oldval'   	 => $d['oldval'],
			'newval'   	 => $d['newval'],
			'created_by' => $d['created_by'],
		);
		return $this->db->insert('tb_logs', $data);
	}
}
