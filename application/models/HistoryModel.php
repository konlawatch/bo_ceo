<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class HistoryModel extends CI_Model {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();

	}

	public function search_data($p,$page,$per_page) {
		$con = '';
		if($p != ''){
			if($p['userid'] != ''){
				$con .= " AND userid like '%".$p['userid']."%'";
			}
			if($p['name'] != ''){
				$con .= " AND name like '%".$p['name']."%'";
			}
			if($p['web'] != '' && $p['web'] != '0'){
				$con .= " AND web = '".$p['web']."'";
			}
			if($p['ag'] != '' && $p['ag'] != '0'){
				$con .= " AND agent = '".$p['ag']."'";
			}
			if($p['st'] != '' && $p['et'] != ''){
				$con .= " AND cdate >= '".date('Y-m-d',strtotime($p['st']))."' AND cdate <= '".date('Y-m-d',strtotime($p['et']))."' ";
			}
			if($p['type'] != '' && $p['type'] != '0'){
				if($p['type'] == '5'){
					$con .= " AND type in ('1','3') ";
				}else if($p['type'] == '6'){
					$con .= " AND type in ('2','4') ";
				}else{
					$con .= " AND type = '".$p['type']."'";
				}
			}
		}

		$w = '';
		// $this->db = $this->load->database('enjoy', TRUE);
		$sql = "SELECT * FROM tb_transec WHERE status = '4' $con $w ORDER BY id DESC LIMIT $page,$per_page";
		// echo $sql;exit();
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function search_databybdate($p,$page,$per_page) {
		$con = '';
		if($p['userid'] != ''){
			$con .= " AND userid like '%".$p['userid']."%'";
		}
		if($p['web'] != '' && $p['web'] != '0'){
			$con .= " AND web = '".$p['web']."'";
		}
		if($p['ag'] != '' && $p['ag'] != '0'){
			$con .= " AND agent = '".$p['ag']."'";
		}
		if($p['st'] != '' && $p['et'] != ''){
			$con .= " AND bdate >= '".date('Y-m-d',strtotime($p['st']))."' AND bdate <= '".date('Y-m-d',strtotime($p['et']))."' ";
		}
		if($p['type'] != '' && $p['type'] != '0'){
			if($p['type'] == '5'){
				$con .= " AND type in ('1','3') ";
			}else if($p['type'] == '6'){
				$con .= " AND type in ('2','4') ";
			}else{
				$con .= " AND type = '".$p['type']."'";
			}
		}
		if($p['company'] != '' && $p['company'] != '0'){
			$this->db = $this->load->database($p['company'], TRUE);
		}
				
		$w = '';
		$sql = "SELECT * FROM tb_transec WHERE status = '4' $con $w ORDER BY id DESC LIMIT $page,$per_page";
		$query = $this->db->query($sql);
		return $query->result();
	}
}
