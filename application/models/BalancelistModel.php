<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class BalancelistModel extends CI_Model {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();

	}

	public function create($d = null) {

		$data = array(
			'bdate'   	 => date('Y-m-d',strtotime($d['bdate'])),
			'did_cnt'    	 => $d['did_cnt'],
			'did_amount'      => $d['did_amount'],
			'wid_cnt'   	 => $d['wid_cnt'],
			'wid_amount'   	 => $d['wid_amount'],
			'webid'   	 => $d['webid'],
			'webname'  	 => $d['webname'],
			'bankid'   	 => $d['bankid'],
			'bankname'   => $d['bankname'],
			'bankno'   	 => $d['bankno'],
			'fromdata'   => $d['fromdata'],
			'other'   	 => $d['other'],
			'status'     => '1',
			'created_by' => $d['created_by'],
			'created_at' => date('Y-m-d H:i:s'),
		);

		$this->db->insert('tb_balance', $data);
		return $this->db->insert_id();

	}

	public function update($id, $d = null) {

		$data = array(
			'cdate'   	 => date('Y-m-d',strtotime($d['cdate'])),
			'name'    	 => $d['name'],
			'lname'      => $d['lname'],
			'tel'   	 => $d['tel'],
			'lineid'   	 => $d['lineid'],
			'webid'   	 => $d['webid'],
			'webname'  	 => $d['webname'],
			'bankid'   	 => $d['bankid'],
			'bankname'   => $d['bankname'],
			'bankno'   	 => $d['bankno'],
			'fromdata'   => $d['fromdata'],
			'other'   	 => $d['other'],
			'updated_by' => $d['updated_by'],
			'updated_at' => date('Y-m-d H:i:s'),
		);

		$this->db->where('id', $id);
		return $this->db->update('tb_balance', $data);

	}
	
	public function get_data_byid($id) {
		$this->db->from('tb_balance');
		$this->db->where('id', $id);
		return $this->db->get()->row();
	}

	public function all_data_cnt($role) {
		$this->db->from('tb_balance');
		if($role != '1'){
			$this->db->where('company', $role);
		}
		return $this->db->get()->num_rows();
	}

	public function all_data($role) {
		$this->db->from('tb_balance');
		if($role != '1'){
			$this->db->where('company', $role);
		}
		$this->db->where('status', '1');
		$this->db->order_by("id", "desc");
		return $this->db->get()->result();
	}

	public function data_page($page, $per_page,$role) {
		if($role == '1'){
			$sql = "SELECT * FROM tb_balance WHERE status = '1'  ORDER BY ID ASC LIMIT $page,$per_page";
		}else{
			$sql = "SELECT * FROM tb_balance WHERE status = '1' ORDER BY ID DESC LIMIT $page,$per_page";
		}
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function search_data($d,$role,$page,$per_page) {
		$con = '';

		if($d != ''){

			if($d['bdate'] != '' && $d['edate']){
				// $con .= "AND bdate like '".date('Y-m-d',strtotime($d['bdate']))."' ";
				$con .= "AND bdate between '".date('Y-m-d',strtotime($d['bdate']))."' and  '".date('Y-m-d',strtotime($d['edate']))."'";
			}

			if($d['company'] != ''){
				$this->db = $this->load->database($d['company'], TRUE);
			}
		}

		$sql = "SELECT * FROM tb_balance WHERE status = '1' $con ORDER BY id ASC LIMIT $page,$per_page";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function del_data($id) {
		// $this->db->where('id', $id);
		// return $this->db->delete('tb_balance');
		$sql = "update tb_balance set status = '0' where id = '".$id."' ";
		return $this->db->query($sql);
	}

	public function inslogs($d) {
		$data = array(
			'menu'     	 => 'balancelist',
			'did'  	 	 => $d['did'],
			'col'   	 => $d['col'],
			'oldval'   	 => $d['oldval'],
			'newval'   	 => $d['newval'],
		);
		return $this->db->insert('tb_logs', $data);
	}
}
