<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class AgentModel extends CI_Model {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();

	}

	public function create($d = null) {

		$data = array(
			'web'    	 => $d['web'],
			'user'       => strtoupper($d['user']),
			'name'   	 => strtoupper($d['name']),
			'pass'   	 => $d['pass'],
			'credit_bl'  => $d['credit_bl'],
			'total_member_credit'   	 => $d['total_member_credit'],
			'company'    => $d['company'],
			'status'     => '1',
			'created_by' => $d['created_by'],
			'created_at' => date('Y-m-d H:i:s'),
		);

		$this->db->insert('tb_agent', $data);
		return $this->db->insert_id();

	}

	public function update($id, $d = null) {

		$data = array(
			'web'    	 => $d['web'],
			'user'       => strtoupper($d['user']),
			'name'   	 => strtoupper($d['name']),
			'pass'   	 => $d['pass'],
			'credit_bl'  => $d['credit_bl'],
			'total_member_credit'   	 => $d['total_member_credit'],
			'company'    => $d['company'],
			'updated_by' => $d['updated_by'],
			'updated_at' => date('Y-m-d H:i:s'),
		);

		$this->db->where('id', $id);
		return $this->db->update('tb_agent', $data);

	}
	
	public function get_data_byid($id) {
		$this->db->from('tb_agent');
		$this->db->where('id', $id);
		return $this->db->get()->row();
	}

	public function all_data_cnt($role) {
		$this->db->from('tb_agent');
		if($role != '1'){
			$this->db->where('company', $role);
		}
		return $this->db->get()->num_rows();
	}

	public function all_data($role) {
		$this->db->from('tb_agent');
		$this->db->where('status', '1');
		$this->db->order_by("id", "desc");
		return $this->db->get()->result();
	}

	public function get_data_byweb($web,$db) {
		$this->db = $this->load->database($db, TRUE);
		$sql = "SELECT * FROM tb_agent WHERE web = '".$web."' and status = '1' ORDER BY id ASC";
		$query = $this->db->query($sql);
		$bank =  $query->result_array();
		$res  = array();
		foreach ($bank as $k => $v) {
			$res[] = array(
				'web' => $v['web'],
				'user' => $v['name'],
			);
		}

		return $res;
	}

	public function get_allagent() {
		$sql = "SELECT * FROM tb_agent WHERE status = '1' ORDER BY id ASC";
		$query = $this->db->query($sql);
		$bank =  $query->result_array();
		$res  = array();
		foreach ($bank as $k => $v) {
			$res[] = array(
				'web' => $v['web'],
				'user' => $v['name'],
			);
		}

		return $res;
	}

	public function data_page($page, $per_page,$role) {
		if($role == '1'){
			$sql = "SELECT * FROM tb_agent WHERE status = '1' ORDER BY ID ASC LIMIT $page,$per_page";
		}else{
			$sql = "SELECT * FROM tb_agent WHERE company = '".$role."' and status = '1' ORDER BY ID DESC LIMIT $page,$per_page";
		}
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function search_data($d,$role,$page,$per_page) {
		$con = '';

		if($d != ''){

			if($d['web'] != ''){
				$con .= "AND web like '%".$d['web']."%' ";
			}

			if($d['agent'] != ''){
				$con .= "AND user like '%".$d['agent']."%' ";
			}
		}

		// if($role == '1' or $role == '6'){
			$sql = "SELECT * FROM tb_agent WHERE status = '1' $con ORDER BY web DESC LIMIT $page,$per_page";
		// }else{
			// $sql = "SELECT * FROM tb_customer WHERE company = '".$role."' and status = '1' $con ORDER BY id DESC LIMIT $page,$per_page";
		// }
		$query = $this->db->query($sql);
		return $query->result();
	}


	public function del_data($id) {
		// $this->db->where('id', $id);
		// return $this->db->delete('tb_agent');
		$sql = "update tb_agent set status = '0' where id = '".$id."' ";
		return $this->db->query($sql);
	}

	public function update_run_status($id,$status,$by) {
		// $sql = "update tb_agent set run_status = '".$status."' where id = '".$id."' ";
		// return $this->db->query($sql);

		$data = array(
			'run_status' => $status
		);

		if($status == '1'){
			$data['open_by']    = $by;
			$data['open_time']  = date('Y-m-d H:i:s');

			$data['close_by']    = '';
			$data['close_time']  = '0000-00-00 00:00:00';
		}else{
			$data['close_by']   = $by;
			$data['close_time'] = date('Y-m-d H:i:s');

			$data['open_by']    = '';
			$data['open_time']  = '0000-00-00 00:00:00';
		}

		$this->db->where('id', $id);
		return $this->db->update('tb_agent', $data);
	}

	public function allag() {
		$con = '';
		$data = array();
		$sql = "SELECT *, NOW() as now ,TIMEDIFF(NOW(),bot_synctime) as svr1 FROM tb_agent WHERE status = '1' $con ORDER BY id DESC";

		$query = $this->db->query($sql);

        $now = new DateTime();
		foreach ($query->result_array() as $k => $v) {
			$date = new DateTime(date('Y-m-d H:i:s',strtotime($v['bot_synctime'])));
			$v['time_over'] = $date->diff($now)->format("%i");
			$v['site'] = 'DEMO';
			$data[] = $v;
		}

		$this->db = $this->load->database('enjoy', TRUE);

		$sql = "SELECT *, NOW() as now ,TIMEDIFF(NOW(),bot_synctime) as svr1 FROM tb_agent WHERE status = '1' $con ORDER BY id DESC";


		$query = $this->db->query($sql);
		foreach ($query->result_array() as $k => $v) {
			$date = new DateTime(date('Y-m-d H:i:s',strtotime($v['bot_synctime'])));
			$v['time_over'] = $date->diff($now)->format("%i");
			$v['site'] = 'ENJOY';
			$data[] = $v;
		}
		$this->db = $this->load->database('asia', TRUE);

		$sql = "SELECT *, NOW() as now ,TIMEDIFF(NOW(),bot_synctime) as svr1 FROM tb_agent WHERE status = '1' $con ORDER BY id DESC";

		$query = $this->db->query($sql);
		foreach ($query->result_array() as $k => $v) {
			$date = new DateTime(date('Y-m-d H:i:s',strtotime($v['bot_synctime'])));
			$v['time_over'] = $date->diff($now)->format("%i");
			$v['site'] = 'ASIA';
			$data[] = $v;
		}
		$this->db = $this->load->database('official', TRUE);

		$sql = "SELECT *, NOW() as now ,TIMEDIFF(NOW(),bot_synctime) as svr1 FROM tb_agent WHERE status = '1' $con ORDER BY id DESC";

		$query = $this->db->query($sql);
		foreach ($query->result_array() as $k => $v) {
			$date = new DateTime(date('Y-m-d H:i:s',strtotime($v['bot_synctime'])));
			$v['time_over'] = $date->diff($now)->format("%i");
			$v['site'] = 'OFFICIAL';
			$data[] = $v;
		}

		return $data;
	}

	public function inslogs($d) {
		$data = array(
			'menu'     	 => 'bank',
			'did'  	 	 => $d['did'],
			'col'   	 => $d['col'],
			'oldval'   	 => $d['oldval'],
			'newval'   	 => $d['newval'],
		);
		return $this->db->insert('tb_logs', $data);
	}
}
