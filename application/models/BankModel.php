<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class BankModel extends CI_Model {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();

	}

	public function create($d = null) {

		$data = array(
			'web'    	 => $d['web'],
			'bankid'     => $d['bankid'],
			'bankname'   => $d['bankname'],
			'bankno'   	 => $d['bankno'],
			'user'   	 => $d['user'],
			'pass'  	 => $d['pass'],
			'company'    => $d['company'],
			'balance'    => $d['balance'],
			'wid_max'    => $d['wid_max'],
			'did_max'    => $d['did_max'],
			'status'     => '1',
			'created_by' => $d['created_by'],
			'created_at' => date('Y-m-d H:i:s'),
		);

		$this->db->insert('tb_bank', $data);
		return $this->db->insert_id();

	}

	public function update($id, $d = null) {

		$data = array(
			'web'    	 => $d['web'],
			'bankid'     => $d['bankid'],
			'bankname'   => $d['bankname'],
			'bankno'   	 => $d['bankno'],
			'user'   	 => $d['user'],
			'pass'  	 => $d['pass'],
			'company'    => $d['company'],
			'balance'    => $d['balance'],
			'wid_max'    => $d['wid_max'],
			'did_max'    => $d['did_max'],
			'updated_by' => $d['updated_by'],
			'updated_at' => date('Y-m-d H:i:s'),
		);

		$this->db->where('id', $id);
		return $this->db->update('tb_bank', $data);

	}
	
	public function get_data_byid($id) {
		$this->db->from('tb_bank');
		$this->db->where('id', $id);
		return $this->db->get()->row();
	}

	public function all_data_cnt($role) {
		$this->db->from('tb_bank');
		if($role != '1'){
			$this->db->where('company', $role);
		}
		return $this->db->get()->num_rows();
	}

	public function all_data($role) {
		$this->db->from('tb_bank');
		$this->db->where('status', '1');
		$this->db->order_by("id", "desc");
		return $this->db->get()->result();
	}

	public function data_page($page, $per_page,$role) {
		if($role == '1'){
			$sql = "SELECT * FROM tb_bank WHERE status = '1' ORDER BY ID ASC LIMIT $page,$per_page";
		}else{
			$sql = "SELECT * FROM tb_bank WHERE company = '".$role."' and status = '1' ORDER BY ID DESC LIMIT $page,$per_page";
		}
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function search_data($d,$role,$page,$per_page) {
		$con = '';

		if($d != ''){

			if($d['web'] != ''){
				$con .= "AND web like '%".$d['web']."%' ";
			}

			if($d['bank'] != ''){
				$con .= "AND bankno like '%".$d['bank']."%' ";
			}

			if($d['bankname'] != ''){
				$con .= "AND bankname like '%".$d['bankname']."%' ";
			}
		}

		// if($role == '1' or $role == '6'){
			// $sql = "SELECT *, DATE_ADD(NOW(), INTERVAL 14 HOUR) as now ,TIMEDIFF(DATE_ADD(NOW(), INTERVAL 14 HOUR),sv1) as svr1,TIMEDIFF(DATE_ADD(NOW(), INTERVAL 14 HOUR),sv2) as svr2,TIMEDIFF(DATE_ADD(NOW(), INTERVAL 14 HOUR),sv3) as svr3 FROM tb_bank WHERE status = '1' $con ORDER BY id DESC LIMIT $page,$per_page";
		$sql = "SELECT *, NOW() as now ,TIMEDIFF(NOW(),sv1) as svr1,TIMEDIFF(NOW(),sv2) as svr2,TIMEDIFF(NOW(),sv3) as svr3 FROM tb_bank WHERE status = '1' $con ORDER BY id DESC LIMIT $page,$per_page";
		// }else{
			// $sql = "SELECT * FROM tb_customer WHERE company = '".$role."' and status = '1' $con ORDER BY id DESC LIMIT $page,$per_page";
		// }
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function getbanklist($role,$c) {
		$this->db = $this->load->database($c, TRUE);
		$sql = "SELECT bankid,bankno,bankname,balance FROM tb_bank WHERE status = '1' ORDER BY id ASC";
		$query = $this->db->query($sql);
		$bank =  $query->result_array();
		$res  = array();
		foreach ($bank as $k => $v) {
			$res[] = array(
				'bankid'    => $v['bankid'],
				'bankno'    => $v['bankno'],
				'bankname'  => $v['bankname'],
				'bankshort' => $v['bankid'],
				'balance' 	=> $v['balance'],
				'bankshow'  => $v['bankid'].'-'.$v['bankname'].'-'.substr($v['bankno'], (strlen($v['bankno'])-4)),
			);
		}

		return $res;
	}

	public function del_data($id) {
		// $this->db->where('id', $id);
		// return $this->db->delete('tb_bank');
		$sql = "update tb_bank set status = '0' where id = '".$id."' ";
		return $this->db->query($sql);
	}

	public function update_run_status($id,$type,$svr,$by) {
		$data = array(
			str_replace('r', '', $svr) => ($type) ? date('Y-m-d H:i:s') : '0000-00-00 00:00:00',
		);

		if($type == '1'){
			$data['open_by']    = $by;
			$data['open_time']  = date('Y-m-d H:i:s');

			$data['close_by']    = '';
			$data['close_time']  = '0000-00-00 00:00:00';
		}else{
			$data['close_by']   = $by;
			$data['close_time'] = date('Y-m-d H:i:s');

			$data['open_by']    = '';
			$data['open_time']  = '0000-00-00 00:00:00';
		}

		$this->db->where('id', $id);
		return $this->db->update('tb_bank', $data);
	}

	public function update_run_status_moni($id,$type,$svr,$by,$db) {
		$data = array(
			str_replace('r', '', $svr) => ($type) ? date('Y-m-d H:i:s') : '0000-00-00 00:00:00',
		);

		$this->db = $this->load->database($db, TRUE);

		if($type == '1'){
			$data['open_by']    = $by;
			$data['open_time']  = date('Y-m-d H:i:s');

			$data['close_by']    = '';
			$data['close_time']  = '0000-00-00 00:00:00';
		}else{
			$data['close_by']   = $by;
			$data['close_time'] = date('Y-m-d H:i:s');

			$data['open_by']    = '';
			$data['open_time']  = '0000-00-00 00:00:00';
		}

		$this->db->where('id', $id);
		return $this->db->update('tb_bank', $data);
	}

	public function allbank() {
		$con = '';
		$data = array();
		$sql = "SELECT *, NOW() as now ,TIMEDIFF(NOW(),sv1) as svr1,TIMEDIFF(NOW(),sv2) as svr2,TIMEDIFF(NOW(),sv3) as svr3 FROM tb_bank WHERE status = '1' $con ORDER BY id DESC";

		$query = $this->db->query($sql);

        $now = new DateTime();
		foreach ($query->result_array() as $k => $v) {
			$date = new DateTime(date('Y-m-d H:i:s',strtotime($v['bot_synctime'])));
			$v['time_over'] = $date->diff($now)->format("%i");
			$v['site'] = 'DEMO';
			$data[] = $v;
		}

		$this->db = $this->load->database('enjoy', TRUE);

		$sql = "SELECT *, NOW() as now ,TIMEDIFF(NOW(),sv1) as svr1,TIMEDIFF(NOW(),sv2) as svr2,TIMEDIFF(NOW(),sv3) as svr3 FROM tb_bank WHERE status = '1' $con ORDER BY id DESC";


		$query = $this->db->query($sql);
		foreach ($query->result_array() as $k => $v) {
			$date = new DateTime(date('Y-m-d H:i:s',strtotime($v['bot_synctime'])));
			$v['time_over'] = $date->diff($now)->format("%i");
			$v['site'] = 'ENJOY';
			$data[] = $v;
		}
		$this->db = $this->load->database('asia', TRUE);

		$sql = "SELECT *, NOW() as now ,TIMEDIFF(NOW(),sv1) as svr1,TIMEDIFF(NOW(),sv2) as svr2,TIMEDIFF(NOW(),sv3) as svr3 FROM tb_bank WHERE status = '1' $con ORDER BY id DESC";

		$query = $this->db->query($sql);
		foreach ($query->result_array() as $k => $v) {
			$date = new DateTime(date('Y-m-d H:i:s',strtotime($v['bot_synctime'])));
			$v['time_over'] = $date->diff($now)->format("%i");
			$v['site'] = 'ASIA';
			$data[] = $v;
		}
		$this->db = $this->load->database('official', TRUE);

		$sql = "SELECT *, NOW() as now ,TIMEDIFF(NOW(),sv1) as svr1,TIMEDIFF(NOW(),sv2) as svr2,TIMEDIFF(NOW(),sv3) as svr3 FROM tb_bank WHERE status = '1' $con ORDER BY id DESC";

		$query = $this->db->query($sql);
		foreach ($query->result_array() as $k => $v) {
			$date = new DateTime(date('Y-m-d H:i:s',strtotime($v['bot_synctime'])));
			$v['time_over'] = $date->diff($now)->format("%i");
			$v['site'] = 'OFFICIAL';
			$data[] = $v;
		}

		$this->db = $this->load->database('play', TRUE);

		$sql = "SELECT *, NOW() as now ,TIMEDIFF(NOW(),sv1) as svr1,TIMEDIFF(NOW(),sv2) as svr2,TIMEDIFF(NOW(),sv3) as svr3 FROM tb_bank WHERE status = '1' $con ORDER BY id DESC";

		$query = $this->db->query($sql);
		foreach ($query->result_array() as $k => $v) {
			$date = new DateTime(date('Y-m-d H:i:s',strtotime($v['bot_synctime'])));
			$v['time_over'] = $date->diff($now)->format("%i");
			$v['site'] = 'PLAY';
			$data[] = $v;
		}

		return $data;
	}

	public function backup_data($db){
		$this->db = $this->load->database($db, TRUE);
		$tb = 'tb_banklog_'.date('Ymd');
		$sql = 'CREATE TABLE IF NOT EXISTS '.$tb.' AS SELECT * FROM tb_banklog';
		return $this->db->query($sql);
	}

	public function backup_del($db){
		$this->db = $this->load->database($db, TRUE);
		$date = date('Y-m-d');
		$sql = "delete from tb_banklog where pdate < '".$date."' and status != '1' ";
		return $this->db->query($sql);
	}

	public function backup_optimize($db){
		$this->db = $this->load->database($db, TRUE);
		$sql = "optimize table tb_banklog_";
		return $this->db->query($sql);
	}

	public function bstatus($id,$data) {
		$this->db->where('id', $id);
		return $this->db->update('tb_bank', $data);
	}

	public function inslogs($d) {
		$data = array(
			'menu'     	 => 'bank',
			'did'  	 	 => $d['did'],
			'col'   	 => $d['col'],
			'oldval'   	 => $d['oldval'],
			'newval'   	 => $d['newval'],
		);
		return $this->db->insert('tb_logs', $data);
	}

	public function get_banklistbyweb($web) {
        $sql   = "select * from tb_bank where web = '$web' and status = '1' ";
        $sql   = "select * from tb_bank where status = '1' ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function update_bank_agent($ag,$bankuse,$type) {
    	if($type == 'did'){
	    	$data = array(
	    		'bankuse' => $bankuse
	    	);
	    }else{
	    	$data = array(
	    		'bankwid' => $bankuse
	    	);
	    }
		$this->db->where('name', $ag);
		return $this->db->update('tb_agent', $data);
	}
}
