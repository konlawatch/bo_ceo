<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class CalsalaryModel extends CI_Model {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();

	}

	public function create($data = array()) {
		//debug($data,true);

		$this->db->insert('tb_incometype', $data);
		return $this->db->insert_id();

	}

	public function update($id, $data = array()) {

		$this->db->where('id',$id);
		return $this->db->update('tb_incometype', $data);

	}

	public function search_data($p,$page,$per_page) {
		$con = '';
		$p['qstatus'] = 1;
		if($p != ''){
			if(isset($p['qtitle'])){
				if($p['qtitle'] != ''){
					$con .= " AND com_name like '%".$p['qtitle']."%'";
				}
			}

			if(isset($p['name_em'])){
				if($p['name_em'] != ''){
					$con .= " AND name = '".$p['name_em']."'";
				}
			}
		}
		$sql = "SELECT * FROM tb_employees WHERE 1=1 $con ORDER BY id DESC LIMIT $page,$per_page";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_company() {
        $sql   = "select * from tb_employees where status = '1' ";
        $query = $this->db->query($sql);
        $row   = array();
        if($query->num_rows() > 0){
            $row = $query->result_array();
        }

        return $row;
    }

	public function get_data_byid($id) {
		$this->db->from('tb_employees');
		$this->db->where('id', $id);
		return $this->db->get()->row();
	}

	public function get_data_byid_inc($id) {
		// debug($id,true);
		$this->db->from('tb_incometype');
		$this->db->where('id', $id);
		return $this->db->get()->row();
	}

	public function get_data_byname($name) {
		$this->db->from('tb_incometype');
		$this->db->where('name', $name);
		return $this->db->get()->row();

		// $sql = "SELECT * FROM tb_incometype where name = '$name' ";
		// $query = $this->db->query($sql);
		// return $query->result_array();
	}

	public function get_data_cal($p) {
		//debug($p,true);
		$d1 = $p['date1'];
		$d2 = $p['date2'];
		$n  = $p['name'];
		$sql = "SELECT * FROM tb_incometype where name = '$n' AND created_at>= '$d1' AND created_at <= '$d2' AND status = '1'";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function inslogs($d) {
		$data = array(
			'menu'     	 => 'deposit',
			'did'  	 	 => $d['did'],
			'col'   	 => $d['col'],
			'oldval'   	 => $d['oldval'],
			'newval'   	 => $d['newval'],
			'created_by' => $d['created_by'],
		);
		return $this->db->insert('tb_logs', $data);
	}

	public function data_company() {

		$sql = "SELECT * FROM tb_company where status = '1' ";

		
		$query = $this->db->query($sql);
		return $query->result_array();

	}

	public function data_com($d) {

		$sql = "SELECT * FROM tb_company where id = '$d' ";
		$query = $this->db->query($sql);
		return $query->result_array();

	}

	public function data_dpm($d) {

		$sql = "SELECT * FROM tb_departments where id = '$d' ";
		$query = $this->db->query($sql);
		return $query->result_array();

	}

	public function data_ps($d) {

		$sql = "SELECT * FROM tb_positions where id = '$d' ";
		$query = $this->db->query($sql);
		return $query->result_array();

	}

	public function data_depart($d) {

		$sql = "SELECT * FROM tb_departments where com_id = $d AND status = 1";
		$query = $this->db->query($sql);
		return $query->result_array();
		//debug($s,true);
	}

	public function data_posi($d,$e) {

		$sql = "SELECT * FROM tb_positions where com_id = $d AND depart_id = $e AND status = 1";
		$query = $this->db->query($sql);
		return $query->result_array();
		//debug($s,true);
	}
}
