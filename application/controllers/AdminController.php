<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AdminController extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->isLoggedIn();   

        $this->website = $this->config->config['website'];
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $content['pagetitle'] = 'Admin '.$this->website.' : Dashboard';
        $content['content'] = $this->load->view('dashboard', $this->global, true);
        $this->load->view('layout/admin', $content);
    }
    
    /**
     * This function is used to load the change password screen
     */
    public function loadChangePass()
    {

        $this->global['pagetitle'] = 'Admin '.$this->website.' : ตั้งค่ารหัสผ่าน';
        $content['content'] = $this->load->view('changePassword',$this->global,true);
        $this->load->view('layout/admin',$content);
    }
    
    
    /**
     * This function is used to change the password of the user
     */
    public function changePassword()
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('oldPassword','Old password','required|max_length[20]');
        $this->form_validation->set_rules('newPassword','New password','required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]|max_length[20]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->loadChangePass();
        }
        else
        {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');
            
            $resultPas = $this->UserModel->matchOldPassword($this->vendorId, $oldPassword);
            
            if(empty($resultPas))
            {
                $this->session->set_flashdata('nomatch', 'Your old password not correct');
                redirect('loadChangePass');
            }
            else
            {
                $usersData = array('password'=>getHashedPassword($newPassword), 'updatedBy'=>$this->vendorId,
                                'updatedDtm'=>date('Y-m-d H:i:s'));
                
                $result = $this->UserModel->changePassword($this->vendorId, $usersData);
                
                if($result > 0) { $this->session->set_flashdata('success', 'Password updation successful'); }
                else { $this->session->set_flashdata('error', 'Password updation failed'); }
                
                redirect('loadChangePass');
            }
        }
    }
}

?>