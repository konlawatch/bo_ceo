<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');

class ApiController extends CI_Controller {

	public function __construct(){
	    parent::__construct();
  	}

	public function index(){
		echo 'denide !!!';
	}

	public function get_contents(){
    	$res = array(
            'status' => false,
            'data'   => '',
            'msg'    => '',
        );
        $get = $this->input->get();
        if($get['sign'] == $this->config->config['key']){
	        $this->load->model('ContentsModel');
	        $d = $this->ContentsModel->get_contents();
	        if($d){
	            $res['status'] = true;
	            $res['data']   = $d;
	        }else{
	            $res['msg'] = 'no data';
	        }
	    }else{
	    	$res['msg'] = 'access denide !!';
	    }
    
        echo json_encode($res);
    }

    public function get_promotions(){
    	$res = array(
            'status' => false,
            'data'   => '',
            'msg'    => '',
        );
        $get = $this->input->get();
        if($get['sign'] == $this->config->config['key']){
	        $this->load->model('PromotionsModel');
	        $d = $this->PromotionsModel->get_promotions();
	        if($d){
	            $res['status'] = true;
	            $res['data']   = $d;
	        }else{
	            $res['msg'] = 'no data';
	        }
	    }else{
	    	$res['msg'] = 'access denide !!';
	    }
    
        echo json_encode($res);
    }
	
	protected function insLogs($action,$data){
		$fp = fopen('request.log', 'a');

		$d['action'] = $action;
		$d['data']   = $data;

		fwrite($fp, print_r($d,true));
		
		fclose($fp);
	}
}
