<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class AccListController extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();   
        $this->website = $this->config->config['website'];
        $this->load->model('UserModel');
        $this->global['pageTitle'] = 'Admin '.$this->website.' : ตั้งค่ารหัสผู้ใช้งาน';

        $allow = array(
            '1','2'
        );
        if(!in_array($this->global['role'], $allow)){
            echo 'denide !!!';exit();
        }
    }
    
    /**
     * This function used to load the first screen of the user
     */

    public function index() {

        $this->load->library('pagination');
        $per_page = 50;
        $page = ($this->uri->segment(3) != '') ? $this->uri->segment(3) : 0; // เลขหน้าที่จะถูกส่งมาเช่น home/member/3
        $config['base_url'] = base_url() . 'acclist/page'; // ชี้หน้าเพจหลักที่จะใช้งานมาที่ home/member
        $config['total_rows'] = $this->UserModel->all_data_cnt(); // จำนวนข้อมูลทั้งหมด
        $config['per_page'] = $per_page; // จำนวนข้อมูลต่อหน้า

        $config['use_page_numbers'] = TRUE; // เพื่อให้เลขหน้าในลิงค์ถูกต้อง ให้เซตค่าส่วนนี้เป็น TRUE
        $config['full_tag_open'] = '<ul class="pagination" style="margin:10px 0;">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['num_links'] = 10;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#" >';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $this->global['pagination'] = $this->pagination->create_links(); // เลขหน้า
        $this->global['data'] = $this->UserModel->data_page($page, $per_page);
        $content['pagetitle'] = 'ตั้งค่ารหัสผู้ใช้งาน';
        $content['content'] = $this->load->view('acclist/index', $this->global, true);
        $this->load->view('layout/admin', $content);
    }

    public function page() {
        $per_page = 50;
        $page = ($this->uri->segment(4) != '') ? (($this->uri->segment(4) - 1) * $per_page) : 0; // เลขหน้าที่จะถูกส่งมาเช่น home/member/3
        $config['base_url'] = base_url() . 'acclist/page'; // ชี้หน้าเพจหลักที่จะใช้งานมาที่ home/member
        $config['total_rows'] = $this->UserModel->all_data_cnt(); // จำนวนข้อมูลทั้งหมด
        $config['per_page'] = $per_page; // จำนวนข้อมูลต่อหน้า

        $config['use_page_numbers'] = TRUE; // เพื่อให้เลขหน้าในลิงค์ถูกต้อง ให้เซตค่าส่วนนี้เป็น TRUE

        $config['full_tag_open'] = '<ul class="pagination" style="margin:10px 0;">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['num_links'] = 10;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#" >';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $this->global['pagination'] = $this->pagination->create_links(); // เลขหน้า
        $this->global['data'] = $this->UserModel->data_page($page, $per_page);
        $content['pagetitle'] = 'ตั้งค่ารหัสผู้ใช้งาน';
        $content['content'] = $this->load->view('acclist/index', $this->global, true);
        $this->load->view('layout/admin', $content);
    }

    public function add() {
        $content['pagetitle'] = 'เพิ่มรหัสผู้ใช้งาน';
        $this->global['rolelist']  = $this->UserModel->getUserRoles();
        $content['content'] = $this->load->view('acclist/form', $this->global, true);
        $this->load->view('layout/admin', $content);
    }

    public function edit($id) {
        if ($id != '') {
            $this->global['data'] = $this->UserModel->getUserInfoById($id);
            $this->global['rolelist']  = $this->UserModel->getUserRoles();
            $content['pagetitle'] = 'แก้ไขรหัสผู้ใช้งาน';
            $content['content'] = $this->load->view('acclist/form', $this->global, true);
            $this->load->view('layout/admin', $content);
        } else {
            redirect('/acclist');
        }
    }

    public function create() {
        $post = $this->input->post();
        // debug($post,true);
        if ($post['username'] != '' && $post['roleId'] != '') {
            if($post['pass'] == $post['conpass']){
                $d = array(
                    'username' => $post['username'],
                    'name'     => $post['name'],
                    'password' => getHashedPassword($post['pass']),
                    'roleId'   => $post['roleId'],
                );
                if (!$this->UserModel->UserExists($post['username'])) {
                    if ($this->UserModel->addNewUser($d)) {

                        $this->session->set_flashdata('success', 'success on save data');

                        redirect('/acclist');

                    } else {

                        $this->session->set_flashdata('error', 'erro on save data');

                        redirect('/acclist/add');
                    }
                } else {

                    $this->session->set_flashdata('error', 'UserExists data');

                    redirect('/acclist/add');
                }
            }else{
                $this->session->set_flashdata('error', 'pass no match conpass');

                redirect('/acclist/add');
            }
        } else {

            $this->session->set_flashdata('error', 'username and roleId is not null.');

            redirect('/acclist/add');

        }
    }

    public function update($id) {
        $post = $this->input->post();

        if ($post['username'] != '' && $post['roleId'] != '') {
            if($post['pass'] == $post['conpass']){
                $olddata = json_decode($post['oldval'],true);
                foreach ($post as $k => $v) {
                    if($k != 'oldval' && $k != 'pass'  && $k != 'conpass' && $k != 'email' && $k != 'mobile'){
                        if($olddata[$k] != $post[$k]){
                            $log = array(
                                'did'        => $id,
                                'col'        => $k,
                                'newval'     => $post[$k],
                                'oldval'     => $olddata[$k],
                            );
                            $this->UserModel->inslogs($log);
                        }
                    }
                }
                $d = array(
                    'username' => $post['username'],
                    'name'     => $post['name'],
                    'password' => getHashedPassword($post['pass']),
                    'roleId'   => $post['roleId'],
                );
                if ($this->UserModel->editUser($d, $id)) {

                    $this->session->set_flashdata('success', 'update data success');

                    redirect('/acclist');

                } else {

                    $this->session->set_flashdata('error', 'erro on update data');

                    redirect('/acclist/edit/' . $id);

                }
            } else {

                $this->session->set_flashdata('error', 'pass no match conpass');

                redirect('/acclist/edit/' . $id);

            }
        } else {
            $this->session->set_flashdata('error', 'username and roleId is not null.');

            redirect('/acclist/edit/' . $id);
        }
    }

    public function delete($id) {
        if ($id != '') {
            if ($this->UserModel->del_data($id)) {
                $this->session->set_flashdata('success', 'delete data success');
            } else {
                $this->session->set_flashdata('error', 'error on delete data');
            }
            redirect('/acclist');
        } else {
            redirect('/acclist');
        }
    }

}

?>