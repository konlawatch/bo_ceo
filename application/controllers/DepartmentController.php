<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : DevDevel
 * @version : 1.1
 */
class DepartmentController extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();   
        $this->website             = $this->config->config['website'];
        $this->global['pageTitle'] = $this->website.' : '.strtoupper($this->uri->segment(1));
    }
    
    /**
     * This function used to load the first screen of the user
     */

    public function index() {
        $content['pagetitle'] = $this->global['pageTitle'];
        $this->load->model('DepartmentModel');
        $this->global['company'] = $this->DepartmentModel->data_company();
        $this->global['status'] = $this->config->config['status'];
        $content['content']   = $this->load->view($this->uri->segment(1).'/main', $this->global, true);
        $this->load->view('layout/admin', $content);
    }

    public function search(){
        $res = array(
            'status' => true,
            'data'   => '',
            'msg'    => '',
        );
        $post = $this->input->post();
        //debug($post,true);
        $this->load->model('DepartmentModel');
        $this->global['data'] = $this->DepartmentModel->search_data($post,$post['qpage'],$post['qper_page']);
        $this->global['status'] = $this->config->config['status'];
        $view = $this->load->view($this->uri->segment(1).'/list',$this->global,true);
        $res['data']   = str_replace('   ', '', $view);
        //debug($res,true);
        echo json_encode($res);
    }

    public function add() {
        $this->load->model('DepartmentModel');
        $this->global['company'] = $this->DepartmentModel->data_company();
        $view = $this->load->view($this->uri->segment(1).'/form',$this->global,true);
        $res['temp']  = str_replace('   ', '', $view);
        $this->output->set_output(json_encode($res));
    }

    public function edit($id) {
        if ($id != '') {
            $this->load->model('DepartmentModel');
            $this->global['data'] = $this->DepartmentModel->get_data_byid($id);
            $this->load->model('DepartmentModel');
            $this->global['company'] = $this->DepartmentModel->data_company();
            $view = $this->load->view($this->uri->segment(1).'/form',$this->global,true);
            $res['temp']  = str_replace('   ', '', $view);
        }

        $this->output->set_output(json_encode($res));
    }

    public function create() {
        $post = $this->input->post();
        $this->load->model('DepartmentModel');
        $res = array(
            'status' => false,
            'msg'    => '',
        );
        //debug($post,true);
        if ($post['title'] != '' && $post['status'] != '') {

            $com = $this->DepartmentModel->data_com($post['status']);
            //debug($com,true);
            $data = array(
                'com_id'     => $post['status'],
                'com_name'   => $com[0]['name'],
                'name'       => $post['title'],
                'desc'       => $post['desc'],
                'status'     => 1,
                'created_at' => date('Y-m-d H:i:s'),    
            );
            
            $ins_id = $this->DepartmentModel->create($data);
            if ($ins_id) {
                $res['status'] = true;
                $res['msg'] = 'success';
            } else {
                $res['msg'] = 'error !!!';
            }
        } else {

            $res['msg'] = 'title & desc is null !!!';

        }

        echo json_encode($res);
    }

    public function update($id) {
        $post = $this->input->post();

         //debug($post,true);

        if ($post['title'] != '' && $post['desc'] != '') {
            $this->load->model('DepartmentModel');
            $com = $this->DepartmentModel->data_com($post['status']);
            $data = array(
                'com_id'     => $post['status'],
                'com_name'   => $com[0]['name'],
                'name'       => $post['title'],
                'desc'       => $post['desc'],
                'status'     => 1,
                'created_at' => date('Y-m-d H:i:s'),       
            );

            if ($this->DepartmentModel->update($id, $data)) {
                $res['status'] = true;
                $res['msg'] = 'success';
            } else {
                $res['msg'] = 'error !!!';
            }
        } else {
            $res['msg'] = 'title & desc is null !!!';
        }
        echo json_encode($res);
    }


    public function delete($id) {
        if ($id != '') {
            $this->load->model('DepartmentModel');
            $data = array(
                'status' => '0'
            );
            if ($this->DepartmentModel->update($id,$data)) {
                $this->session->set_flashdata('success', 'delete data success');
            } else {
                $this->session->set_flashdata('error', 'error on delete data');
            }
            redirect('/'.$this->uri->segment(1));
        } else {
            redirect('/'.$this->uri->segment(1));
        }
    }
}

?>