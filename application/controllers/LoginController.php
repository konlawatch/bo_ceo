<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('LoginModel');
	}

	public function index()
    {
        $this->isLoggedIn();
    }
    
    /**
     * This function used to check the user is logged in or not
     */
    function isLoggedIn()
    {
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            $this->load->view('layout/login');
        }
        else
        {
            redirect('/balance');
        }
    }

    /**
     * This function used to logged in user
     */
    public function loginMe()
    {
        $this->load->library('form_validation');
        
        // $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
        $this->form_validation->set_rules('username', 'Username', 'required|max_length[32]');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $username = $this->security->xss_clean($this->input->post('username'));
            $password = $this->input->post('password');
            
            $result = $this->LoginModel->loginMe($username, $password);
            
            if(!empty($result))
            {
                $lastLogin = $this->LoginModel->lastLoginInfo($result->userId);

                $sessionArray = array(
                    'userId'        => $result->userId,                    
                    'role'          => $result->roleId,
                    'roleText'      => $result->role,
                    'name'          => $result->name,
                    'lastLogin'     => $lastLogin->createdDtm,
                    'isLoggedIn'    => TRUE
                );

                $this->session->set_userdata($sessionArray);

                unset($sessionArray['userId'], $sessionArray['isLoggedIn'], $sessionArray['lastLogin']);

                $loginInfo = array("userId"=>$result->userId, "sessionData" => json_encode($sessionArray), "machineIp"=>$_SERVER['REMOTE_ADDR'], "userAgent"=>getBrowserAgent(), "agentString"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());

                $this->LoginModel->lastLogin($loginInfo);

                if($result->roleId == 1){
                
                    redirect('/dashboard');
                }else{
                    redirect('/dashboard');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'username or password mismatch');
                
                redirect('/login');
            }
        }
    }
}
