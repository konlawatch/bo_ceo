<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : DevDevel
 * @version : 1.1
 */
class CompanyController extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();   
        $this->website             = $this->config->config['website'];
        $this->global['pageTitle'] = $this->website.' : '.strtoupper($this->uri->segment(1));
    }
    
    /**
     * This function used to load the first screen of the user
     */

    public function index() {
        $this->load->model('CompanyModel');
        $this->global['company'] = $this->CompanyModel->data_company();
        $content['pagetitle'] = $this->global['pageTitle'];
        $this->global['status'] = $this->config->config['status'];
        $content['content']   = $this->load->view($this->uri->segment(1).'/main', $this->global, true);
        $this->load->view('layout/admin', $content);
    }

    public function search(){
        $res = array(
            'status' => true,
            'data'   => '',
            'msg'    => '',
        );
        $post = $this->input->post();
        //debug($post,true);
        $this->load->model('CompanyModel');
        $this->global['data'] = $this->CompanyModel->search_data($post,$post['qpage'],$post['qper_page']);
        $this->global['status'] = $this->config->config['status'];
        $view = $this->load->view($this->uri->segment(1).'/list',$this->global,true);
        $res['data']   = str_replace('   ', '', $view);
        //debug($res,true);
        echo json_encode($res);
    }

    public function add() {
        $view = $this->load->view($this->uri->segment(1).'/form',$this->global,true);
        $res['temp']  = str_replace('   ', '', $view);
        $this->output->set_output(json_encode($res));
    }

    public function edit($id) {
        if ($id != '') {
            $this->load->model('CompanyModel');
            $this->global['data'] = $this->CompanyModel->get_data_byid($id);
            $view = $this->load->view($this->uri->segment(1).'/form',$this->global,true);
            $res['temp']  = str_replace('   ', '', $view);
        }

        $this->output->set_output(json_encode($res));
    }

    public function create() {
        $post = $this->input->post();
        $res = array(
            'status' => false,
            'msg'    => '',
        );
        //debug($post,true);
        if ($post['title'] != '' && $post['desc'] != '') {

           
            // $config['upload_path'] = './assets/uploads/';
            // $config['allowed_types'] = 'jpg|png';
            // $this->load->library('upload', $config);

            // if(isset($_FILES['file'])){
            //     if($_FILES['file']['name'] != ''){

            //         if (!$this->upload->do_upload('file')) {
            //             $res['msg'] = $this->upload->display_errors();
            //         }else{
            //             $img = $this->upload->data();
            //             $data['img_url'] = $img['file_name'];
            //             $data['img_url'] ='http://localhost/bo_ceo'.'/'. $config['upload_path'].'/'. $img['file_name'];

            //         } 
            //     }
            // }else{
            //     $data['img_url'] = '';
            // }

            $data = array(
                'name'      => $post['title'],
                'desc'      => $post['desc'],
                'status'    => 1,
                // 'img_url'    => $data['img_url'],
                'created_at' => date('Y-m-d H:i:s'),
                // 'created_by' => $this->global['name'],
                // 'updated_at' => $this->global['name'],
            );


            $this->load->model('CompanyModel');
            $ins_id = $this->CompanyModel->create($data);
            if ($ins_id) {
                $res['status'] = true;
                $res['msg'] = 'success';
            } else {
                $res['msg'] = 'error !!!';
            }
        } else {

            $res['msg'] = 'title & desc is null !!!';

        }

        echo json_encode($res);
    }

    public function update($id) {
        $post = $this->input->post();

        // debug($post,true);

        if ($post['title'] != '' && $post['desc'] != '') {

            // $config['upload_path'] = './assets/uploads/';
            // $config['allowed_types'] = 'jpg|png';
            // $this->load->library('upload', $config);

            // if(isset($_FILES['file'])){
            //     if($_FILES['file']['name'] != ''){

            //         if (!$this->upload->do_upload('file')) {
            //             $res['msg'] = $this->upload->display_errors();
            //         }else{
            //             $img = $this->upload->data();
            //             $data['img_url'] = $img['file_name'];
            //             $data['img_url'] ='http://localhost/bo_ceo'.'/'. $config['upload_path'].'/'. $img['file_name'];

            //         } 
            //     }
            // }

            $data = array(
                'name'     => $post['title'],
                'desc'  => $post['desc'],
                // 'img_url'    => $data['img_url'],
                'updated_at' => date('Y-m-d H:i:s'),
                // 'created_by' => $this->global['name'],
                // 'updated_by' => $this->global['name'],
            );
         
            $this->load->model('CompanyModel');
            if ($this->CompanyModel->update($id, $data)) {
                $res['status'] = true;
                $res['msg'] = 'success';
            } else {
                $res['msg'] = 'error !!!';
            }
        } else {
            $res['msg'] = 'title & desc is null !!!';
        }
        echo json_encode($res);
    }


    public function delete($id) {
        if ($id != '') {
            $this->load->model('CompanyModel');
            $data = array(
                'status' => '0'
            );
            if ($this->CompanyModel->update($id,$data)) {
                $this->session->set_flashdata('success', 'delete data success');
            } else {
                $this->session->set_flashdata('error', 'error on delete data');
            }
            redirect('/'.$this->uri->segment(1));
        } else {
            redirect('/'.$this->uri->segment(1));
        }
    }
}

?>