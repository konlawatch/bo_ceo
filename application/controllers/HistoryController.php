<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class HistoryController extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();   
        $this->website = $this->config->config['website'];
        $this->global['pageTitle'] = 'Admin '.$this->website.' : ประวัติการฝาก-ถอน';
    }
    
    /**
     * This function used to load the first screen of the user
     */

    public function index() {
        $this->global['sitelist'] = $this->config->config['sitelist'];
        $content['pagetitle'] = 'ประวัติการฝาก-ถอน';
        $content['content'] = $this->load->view('history/main', $this->global, true);
        $this->load->view('layout/admin', $content);
    }

    public function search(){
        $res = array(
            'status' => false,
            'data'   => '',
            'msg'    => '',
        );
        $post = $this->input->post();
        if(isset($post['userid']) && isset($post['name']) && isset($post['web'])  && isset($post['ag'])){
            $this->load->model('HistoryModel');
            $this->global['data']   = $this->HistoryModel->search_data($post,$post['page'],$post['per_page']);
            $res['data']  = $this->load->view('history/list',$this->global,true);
            $res['status'] = true;
        }else{
            $res['msg'] = 'no param userid | name | web | ag';
        }
        echo json_encode($res);
    }

    public function balance($bdate){
        $res = array(
            'status' => false,
            'data'   => '',
            'msg'    => '',
        );
        if($bdate != ''){
            $this->global['sitelist'] = $this->config->config['sitelist'];
            $this->global['bdate']    = $bdate;
            $content['pagetitle'] = 'รายการปิดยอด วันที่ '.$bdate;
            $content['content']   = $this->load->view('history/m', $this->global, true);
            $this->load->view('layout/blank', $content);
        }else{
            $res['msg'] = 'no param bdate null';
            echo json_encode($res);
        }
    }

    public function s(){
        $res = array(
            'status' => false,
            'data'   => '',
            'msg'    => '',
        );
        $post = $this->input->post();
        if(isset($post['userid']) && isset($post['web'])  && isset($post['ag'])){
            $this->load->model('HistoryModel');
            $this->global['data']   = $this->HistoryModel->search_databybdate($post,$post['page'],$post['per_page']);
            $res['data']  = $this->load->view('history/l',$this->global,true);
            $res['status'] = true;
        }else{
            $res['msg'] = 'no param userid | web | ag';
        }
        echo json_encode($res);
    }
}

?>