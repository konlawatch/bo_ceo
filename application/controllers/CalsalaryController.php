<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : DevDevel
 * @version : 1.1
 */
class CalsalaryController extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();   
        $this->website             = $this->config->config['website'];
        $this->global['pageTitle'] = $this->website.' : '.strtoupper($this->uri->segment(1));
    }
    
    /**
     * This function used to load the first screen of the user
     */

    public function index() {
        $content['pagetitle'] = $this->global['pageTitle'];
        $this->load->model('CalsalaryModel');
        $this->global['company'] = $this->CalsalaryModel->data_company();
        $this->global['status_w'] = $this->config->config['status_w'];
        $content['content']   = $this->load->view($this->uri->segment(1).'/main', $this->global, true);
        $this->load->view('layout/admin', $content);
    }

    public function search(){
        $res = array(
            'status' => true,
            'data'   => '',
            'msg'    => '',
        );
        $post = $this->input->post();
        //debug($post,true);
        $this->load->model('CalsalaryModel');
        $this->global['data'] = $this->CalsalaryModel->search_data($post,$post['qpage'],$post['qper_page']);
        $this->global['status'] = $this->config->config['status'];
        $view = $this->load->view($this->uri->segment(1).'/list',$this->global,true);
        $res['data']   = str_replace('   ', '', $view);
        //debug($res,true);
        echo json_encode($res);
    }

    public function search_dep(){ 
        $res = array(
            'status' => false,
            'data1'   => '',
            'msg'    => '',
        );
        $post = $this->input->post();
                //Debug($post,true);
        $this->load->model('CalsalaryModel');
        $this->global['company'] = $this->CalsalaryModel->data_company();
        $this->load->model('CalsalaryModel');
        $d = $this->CalsalaryModel->data_depart($post['x']);
            if($d){
                $res['status'] = true;
                $data1 = array(
                    'd' => $d

                );
                $res['data1'] = $d;
                      
                $view = $this->load->view('calsalary/form',$data1,true);
                        // $res['data']  = str_replace('   ', '', $view);
                       
            }else{
                $res['msg'] = 'no data';
                       
            }           
        $this->output->set_output(json_encode($res));
                 
                            
    }

    public function search_data(){ 
        $res = array(
            'status' => true,
            'data'   => '',
            'msg'    => '',
        );

        $post = $this->input->post();
        //debug($post,true);
        $this->load->model('CalsalaryModel');
        $this->global['data'] = $this->CalsalaryModel->get_data_cal($post);
        $this->global['status'] = $this->config->config['status'];
        $view = $this->load->view($this->uri->segment(1).'/list_e',$this->global,true);
        $res['data']   = str_replace('   ', '', $view);
        //debug($res,true);
        echo json_encode($res);    
                            
    }

    
    public function add($id) {
         if ($id != '') {
            $this->load->model('CalsalaryModel');
            $this->global['data'] = $this->CalsalaryModel->get_data_byid($id);
            $this->global['status_w'] = $this->config->config['status_w'];
            $this->load->model('CalsalaryModel');
            $this->global['company'] = $this->CalsalaryModel->data_company();
            $view = $this->load->view($this->uri->segment(1).'/form',$this->global,true);
            $res['temp']  = str_replace('   ', '', $view);
        }

        $this->output->set_output(json_encode($res));
    }

    public function edit_inc($id) {
         if ($id != '') {
            $this->load->model('CalsalaryModel');
            $this->global['data'] = $this->CalsalaryModel->get_data_byid_inc($id);
            $this->global['status_w'] = $this->config->config['status_w'];
            $this->load->model('CalsalaryModel');
            $this->global['company'] = $this->CalsalaryModel->data_company();
            $view = $this->load->view($this->uri->segment(1).'/form',$this->global,true);
            $res['temp']  = str_replace('   ', '', $view);
        }

        $this->output->set_output(json_encode($res));
    }

    public function edit($name) {
        if ($name != '') {
            $this->load->model('CalsalaryModel');
            $this->global['data'] = $this->CalsalaryModel->get_data_byname($name);
            //$dd = $this->global['data'];
            //debug($dd,true);
            $this->global['status_w'] = $this->config->config['status_w'];
            $this->load->model('CalsalaryModel');
            $this->global['company'] = $this->CalsalaryModel->data_company();
            $view = $this->load->view($this->uri->segment(1).'/form_edit',$this->global,true);
            $res['temp']  = str_replace('   ', '', $view);
        }

        $this->output->set_output(json_encode($res));
    }

    // public function barcode() {
    //     $post = $this->input->post();
    //     $code = $post['key'];
    //     //debug($post,true);
    //     $this->load->model('SettingModel');
    //     if ($code !='' ) {

    //         $res = $this->SettingModel->getdata_code_product($code);
    //     }
    //     $this->output->set_output(json_encode($res));
    // }

    public function create($id) {
        $post = $this->input->post();
        $this->load->model('CalsalaryModel');
        $res = array(
            'status' => false,
            'msg'    => '',
        );
        //debug($post,true);
        if ($post['title'] != '' && $post['desc'] != ''){

            $data = array(
                'name'      => $post['title'],
                'desc'      => $post['desc'],
                'amount'    => $post['amount'],
                'status'    => 1,
                'created_at'    => date('Y-m-d H:i:s'),    
            );
            //debug($data,true);
            $this->load->model('CalsalaryModel');
            $ins_id = $this->CalsalaryModel->create($data);
            if ($ins_id) {
                $res['status'] = true;
                $res['msg'] = 'success';
            } else {
                $res['msg'] = 'error !!!';
            }
        } else {

            $res['msg'] = 'title & desc is null !!!';

        }

        echo json_encode($res);
    }

    public function update($id) {
        $post = $this->input->post();

        //debug($post,true);

        if ($post['title'] != '' && $post['desc'] != '') {

             $data = array(
                'desc'        => $post['desc'],
                'amount'      => $post['amount'],
                'status'      => 1,
                'updated_at'  => date('Y-m-d H:i:s'),    
            );
           
            //debug($data,true);
            $this->load->model('CalsalaryModel');
            if ($this->CalsalaryModel->update($id, $data)) {
                $res['status'] = true;
                $res['msg'] = 'success';
            } else {
                $res['msg'] = 'error !!!';
            }
        } else {
            $res['msg'] = 'title & desc is null !!!';
        }
        echo json_encode($res);
    }


    public function delete($id) {
        //debug($id);
        if ($id != '') {
            
            $data = array(
                'status' => '0'
            );
            //debug($data,true);
            $this->load->model('CalsalaryModel');
            if ($this->CalsalaryModel->update($id,$data)) {
                $this->session->set_flashdata('success', 'delete data success');
            } else {
                $this->session->set_flashdata('error', 'error on delete data');
            }
            redirect('/'.$this->uri->segment(1));
        } else {
            redirect('/'.$this->uri->segment(1));
        }
    }
}

?>