<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class BalanceController extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();   
        $this->website = $this->config->config['website'];
        $this->load->model('BalanceModel');
        $this->global['pageTitle'] = 'Admin '.$this->website.' : ลูกค้า';
    }
    
    /**
     * This function used to load the first screen of the user
     */

    public function index() {
        // $this->load->model('AgentModel');
        // $ag = $this->AgentModel->all_data($this->global['role']);
        // $aglist = array();
        // foreach ($ag as $k => $v) {
        //     $aglist[$v->web][] = $v;
        // }
        // $this->global['agentlist'] = $aglist;
        // debug($this->global['agentlist'],true);

        // $sdate = date('Y-m-d 11:00:00');
        // if (date('Y-m-d H:i:s') <= $sdate) {
        //     $cdate = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
        // }else{
        //     $cdate = date('Y-m-d');
        // }
        // $this->load->model('BankModel');
        // $this->global['banklist']  = $this->BankModel->getbanklist($this->global['role']);
        $blist = $this->BalanceModel->get_balance();
        if($blist){
            $this->global['balancelist'] = $blist;
            $this->global['otherlist']   = $this->BalanceModel->get_otherlist($blist[0]->bdate);
            $this->global['banklist']    = $this->BalanceModel->get_banklist($blist[0]->bdate);
            $this->global['bldate']      = $blist[0]->bdate;
            $this->global['flag']        = $blist[0]->flag;
            $ag = $this->BalanceModel->get_agentlist($blist[0]->bdate);
            $aglist = array();
            foreach ($ag as $k => $v) {
                // if($v->web == 'IMI'){
                //     $v->af_amount = $v->credit_change;
                // }
                $aglist[$v->web][] = $v;
            }
            $this->global['agentlist']   = $aglist;
        }else{
            $this->global['balancelist'] = array();
            $this->global['otherlist']   = array();
            $this->global['banklist']    = array();
            $this->global['agentlist']   = array();
            $this->global['bldate']      = '0000-00-00';
            $this->global['flag']        = 'N';
        }
        $this->load->model('BankModel');
        $this->load->model('AgentModel');
        $this->global['bankidlist']  = $this->BankModel->getbanklist($this->global['role']);
        $this->global['agdlist']     = $this->AgentModel->all_data($this->global['role']);
        $content['pagetitle']        = 'ปิดยอด';
        $content['content']          = $this->load->view('balance/index', $this->global, true);
        $this->load->view('layout/admin', $content);
    }

    public function addother() {
        $post = $this->input->post();
        // debug($post,true);
        $res = array(
            'status' => false,
            'data'   => '',
            'msg'    => '',
        );

        $blist = $this->BalanceModel->get_balance();

        if ($post['name'] != '') {
            if($post['bank'] != ''){
                $bb = explode('-', $post['bank']);
                $post['bankid']     = $bb[0];
                $post['bankno']     = $bb[1].'-'.date('is');
                $post['type']       = 'O';
            }else{
                $bb = explode('-', $post['ag']);
                $post['bankid']     = $bb[0];
                $post['bankno']     = $bb[1].'-'.date('is');
                $post['type']       = 'G';
            }

            $post['web']        = '';
            $post['af_amount']  = '0';
            $post['fee']        = '0';
            $post['cdate']      = ($blist) ? $blist[0]->bdate : '';
            $post['created_by'] = $this->global['name'];
            $post['status']     = '1';
            $post['created_at'] = date('Y-m-d H:i:s');
            if ($this->BalanceModel->create($post)) {
                $res['status'] = true;
            } else {
                $res['msg'] = 'create error';
            }
        } else {

            $res['msg'] = 'name is null';

        }
        echo json_encode($res);
    }

    public function addbank() {
        $post = $this->input->post();
        $res = array(
            'status' => false,
            'data'   => '',
            'msg'    => '',
        );

        $blist = $this->BalanceModel->get_balance();

        if ($post['name'] != '') {
            $post['cdate']      = ($blist) ? $blist[0]->bdate : '';
            $post['type']       = 'B';
            $post['created_by'] = $this->global['name'];
            $post['status']     = '1';
            $post['created_at'] = date('Y-m-d H:i:s');
            if ($this->BalanceModel->create($post)) {
                $res['status'] = true;
            } else {
                $res['msg'] = 'create error';
            }
        } else {

            $res['msg'] = 'name is null';

        }
        echo json_encode($res);
    }

    public function addagent() {
        $post = $this->input->post();
        $res = array(
            'status' => false,
            'data'   => '',
            'msg'    => '',
        );

        $blist = $this->BalanceModel->get_balance();

        if ($post['name'] != '') {
            $post['cdate']      = ($blist) ? $blist[0]->bdate : '';
            $post['type']       = 'A';
            $post['created_by'] = $this->global['name'];
            $post['status']     = '1';
            $post['created_at'] = date('Y-m-d H:i:s');
            if ($this->BalanceModel->create($post)) {
                $res['status'] = true;
            } else {
                $res['msg'] = 'create error';
            }
        } else {

            $res['msg'] = 'name is null';

        }
        echo json_encode($res);
    }

    public function update($id) {
        $post = $this->input->post();

        $res = array(
            'status' => false,
            'data'   => '',
            'msg'    => '',
        );

        if($post['value'] != ''){
            $log = array(
                'did'        => $id,
                'col'        => $post['table_column'],
                'newval'     => $post['value'],
                'oldval'     => $post['oldval'],
                'created_by' =>  $this->global['name'],
            );
            $this->BalanceModel->inslogs($log);
            $data = array(
                $post['table_column'] =>  $post['value'],
                'updated_by'  =>  $this->global['name'],
            );
            if ($this->BalanceModel->update($id, $data)) {
                $res['status'] = true;
            }else{
                $res['msg'] = 'update error';
            }
        }else{
            $res['msg'] = 'value is null';
        }
        echo json_encode($res);
    }

    public function new_period(){

        $sdate = date('Y-m-d 11:00:00');
        if (date('Y-m-d H:i:s') <= $sdate) {
            $cdate = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
        }else{
            $cdate = date('Y-m-d');
        }

        $d = array(
            'bdate'      => $cdate,
            'did_cnt'    => '0',
            'did_amount' => '0',
            'wid_cnt'    => '0',
            'wid_amount' => '0',
            'bonus_amount' => '0',
            'bank_bl'    => '0',
            'didwid_bl'  => '0',
            'diff'       => '0',
            'flag'       => 'N',
            'created_by' => $this->global['name'],
            'created_at' => date('Y-m-d H:i:s')
        );
        if ($this->BalanceModel->createbl($d)) {
            $this->load->model('AgentModel');
            $ag = $this->AgentModel->all_data($this->global['role']);
            foreach ($ag as $k => $v) {
                $ag_blist = array(
                    'cdate'      => $cdate,
                    'web'        => $v->web,
                    'type'       => 'A',
                    'name'       => $v->name,
                    'bankid'     => '',
                    'bankno'     => $v->name,
                    'bf_amount'  => $v->total_member_credit,
                    'af_amount'  => $v->total_member_credit,
                    'fee'        => '0',
                    'status'     => '1',
                    'created_by' => $this->global['name'],
                    'created_at' => date('Y-m-d H:i:s'),
                );

                $this->BalanceModel->create($ag_blist);
            }

            $this->load->model('BankModel');
            $bank  = $this->BankModel->all_data($this->global['role']);

            foreach ($bank as $k => $v) {
                $bank_blist = array(
                    'cdate'      => $cdate,
                    'web'        => $v->web,
                    'type'       => 'B',
                    'name'       => $v->bankname,
                    'bankid'     => $v->bankid,
                    'bankno'     => $v->bankno,
                    'bf_amount'  => $v->balance,
                    'af_amount'  => $v->balance,
                    'fee'        => '0',
                    'status'     => '1',
                    'created_by' => $this->global['name'],
                    'created_at' => date('Y-m-d H:i:s'),
                );

                $this->BalanceModel->create($bank_blist);
            }
            redirect('/balance');
        } else {
            echo 'create bl error !!';
        }
    }

    public function cal_period($cdate){

        $this->load->model('AgentModel');
        $ag = $this->AgentModel->all_data($this->global['role']);
        $bl = $this->BalanceModel->get_balance();
        $allag = array();
        if($cdate != $bl[0]->bdate){
            $didbank = $this->BalanceModel->getdidcnt_bybank($cdate);
            $widbank = $this->BalanceModel->getwidcnt_bybank($cdate);
            $didag   = $this->BalanceModel->getdidcnt_byagent($cdate);
            $widag   = $this->BalanceModel->getwidcnt_byagent($cdate);
        }else{
            foreach ($ag as $k => $v) {
                $ag_blist = array(
                    'cdate'      => $cdate,
                    'web'        => $v->web,
                    'type'       => 'A',
                    'name'       => $v->name,
                    'bankid'     => '',
                    'bankno'     => $v->name,
                    'bf_amount'  => $v->total_member_credit,
                    'af_amount'  => $v->total_member_credit,
                    'fee'        => '0',
                    'status'     => '1',
                    'created_by' => $this->global['name'],
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->BalanceModel->create($ag_blist);
                $allag[$v->name] = $v->name;
            }

            $this->load->model('BankModel');
            $bank  = $this->BankModel->all_data($this->global['role']);

            foreach ($bank as $k => $v) {
                $bank_blist = array(
                    'cdate'      => $cdate,
                    'web'        => $v->web,
                    'type'       => 'B',
                    'name'       => $v->bankname,
                    'bankid'     => $v->bankid,
                    'bankno'     => $v->bankno,
                    'bf_amount'  => $v->balance,
                    'af_amount'  => $v->balance,
                    'fee'        => '0',
                    'status'     => '1',
                    'created_by' => $this->global['name'],
                    'created_at' => date('Y-m-d H:i:s'),
                );

                $this->BalanceModel->create($bank_blist);
            }

            $didbank = $this->BalanceModel->getdidcnt_bybank();
            $widbank = $this->BalanceModel->getwidcnt_bybank();
            $didag   = $this->BalanceModel->getdidcnt_byagent();
            $widag   = $this->BalanceModel->getwidcnt_byagent();
        }

        // debug($didag,true);

        // $credit_change   = $this->BalanceModel->getag_changecredit();

        $did_cnt = $wid_cnt = $did_amt = $wid_amt = $did_bonus = 0;

        foreach ($didbank as $v) {
            $d = array(
                'did_cnt'   => $v->cnt,
                'did_amt'   => $v->amt,
            );
            $this->BalanceModel->updatelist($cdate,$v->uid,$d);
        }

        foreach ($widbank as $v) {
            $d = array(
                'wid_cnt' => $v->cnt,
                'wid_amt' => $v->amt,
            );
            $this->BalanceModel->updatelist($cdate,$v->uid,$d);
        }

        foreach ($didag as $v) {
            $did_cnt   += $v->cnt;
            $did_amt   += $v->amt;
            $did_bonus += $v->bonus;
            if($cdate != $bl[0]->bdate){
                $credit_change   = $this->BalanceModel->getag_changecredit_bydate($v->uid,$cdate);
            }else{
                $credit_change   = $this->BalanceModel->getag_changecredit($v->uid);
            }
            $d = array(
                'did_cnt'   => $v->cnt,
                'did_amt'   => $v->amt,
                'did_bonus' => $v->bonus,
                'credit_change' => ($credit_change)? $credit_change[0]->credit_change : '0.00',
            );

            $this->BalanceModel->updatelist($cdate,$v->uid,$d);

            $credit_change  = '';
            // unset($v->uid);
        }

        foreach ($widag as $v) {
            $wid_cnt += $v->cnt;
            $wid_amt += $v->amt;
            if($cdate != $bl[0]->bdate){
                $credit_change   = $this->BalanceModel->getag_changecredit_bydate($v->uid,$cdate);
            }else{
                $credit_change   = $this->BalanceModel->getag_changecredit($v->uid);
            }
            $d = array(
                'wid_cnt' => $v->cnt,
                'wid_amt' => $v->amt,
                'credit_change' => ($credit_change)? $credit_change[0]->credit_change : '0.00',
            );

            $this->BalanceModel->updatelist($cdate,$v->uid,$d);

            unset($v->uid);
        }

        foreach ($allag as $k) {
           
            if($cdate != $bl[0]->bdate){
                $credit_change   = $this->BalanceModel->getag_changecredit_bydate($k,$cdate);
            }else{
                $credit_change   = $this->BalanceModel->getag_changecredit($k);
            }
            $d = array(
                'credit_change' => ($credit_change)? $credit_change[0]->credit_change : '0.00',
            );

            $this->BalanceModel->updatelist($cdate,$k,$d);
            $credit_change  = '';
        }


        // debug($allag,true);

        // echo 'didbank--<br/>';
        // debug($didbank);
        // echo 'widbank--<br/>';
        // debug($widbank);
        // echo 'didag--<br/>';
        // debug($didag);
        // echo 'widag--<br/>';
        // debug($widag,true);
        if($cdate != $bl[0]->bdate){
            $blist = $this->BalanceModel->get_balancelist_old($cdate);
        }else{
            $blist = $this->BalanceModel->get_balancelist($cdate);
        }

        // debug($blist);
        $agent_bl = $bank_bl = 0;
        foreach ($blist as $v) {
            // if($v->type == 'A'){
            //     $agent_bl  += ($v->bf_amount - $v->af_amount);
            // }
            // echo 'af bank_bl -- '.$bank_bl.'  <br/>';
            if($v->type == 'B'){
                
                $bank_bl  += ($v->af_amount - $v->bf_amount);
                // echo 'B  '.$bank_bl.'<br/>';
            }
            if($v->type == 'O'){
                
                $bank_bl -= ($v->bf_amount);
                // echo 'O '.$bank_bl.'<br/>';
            }
            // echo 'bf bank_bl -- '.$bank_bl.'  <br/>';
        }
        // echo $bank_bl;
        $didwid_bl = ($did_amt - $wid_amt);
        $d = array(
            'did_cnt'      => $did_cnt,
            'did_amount'   => $did_amt,
            'wid_cnt'      => $wid_cnt,
            'wid_amount'   => $wid_amt,
            'bonus_amount' => $did_bonus,
            'bank_bl'      => $bank_bl,
            'didwid_bl'    => $didwid_bl,
            'diff'         => ($didwid_bl - $bank_bl),
            'updated_at'   => date('Y-m-d H:i:s'),
        );
        // debug($d,true);
        $this->BalanceModel->update_balance($cdate,$d);

        // $did_cnt[0]->did_cnt
        if($cdate != $bl[0]->bdate){
            redirect('/balance/period/'.$cdate);
        }else{
            redirect('/balance');
        }
    }

    public function close_period($date){

        $this->BalanceModel->closebl($date);
        $this->BalanceModel->closebl_list($date);
        $this->BalanceModel->close_transsec($date);

        $cdate = date('Y-m-d', strtotime($date . ' +1 day'));
        $d = array(
            'bdate'      => $cdate,
            'did_cnt'    => '0',
            'did_amount' => '0',
            'wid_cnt'    => '0',
            'wid_amount' => '0',
            'bonus_amount' => '0',
            'bank_bl'    => '0',
            'didwid_bl'  => '0',
            'diff'       => '0',
            'flag'       => 'N',
            'created_by' => $this->global['name'],
            'created_at' => date('Y-m-d H:i:s')
        );
        if ($this->BalanceModel->createbl($d)) {
            $this->load->model('AgentModel');
            $ag = $this->AgentModel->all_data($this->global['role']);
            foreach ($ag as $k => $v) {
                $ag_blist = array(
                    'cdate'      => $cdate,
                    'web'        => $v->web,
                    'type'       => 'A',
                    'name'       => $v->name,
                    'bankid'     => '',
                    'bankno'     => $v->name,
                    'bf_amount'  => $v->total_member_credit,
                    'af_amount'  => $v->total_member_credit,
                    'fee'        => '0',
                    'status'     => '1',
                    'created_by' => $this->global['name'],
                    'created_at' => date('Y-m-d H:i:s'),
                );

                $this->BalanceModel->create($ag_blist);
            }

            $this->load->model('BankModel');
            $bank  = $this->BankModel->all_data($this->global['role']);

            foreach ($bank as $k => $v) {
                $bank_blist = array(
                    'cdate'      => $cdate,
                    'web'        => $v->web,
                    'type'       => 'B',
                    'name'       => $v->bankname,
                    'bankid'     => $v->bankid,
                    'bankno'     => $v->bankno,
                    'bf_amount'  => $v->balance,
                    'af_amount'  => $v->balance,
                    'fee'        => '0',
                    'status'     => '1',
                    'created_by' => $this->global['name'],
                    'created_at' => date('Y-m-d H:i:s'),
                );

                $this->BalanceModel->create($bank_blist);
            }
            $this->BalanceModel->clear_api_didwid($date);
            redirect('/balance');
        } else {
            echo 'create bl error !!';
        }

        // $this->load->model('AgentModel');
        // $ag = $this->AgentModel->all_data($this->global['role']);
        // foreach ($ag as $k => $v) {
        //     $ag_blist = array(
        //         'cdate'      => $cdate,
        //         'web'        => $v->web,
        //         'type'       => 'A',
        //         'name'       => $v->name,
        //         'bankid'     => '',
        //         'bankno'     => $v->name,
        //         'bf_amount'  => $v->total_member_credit,
        //         'af_amount'  => $v->total_member_credit,
        //         'fee'        => '0',
        //         'status'     => '1',
        //         'created_by' => $this->global['name'],
        //         'created_at' => date('Y-m-d H:i:s'),
        //     );

        //     $this->BalanceModel->create($ag_blist);
        // }

        // $this->load->model('BankModel');
        // $bank  = $this->BankModel->all_data($this->global['role']);

        // foreach ($bank as $k => $v) {
        //     $bank_blist = array(
        //         'cdate'      => $cdate,
        //         'web'        => $v->web,
        //         'type'       => 'B',
        //         'name'       => $v->bankname,
        //         'bankid'     => $v->bankid,
        //         'bankno'     => $v->bankno,
        //         'bf_amount'  => $v->balance,
        //         'af_amount'  => $v->balance,
        //         'fee'        => '0',
        //         'status'     => '1',
        //         'created_by' => $this->global['name'],
        //         'created_at' => date('Y-m-d H:i:s'),
        //     );

        //     $this->BalanceModel->create($bank_blist);
        // }
        // redirect('/balance');
    }

    public function get_cal_period(){
        $res = array(
            'status' => false,
            'msg'    => '',
        );
        // $get = $this->input->get();
        $this->load->model('BalanceModel');
        $blist = $this->BalanceModel->get_balance();
        if($blist){
            $cdate = $blist[0]->bdate;
            $this->load->model('AgentModel');
            $ag = $this->AgentModel->all_data($this->global['role']);

            foreach ($ag as $k => $v) {
                $ag_blist = array(
                    'cdate'      => $cdate,
                    'web'        => $v->web,
                    'type'       => 'A',
                    'name'       => $v->name,
                    'bankid'     => '',
                    'bankno'     => $v->name,
                    'bf_amount'  => $v->total_member_credit,
                    'af_amount'  => $v->total_member_credit,
                    'fee'        => '0',
                    'status'     => '1',
                    'created_by' => $this->global['name'],
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->BalanceModel->create($ag_blist);
            }

            $this->load->model('BankModel');
            $bank  = $this->BankModel->all_data($this->global['role']);

            foreach ($bank as $k => $v) {
                $bank_blist = array(
                    'cdate'      => $cdate,
                    'web'        => $v->web,
                    'type'       => 'B',
                    'name'       => $v->bankname,
                    'bankid'     => $v->bankid,
                    'bankno'     => $v->bankno,
                    'bf_amount'  => $v->balance,
                    'af_amount'  => $v->balance,
                    'fee'        => '0',
                    'status'     => '1',
                    'created_by' => $this->global['name'],
                    'created_at' => date('Y-m-d H:i:s'),
                );

                $this->BalanceModel->create($bank_blist);
            }

            $didbank = $this->BalanceModel->getdidcnt_bybank($cdate);
            $widbank = $this->BalanceModel->getwidcnt_bybank($cdate);
            $didag   = $this->BalanceModel->getdidcnt_byagent($cdate);
            $widag   = $this->BalanceModel->getwidcnt_byagent($cdate);
            // $credit_change   = $this->BalanceModel->getag_changecredit();

            $did_cnt = $wid_cnt = $did_amt = $wid_amt = $did_bonus = 0;

            foreach ($didbank as $v) {
                $d = array(
                    'did_cnt'   => $v->cnt,
                    'did_amt'   => $v->amt,
                );
                $this->BalanceModel->updatelist($cdate,$v->uid,$d);
            }

            foreach ($widbank as $v) {
                $d = array(
                    'wid_cnt' => $v->cnt,
                    'wid_amt' => $v->amt,
                );
                $this->BalanceModel->updatelist($cdate,$v->uid,$d);
            }

            foreach ($didag as $v) {
                $did_cnt   += $v->cnt;
                $did_amt   += $v->amt;
                $did_bonus += $v->bonus;
                $credit_change   = $this->BalanceModel->getag_changecredit($v->uid);
                $d = array(
                    'did_cnt'   => $v->cnt,
                    'did_amt'   => $v->amt,
                    'did_bonus' => $v->bonus,
                    'credit_change' => ($credit_change)? $credit_change[0]->credit_change : '0.00',
                );
                $this->BalanceModel->updatelist($cdate,$v->uid,$d);
            }

            foreach ($widag as $v) {
                $wid_cnt += $v->cnt;
                $wid_amt += $v->amt;
                $d = array(
                    'wid_cnt' => $v->cnt,
                    'wid_amt' => $v->amt,
                );
                $this->BalanceModel->updatelist($cdate,$v->uid,$d);
            }

            $blist = $this->BalanceModel->get_balancelist($cdate);
            $agent_bl = $bank_bl = 0;
            foreach ($blist as $v) {
                // if($v->type == 'A'){
                //     $agent_bl  += ($v->bf_amount - $v->af_amount);
                // }
                if($v->type == 'B'){
                    $bank_bl  += ($v->af_amount - $v->bf_amount);
                }
                if($v->type == 'O'){
                    $bank_bl -= $v->bf_amount;
                }
            }
            $didwid_bl = ($did_amt - $wid_amt);
            $d = array(
                'did_cnt'      => $did_cnt,
                'did_amount'   => $did_amt,
                'wid_cnt'      => $wid_cnt,
                'wid_amount'   => $wid_amt,
                'bonus_amount' => $did_bonus,
                'bank_bl'      => $bank_bl,
                'didwid_bl'    => $didwid_bl,
                'diff'         => ($didwid_bl - $bank_bl),
                'updated_at'   => date('Y-m-d H:i:s'),
            );
            // debug($d,true);
            $this->BalanceModel->update_balance($cdate,$d);
            $res['msg'] = 'ok';
            $res['status'] = true;
        }
        echo json_encode($res);
    }

    public function period($date) {
        $get = $this->input->get();
        if(!isset($get['company'])){
            echo 'error';exit();
        }else if($get['company'] == ''){
            echo 'error';exit();
        }
        $blist = $this->BalanceModel->get_balance_bydate($date,$get['company']);
        $this->global['balancelist'] = $blist;
       
        $this->global['bldate']      = $date;
        if($blist[0]->flag == 'Y'){
            $this->global['otherlist']   = $this->BalanceModel->get_otherlist_ye($date,$get['company']);
            $this->global['banklist']    = $this->BalanceModel->get_banklist_ye($date,$get['company']);
            $this->global['flag']        = $blist[0]->flag;
            $ag = $this->BalanceModel->get_agentlist_ye($date,$get['company']);
        }else{
            $this->global['otherlist']   = $this->BalanceModel->get_otherlist($date,$get['company']);
            $this->global['banklist']    = $this->BalanceModel->get_banklist($date,$get['company']);
            $this->global['flag']        = $blist[0]->flag;
            $ag = $this->BalanceModel->get_agentlist($date,$get['company']);
        }
        // debug($ag,true);
        $aglist = array();
        foreach ($ag as $k => $v) {
            if($v->web == 'IMI'){
                $v->af_amount = $v->credit_change;
            }
            $aglist[$v->web][] = $v;
        }
        $this->global['agentlist']   = $aglist;
        $this->load->model('BankModel');
        $this->global['bankidlist']  = $this->BankModel->getbanklist($this->global['role'],$get['company']);
        $content['pagetitle']        = 'ปิดยอด';
        $content['content']          = $this->load->view('balance/index', $this->global, true);
        $this->load->view('layout/admin', $content);
    }


    public function loadother(){
        $res = array(
            'status' => false,
            'data'   => array(),
            'msg'    => '',
        );
        $post = $this->input->post();
        if(isset($post['date'])){
            $d = $this->BalanceModel->get_otherlist($post['date']);
            if($d){
                $res['status'] = true;
                foreach ($d as $k => $v) {
                    $xp = explode('-', $v->bankno);
                    foreach ($v as $kk => $vv) {
                        $res['data'][$k][$kk] = $vv;
                    }
                    $res['data'][$k]['bankno'] = $xp[0];
                }
            }else{
                $res['msg'] = 'no data';
            }
        }else{
            $res['msg'] = 'no param date';
        }
        echo json_encode($res);
    }

    public function delete($id) {
        $res = array(
            'status' => false,
            'data'   => '',
            'msg'    => '',
        );
        if ($id != '') {
            if ($this->BalanceModel->del_data($id)) {
                $res['status'] = true;
            } else {
               $res['msg'] = 'delete error';
            }
        } else {
            $res['msg'] = 'id is null';
        }
        echo json_encode($res);
    }
}

?>