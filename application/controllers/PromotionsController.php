<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : DevDevel
 * @version : 1.1
 */
class PromotionsController extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();   
        $this->website             = $this->config->config['website'];
        $this->global['pageTitle'] = $this->website.' : '.strtoupper($this->uri->segment(1));
    }
    
    /**
     * This function used to load the first screen of the user
     */

    public function index() {
        $content['pagetitle'] = $this->global['pageTitle'];
        $this->global['status'] = $this->config->config['status'];
        $content['content']   = $this->load->view($this->uri->segment(1).'/main', $this->global, true);
        $this->load->view('layout/admin', $content);
    }

    public function search(){
        $res = array(
            'status' => true,
            'data'   => '',
            'msg'    => '',
        );
        $post = $this->input->post();

        $this->load->model('PromotionsModel');
        $this->global['data'] = $this->PromotionsModel->search_data($post,$post['qpage'],$post['qper_page']);
        $this->global['status'] = $this->config->config['status'];
        $view = $this->load->view($this->uri->segment(1).'/list',$this->global,true);
        $res['data']   = str_replace('   ', '', $view);
        echo json_encode($res);
    }

    public function add() {
        $view = $this->load->view($this->uri->segment(1).'/form',$this->global,true);
        $res['temp']  = str_replace('   ', '', $view);
        $this->output->set_output(json_encode($res));
    }

    public function edit($id) {
        if ($id != '') {
            $this->load->model('PromotionsModel');
            $this->global['data'] = $this->PromotionsModel->get_data_byid($id);
            $view = $this->load->view($this->uri->segment(1).'/form',$this->global,true);
            $res['temp']  = str_replace('   ', '', $view);
        }

        $this->output->set_output(json_encode($res));
    }

    public function create() {
        $post = $this->input->post();
        $res = array(
            'status' => false,
            'msg'    => '',
        );
        // debug($post,true);
        if ($post['title'] != '' && $post['desc'] != '') {

            $data = array(
                'title'      => $post['title'],
                'desc'       => base64_encode($post['desc']),
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $this->global['name'],
                'updated_by' => $this->global['name'],
            );

            $config['upload_path'] = './assets/uploads/';
            $config['allowed_types'] = 'jpg|png';
            $this->load->library('upload', $config);

            if(isset($_FILES['file'])){
                if($_FILES['file']['name'] != ''){

                    if (!$this->upload->do_upload('file')) {
                        $res['msg'] = $this->upload->display_errors();
                    }else{
                        $img = $this->upload->data();
                        $data['img_url'] = $img['file_name'];
                    } 
                }
            }else{
                $data['img_url'] = '';
            }
            $this->load->model('PromotionsModel');
            $ins_id = $this->PromotionsModel->create($data);
            if ($ins_id) {
                $res['status'] = true;
                $res['msg'] = 'success';
            } else {
                $res['msg'] = 'error !!!';
            }
        } else {

            $res['msg'] = 'title & desc is null !!!';

        }

        echo json_encode($res);
    }

    public function update($id) {
        $post = $this->input->post();

        // debug($post,true);

        if ($post['title'] != '' && $post['desc'] != '') {
            $data = array(
                'title'      => $post['title'],
                'status'     => $post['status'],
                'desc'       => base64_encode($post['desc']),
                'updated_by' => $this->global['name']
            );
            if(isset($_FILES['file'])){
                if($_FILES['file']['name'] != ''){
                    $config['upload_path'] = './assets/uploads/';
                    $config['allowed_types'] = 'jpg|png';
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('file')) {
                        $res['msg'] = $this->upload->display_errors();
                    }else{
                        $img = $this->upload->data();
                        $data['img_url'] = $img['file_name'];
                    } 
                }
            }
            $this->load->model('PromotionsModel');
            if ($this->PromotionsModel->update($id, $data)) {
                $res['status'] = true;
                $res['msg'] = 'success';
            } else {
                $res['msg'] = 'error !!!';
            }
        } else {
            $res['msg'] = 'title & desc is null !!!';
        }
        echo json_encode($res);
    }


    public function delete($id) {
        if ($id != '') {
            $this->load->model('PromotionsModel');
            $data = array(
                'status' => '0'
            );
            if ($this->PromotionsModel->update($id,$data)) {
                $this->session->set_flashdata('success', 'delete data success');
            } else {
                $this->session->set_flashdata('error', 'error on delete data');
            }
            redirect('/'.$this->uri->segment(1));
        } else {
            redirect('/'.$this->uri->segment(1));
        }
    }
}

?>