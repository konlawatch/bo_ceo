<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class BalancelistController extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();   
        $this->website = $this->config->config['website'];
        $this->load->model('BalancelistModel');
        $this->global['pageTitle'] = 'Admin '.$this->website.' : ลูกค้า';
    }
    
    /**
     * This function used to load the first screen of the user
     */

    public function index() {

        // $this->load->library('pagination');
        // $per_page = 50;
        // $page = ($this->uri->segment(2) != '') ? $this->uri->segment(2) : 0; // เลขหน้าที่จะถูกส่งมาเช่น home/member/3
        // $config['base_url'] = base_url() . 'balancelist/page'; // ชี้หน้าเพจหลักที่จะใช้งานมาที่ home/member
        // $config['total_rows'] = $this->BalancelistModel->all_data_cnt($this->global['role']); // จำนวนข้อมูลทั้งหมด
        // $config['per_page'] = $per_page; // จำนวนข้อมูลต่อหน้า

        // $config['use_page_numbers'] = TRUE; // เพื่อให้เลขหน้าในลิงค์ถูกต้อง ให้เซตค่าส่วนนี้เป็น TRUE
        // $config['full_tag_open'] = '<ul class="pagination" style="margin:10px 0;">';
        // $config['full_tag_close'] = '</ul>';
        // $config['first_link'] = false;
        // $config['last_link'] = false;
        // $config['num_links'] = 10;
        // $config['first_tag_open'] = '<li>';
        // $config['first_tag_close'] = '</li>';
        // $config['prev_link'] = 'Previous';
        // $config['prev_tag_open'] = '<li class="prev">';
        // $config['prev_tag_close'] = '</li>';
        // $config['next_link'] = 'Next';
        // $config['next_tag_open'] = '<li>';
        // $config['next_tag_close'] = '</li>';
        // $config['last_tag_open'] = '<li>';
        // $config['last_tag_close'] = '</li>';
        // $config['cur_tag_open'] = '<li class="active"><a href="#" >';
        // $config['cur_tag_close'] = '</a></li>';
        // $config['num_tag_open'] = '<li>';
        // $config['num_tag_close'] = '</li>';

        // $this->pagination->initialize($config);
        // $this->global['pagination'] = $this->pagination->create_links($this->global['role']); // เลขหน้า
        // $this->global['data'] = $this->BalancelistModel->search_data('',$this->global['role'],0,100);
        
        $get = $this->input->get();
        if(isset($get['company'])){
            // $this->global['data'] = array();
            $get['bdate'] = '';
            $this->global['data'] = $this->BalancelistModel->search_data($get,$this->global['role'],0,10);
            $this->global['company'] = $get['company'];
            $content['pagetitle'] = 'รายงานแพ้ชนะ '.$get['company'];
            $content['content'] = $this->load->view('balancelist/index', $this->global, true);
        }else{
            $content['pagetitle'] = 'รายงานแพ้ชนะ';
            $content['content'] = '';
        }
        $this->load->view('layout/admin', $content);
    }

    public function page() {
        $per_page = 50;
        $page = ($this->uri->segment(3) != '') ? (($this->uri->segment(3) - 1) * $per_page) : 0; // เลขหน้าที่จะถูกส่งมาเช่น home/member/3
        $config['base_url'] = base_url() . 'balancelist/page'; // ชี้หน้าเพจหลักที่จะใช้งานมาที่ home/member
        $config['total_rows'] = $this->BalancelistModel->all_data_cnt($this->global['role']); // จำนวนข้อมูลทั้งหมด
        $config['per_page'] = $per_page; // จำนวนข้อมูลต่อหน้า

        $config['use_page_numbers'] = TRUE; // เพื่อให้เลขหน้าในลิงค์ถูกต้อง ให้เซตค่าส่วนนี้เป็น TRUE

        $config['full_tag_open'] = '<ul class="pagination" style="margin:10px 0;">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['num_links'] = 10;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#" >';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $this->global['pagination'] = $this->pagination->create_links(); // เลขหน้า
        $this->global['data'] = $this->BalancelistModel->data_page($page, $per_page,$this->global['role']);
        $content['pagetitle'] = 'ทะเบียนข้อมูลลูกค้า';
        $content['content'] = $this->load->view('balancelist/index', $this->global, true);
        $this->load->view('layout/admin', $content);
    }

    public function search(){
        $res = array(
            'status' => false,
            'data'   => '',
            'msg'    => '',
            'sm'     => '',
            'em'     => '',
        );
        $post = $this->input->post();

         if(isset($post['bdate']) && isset($post['edate']) && isset($post['company']) && isset($post['type'])){
            // $d = $this->BalancelistModel->search_data($post,$this->global['role'],$post['page'],$post['per_page']);
            if($post['type'] != ''){
                switch ($post['type']) {
                    // case 'today':
                    //     $post['bdate'] = date('d-m-Y');
                    //     $post['edate'] = date('d-m-Y');
                    // break;
                    // case '7days':
                    //     $post['bdate'] = date('d-m-Y', strtotime(date('Y-m-d') . ' -7 day'));
                    //     $post['edate'] = date('d-m-Y');
                    // break;
                    // case '30days':
                    //     $post['bdate'] = date('d-m-Y', strtotime(date('Y-m-d') . ' -30 day'));
                    //     $post['edate'] = date('d-m-Y');
                    // break;
                    case 'month':
                        if($post['month'] == ''){
                            $post['month'] = date('n');
                        }
                        $m = explode(',',$post['month']);
                        if($m[0] < 10){
                            $post['bdate'] = date('01-0'.$m[0].'-'.$post['year']);
                        }else{
                            $post['bdate'] = date('01-'.$m[0].'-'.$post['year']);
                        }
                        $post['edate'] = date('t-m-Y', strtotime(date('d-'.$m[count($m) - 1].'-'.$post['year'])));

                        $res['sm']       = $m[0];
                        $res['em']       = $m[count($m) - 1];
                        $this->global['month']  = $m;
                        $this->global['data']   = $this->BalancelistModel->search_data($post,$this->global['role'],$post['page'],$post['per_page']);
                        $this->global['company'] = $post['company'];
                        $res['data']    = $this->load->view('balancelist/mlists',$this->global,true);
                    break;

                    case 'day':
                        if($post['bdate'] == '' || $post['edate'] == ''){
                            $post['bdate'] = date('d-m-'.$post['year']);
                            $post['edate'] = date('d-m-'.$post['year']);
                        }

                        // if($post['month'] == ''){
                        //     $post['month'] = date('n');
                        // }
                        // $m = explode(',',$post['month']);
                        // if($m[0] < 10){
                        //     $post['bdate'] = date('01-0'.$m[0].'-'.$post['year']);
                        // }else{
                        //     $post['bdate'] = date('01-'.$m[0].'-'.$post['year']);
                        // }
                        // $post['edate'] = date('t-m-Y', strtotime(date('d-'.$m[count($m) - 1].'-'.$post['year'])));

                        $this->global['data']   = $this->BalancelistModel->search_data($post,$this->global['role'],$post['page'],$post['per_page']);
                        $this->global['company'] = $post['company'];
                        $res['data']    = $this->load->view('balancelist/lists',$this->global,true);
                    break;
                }
            }
            // debug($post,true);
            $res['status']  = true;
            $res['q']       = $post;
            // if($d){
            //     $res['status'] = true;
            //     $res['data']   = $d;
            // }else{
            //     $res['msg'] = 'no data';
            // }
        }else{
            $res['msg'] = 'no param';
        }
        echo json_encode($res);
    }

    public function add() {
        $this->global['banklist'] = $this->config->config['banklist'];
        $this->global['sitelist'] = $this->config->config['sitelist'];
        $content['pagetitle'] = 'เพิ่มข้อมูลลูกค้า';
        $content['content'] = $this->load->view('balancelist/form', $this->global, true);
        $this->load->view('layout/admin', $content);
    }

    public function edit($id) {
        $this->global['banklist'] = $this->config->config['banklist'];
        $this->global['sitelist'] = $this->config->config['sitelist'];
        if ($id != '') {
            $this->global['data'] = $this->BalancelistModel->get_data_byid($id);
             $content['pagetitle'] = 'แก้ไขข้อมูลลูกค้า';
            $content['content'] = $this->load->view('balancelist/form', $this->global, true);
            $this->load->view('layout/admin', $content);
        } else {
            redirect('/balancelist');
        }
    }

    public function create() {
        $post = $this->input->post();
        // debug($post,true);
        if ($post['name'] != '') {
            $post['bankname']   = $this->config->config['banklist'][$post['bankid']];
            $post['webname']    = $this->config->config['sitelist'][$post['webid']];
            $post['created_by'] = $this->global['name'];
            $post['company']    = $this->global['role'];
            if ($this->BalancelistModel->create($post)) {

                $this->session->set_flashdata('success', 'success on save data');

                redirect('/balancelist');

            } else {

                $this->session->set_flashdata('error', 'erro on save data');

                redirect('/balancelist/add');
            }
        } else {

            $this->session->set_flashdata('error', 'title is not null.');

            redirect('/balancelist/add');

        }
    }

    public function update($id) {
        $post = $this->input->post();

        // debug($post,true);

        if ($post['name'] != '') {
            $post['updated_by'] = $this->global['name'];
            $olddata = json_decode($post['oldval'],true);
            // debug($olddata,true);
            foreach ($post as $k => $v) {
                if($k != 'oldval'  && $k != 'company'){
                    if($olddata[$k] != $post[$k]){
                        $log = array(
                            'did'        => $id,
                            'col'        => $k,
                            'newval'     => $post[$k],
                            'oldval'     => $olddata[$k],
                        );
                        $this->BalancelistModel->inslogs($log);
                    }
                }
            }

            if ($this->BalancelistModel->update($id, $post)) {

                $this->session->set_flashdata('success', 'update data success');

                redirect('/balancelist');

            } else {

                $this->session->set_flashdata('error', 'erro on update data');

                redirect('/balancelist/edit/' . $id);

            }
        } else {
            $this->session->set_flashdata('error', 'title is not null.');

            redirect('/balancelist/edit/' . $id);
        }
    }

    public function delete($id) {
        if ($id != '') {
            if ($this->BalancelistModel->del_data($id)) {
                $this->session->set_flashdata('success', 'delete data success');
            } else {
                $this->session->set_flashdata('error', 'error on delete data');
            }
            redirect('/balancelist');
        } else {
            redirect('/balancelist');
        }
    }
}

?>