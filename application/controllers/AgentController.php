<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class AgentController extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();   
        $this->website = $this->config->config['website'];
        $this->load->model('AgentModel');
        $this->global['pageTitle'] = 'Admin '.$this->website.' : ตั้งค่ารหัสเอเย่นต์';
    }
    
    /**
     * This function used to load the first screen of the user
     */

    public function index() {

        $allow = array(
            '1','2'
        );
        if(!in_array($this->global['role'], $allow)){
            echo 'denide !!!';exit();
        }

        // $this->load->library('pagination');
        $this->global['sitelist']  = $this->config->config['sitelist'];
        $content['pagetitle'] = 'ตั้งค่ารหัสเอเย่นต์';
        $content['content'] = $this->load->view('agent/main', $this->global, true);
        $this->load->view('layout/admin', $content);
    }

    public function page() {

        $allow = array(
            '1','2'
        );
        if(!in_array($this->global['role'], $allow)){
            echo 'denide !!!';exit();
        }

        $per_page = 50;
        $page = ($this->uri->segment(3) != '') ? (($this->uri->segment(3) - 1) * $per_page) : 0; // เลขหน้าที่จะถูกส่งมาเช่น home/member/3
        $config['base_url'] = base_url() . 'agent/page'; // ชี้หน้าเพจหลักที่จะใช้งานมาที่ home/member
        $config['total_rows'] = $this->AgentModel->all_data_cnt($this->global['role']); // จำนวนข้อมูลทั้งหมด
        $config['per_page'] = $per_page; // จำนวนข้อมูลต่อหน้า

        $config['use_page_numbers'] = TRUE; // เพื่อให้เลขหน้าในลิงค์ถูกต้อง ให้เซตค่าส่วนนี้เป็น TRUE

        $config['full_tag_open'] = '<ul class="pagination" style="margin:10px 0;">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['num_links'] = 10;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#" >';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $this->global['pagination'] = $this->pagination->create_links(); // เลขหน้า
        $this->global['data'] = $this->AgentModel->data_page($page, $per_page,$this->global['role']);
        $content['pagetitle'] = 'ตั้งค่ารหัสเอเย่นต์';
        $content['content'] = $this->load->view('agent/index', $this->global, true);
        $this->load->view('layout/admin', $content);
    }

    public function search(){

        $allow = array(
            '1','2'
        );
        if(!in_array($this->global['role'], $allow)){
            echo 'denide !!!';exit();
        }

        $res = array(
            'status' => false,
            'data'   => '',
            'msg'    => '',
        );
        $post = $this->input->post();

         if(isset($post['web']) && isset($post['agent'])){
            $this->global['botstatus'] = $this->config->config['botstatus'];
            $this->global['data'] = $this->AgentModel->search_data($post,$this->global['role'],$post['page'],$post['per_page']);
            $res['data']  = $this->load->view('agent/list',$this->global,true);
            $res['status'] = true;
        }else{
            $res['msg'] = 'no param';
        }
        echo json_encode($res);
    }


    public function get_agent_byid(){
        $res = array(
            'status' => false,
            'data'   => '',
            'msg'    => '',
        );
        $get = $this->input->get();

        if(isset($get['cid'])){
            $d = $this->AgentModel->get_data_byid($get['cid']);
            if($d){
                $res['status'] = true;
                $res['data']   = $d;
            }else{
                $res['msg'] = 'no data';
            }
        }else{
            $res['msg'] = 'no param cid';
        }
        echo json_encode($res);
    }

    public function get_agent_byweb(){
        $res = array(
            'status' => false,
            'data'   => '',
            'msg'    => '',
        );
        $get = $this->input->get();

        if(isset($get['web']) && isset($get['company'])){
            $d = $this->AgentModel->get_data_byweb($get['web'],$get['company']);
            if($d){
                $res['status'] = true;
                $res['data']   = $d;
            }else{
                $res['msg'] = 'no data';
            }
        }else{
            $res['msg'] = 'no param web';
        }
        echo json_encode($res);
    }


    public function add() {
        $allow = array(
            '1','2'
        );
        if(!in_array($this->global['role'], $allow)){
            echo 'denide !!!';exit();
        }

        $this->global['banklist'] = $this->config->config['bankshortconf'];
        $this->global['sitelist'] = $this->config->config['sitelist'];
        $content['pagetitle'] = 'เพิ่มรหัสเอเย่นต​์';
        $content['content'] = $this->load->view('agent/form', $this->global, true);
        $this->load->view('layout/admin', $content);
    }

    public function edit($id) {
        $allow = array(
            '1','2'
        );
        if(!in_array($this->global['role'], $allow)){
            echo 'denide !!!';exit();
        }

        $this->global['banklist'] = $this->config->config['bankshortconf'];
        $this->global['sitelist'] = $this->config->config['sitelist'];
        if ($id != '') {
            $this->global['data'] = $this->AgentModel->get_data_byid($id);
            $content['pagetitle'] = 'แก้ไขรหัสเอเย่นต์';
            $content['content']   = $this->load->view('agent/form', $this->global, true);
            $this->load->view('layout/admin', $content);
        } else {
            redirect('/agent');
        }
    }

    public function create() {
        $allow = array(
            '1','2'
        );
        if(!in_array($this->global['role'], $allow)){
            echo 'denide !!!';exit();
        }

        $post = $this->input->post();
        // debug($post,true);
        if ($post['name'] != '') {
            $post['created_by'] = $this->global['name'];
            $post['company']    = $this->global['role'];
            if ($this->AgentModel->create($post)) {

                $this->session->set_flashdata('success', 'success on save data');

                redirect('/agent');

            } else {

                $this->session->set_flashdata('error', 'erro on save data');

                redirect('/agent/add');
            }
        } else {

            $this->session->set_flashdata('error', 'title is not null.');

            redirect('/agent/add');

        }
    }

     public function fm_create() {
        $allow = array(
            '1','2'
        );
        if(!in_array($this->global['role'], $allow)){
            echo 'denide !!!';exit();
        }

        $post = $this->input->post();
        $res = array(
            'status' => false,
            'msg'    => '',
        );
        debug($post,true);
        if ($post['name'] != '') {
            $post['created_by'] = $this->global['name'];
            $post['company']    = $this->global['role'];

            if ($this->AgentModel->create($post)) {
                $res['status'] = true;
                $res['msg'] = 'success on save data';
            } else {
                $res['msg'] = 'success on save data';
            }
        } else {
            $res['msg'] = 'กรุณาป้อนข้อมูลให้ครบตามเครื่องหมาย *.';

        }
        echo json_encode($res);
    }

    public function update($id) {
        $allow = array(
            '1','2'
        );
        if(!in_array($this->global['role'], $allow)){
            echo 'denide !!!';exit();
        }

        $post = $this->input->post();

        // debug($post,true);

        if ($post['name'] != '') {
            $post['updated_by'] = $this->global['name'];
            $olddata = json_decode($post['oldval'],true);
            // debug($olddata,true);
            foreach ($post as $k => $v) {
                if($k != 'oldval'  && $k != 'company'){
                    if($olddata[$k] != $post[$k]){
                        $log = array(
                            'did'        => $id,
                            'col'        => $k,
                            'newval'     => $post[$k],
                            'oldval'     => $olddata[$k],
                        );
                        $this->AgentModel->inslogs($log);
                    }
                }
            }

            if ($this->AgentModel->update($id, $post)) {

                $this->session->set_flashdata('success', 'update data success');

                redirect('/agent');

            } else {

                $this->session->set_flashdata('error', 'erro on update data');

                redirect('/agent/edit/' . $id);

            }
        } else {
            $this->session->set_flashdata('error', 'title is not null.');

            redirect('/agent/edit/' . $id);
        }
    }

    public function delete($id) {
        $allow = array(
            '1','2'
        );
        if(!in_array($this->global['role'], $allow)){
            echo 'denide !!!';exit();
        }

        if ($id != '') {
            if ($this->AgentModel->del_data($id)) {
                $this->session->set_flashdata('success', 'delete data success');
            } else {
                $this->session->set_flashdata('error', 'error on delete data');
            }
            redirect('/agent');
        } else {
            redirect('/agent');
        }
    }

    public function update_run_status() {
        $allow = array(
            '1','2'
        );
        if(!in_array($this->global['role'], $allow)){
            echo 'denide !!!';exit();
        }
        
        $res = array(
            'status' => false,
            'data'   => '',
            'msg'    => '',
        );
        $get = $this->input->get();

        if(isset($get['id'])){
            if($get['status'] == '1'){
                $type = 'start';
            }else{
                $type = 'stop';
            }
            $ex   = explode('_', $get['sig']);
            // $exx  = explode('-', $ex[2]);
            // $url  = 'https://pm2.botbo21.com/ag.php?user='.$ex[0].'&pass='.$ex[1].'&type='.$type.'&agent='.$ex[2];
            $pm   = 'user='.$ex[0].'&pass='.$ex[1].'&agent='.$ex[2].'&type='.$type.'&key='.$this->config->config['key'].'&v='.(mt_rand( 0, 100 ) / 10);
            $url  = 'https://'.$this->config->config['key'].'route.botbo21.com';
            // $url  = 'http://localhost:3001';
            // debug($ex,true);
            $url = 'https://svr4.botbo21.com';
            if($get['web'] == 'TS911'){
                $curl = cUrl($url.'/login_ts','get',$pm);
            }else if($get['web'] == 'UFA'){
                $url = 'https://svr5.botbo21.com';
                $curl = cUrl($url.'/login_ufa','get',$pm);
            }else if($get['web'] == 'LSM'){
                $url = 'https://svr5.botbo21.com';
                $curl = cUrl($url.'/login_lsm','get',$pm);
            }else if($get['web'] == 'IMI'){
                $url = 'https://svr5.botbo21.com';
                $curl = cUrl($url.'/login_imi','get',$pm);
            }else{
                $res['msg'] = 'error';
                echo json_encode($res);
                exit();
            }
            // echo $url;exit();
            // $curl = cUrl($url, $method = "get");
            $r    = json_decode($curl,true);
            if($r['status']){
                if($this->AgentModel->update_run_status($get['id'],$get['status'],$this->global['name'])){
                    $res['status'] = true;
                    $res['msg'] = $this->config->config['botstatus'][$get['status']];
                }else{
                    $res['msg'] = 'error';
                }
            }else{
               $res['msg'] = 'bot error'; 
               $res['d'] = $r; 
            }
        }else{
            $res['msg'] = 'no param id';
        }
        echo json_encode($res);
    }
}

?>