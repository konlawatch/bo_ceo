<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Config for the CodeIgniter Redis library
 *
 * @see ../libraries/Redis.php
 */

$config['redis_default']['host'] = '127.0.0.1';		// IP address or host
$config['redis_default']['port'] = '6379';			// Default Redis port is 6379
$config['redis_default']['password'] = '';			// Can be left empty when the server does not require AUTH

// $config['redis_slave']['host'] = '10.10.3.15';
// $config['redis_slave']['host'] = '35.240.188.34';
// $config['redis_slave']['host'] = '148.72.207.22';
// $config['redis_slave']['host'] = '119.59.103.191';
$config['redis_slave']['host'] = '119.8.171.69';
// $config['redis_slave']['host'] = '159.138.109.134';
// $config['redis_slave']['host'] = '35.185.187.198';
$config['redis_slave']['port'] = '6379';
$config['redis_slave']['password'] = '';