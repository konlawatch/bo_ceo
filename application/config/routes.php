<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller']  = 'AdminController/index';

$route['index']               = 'HomeController/index';
$route['login']               = 'LoginController/loginMe';

$route['acclist']                = 'AccListController/index';
$route['acclist/(:any)']         = 'AccListController/$1';
$route['acclist/(:any)/(:any)']  = 'AccListController/$1/$2';

$route['logout']              = 'AdminController/logout';
$route['loadChangePass']      = "AdminController/loadChangePass";
$route['changePassword']      = "AdminController/changePassword";

$route['dashboard'] 	      = 'AdminController/index';
$route['admin/(:any)'] 	      = 'AdminController/$1';

$route['company']            = 'CompanyController/index';
$route['company/(:any)']     = 'CompanyController/$1';
$route['company/(:any)/(:any)'] = 'CompanyController/$1/$2';

$route['department']            = 'DepartmentController/index';
$route['department/(:any)']     = 'DepartmentController/$1';
$route['department/(:any)/(:any)'] = 'DepartmentController/$1/$2';

$route['positions']            = 'PositionsController/index';
$route['positions/(:any)']     = 'PositionsController/$1';
$route['positions/(:any)/(:any)'] = 'PositionsController/$1/$2';
 
$route['employees']            = 'EmployeesController/index';
$route['employees/(:any)']     = 'EmployeesController/$1';
$route['employees/(:any)/(:any)'] = 'EmployeesController/$1/$2';

$route['incometype']            = 'IncometypeController/index';
$route['incometype/(:any)']     = 'IncometypeController/$1';
$route['incometype/(:any)/(:any)'] = 'IncometypeController/$1/$2';

$route['calsalary']            = 'CalsalaryController/index';
$route['calsalary/(:any)']     = 'CalsalaryController/$1';
$route['calsalary/(:any)/(:any)'] = 'CalsalaryController/$1/$2';

$route['promotions']          = 'PromotionsController/index';
$route['promotions/(:any)']   = 'PromotionsController/$1';
$route['promotions/(:any)/(:any)'] = 'PromotionsController/$1/$2';

$route['reportwinloss']                = 'BalancelistController/index';
$route['reportwinloss/(:any)']         = 'BalancelistController/$1';
$route['reportwinloss/(:any)/(:any)']  = 'BalancelistController/$1/$2';

$route['history']                   = 'HistoryController/index';
$route['history/(:any)']            = 'HistoryController/$1';
$route['history/(:any)/(:any)']     = 'HistoryController/$1/$2';

$route['balance']                = 'BalanceController/index';
$route['balance/(:any)']         = 'BalanceController/$1';
$route['balance/(:any)/(:any)']  = 'BalanceController/$1/$2';

$route['agent']                 = 'AgentController/index';
$route['agent/(:any)']          = 'AgentController/$1';
$route['agent/(:any)/(:any)']   = 'AgentController/$1/$2';

$route['api']            = 'ApiController';
$route['api/(:any)']     = 'ApiController/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
